var tnl_search_locked = false;
var tnl_tabgroup = '';

$(document).ready(function(){
	if (localStorage[tnl_tabgroup+'tnl_lastopentab']) {
		tnl_lastopentab = localStorage[tnl_tabgroup+'tnl_lastopentab'];
		tnl_show_nlsettings_block(tnl_lastopentab);
	}

	$('.tnl_tab_'+tnl_tabgroup).addClass('active');

});

function tnl_show_nlsettings_block(what) {
	$('.tnl_nlsettings_block').hide(0);
	$('#tnl_nlsettings_block_'+what).show(0);

	$('.tnl_nlsettings_tab').removeClass('active');
	$('.tnl_tab_'+what).addClass('active');

	localStorage.setItem(tnl_tabgroup+'tnl_lastopentab', what);
}

//Newsletter ==================================

function tnl_send_testmail(tnl_id) {
	var url = tnl_mod_dir+'/show.php?tnl_id='+tnl_id+'&mode=testmail';
	$('#tnl_testing_message').load( url, function() {
		$('#tnl_testing_message').addClass('tnl_message_loaded');
		setTimeout(function(){
		 $('#tnl_testing_message').removeClass('tnl_message_loaded');
		}, 5000);
	});
}

function tnl_make_archive(tnl_id) {
	var url = tnl_mod_dir+'/show.php?tnl_id='+tnl_id+'&mode=archive';
	$('#tnl_archive_message').load( url, function() {
	$('#tnl_is_achived_message').hide(100);
	$('#tnl_archive_message').addClass('tnl_message_loaded');
		setTimeout(function(){
		 $('#tnl_archive_message').removeClass('tnl_message_loaded');
		}, 5000);
	});
}

function tnl_un_archive(tnl_id) {
	var url = tnl_mod_dir+'/show.php?tnl_id='+tnl_id+'&mode=unarchive';
	$('#tnl_archive_message').load( url, function() {
	$('#tnl_is_achived_message').hide(100);
	$('#tnl_archive_message').addClass('tnl_message_loaded');
		setTimeout(function(){
		 $('#tnl_archive_message').removeClass('tnl_message_loaded');
		}, 5000);
	});
}


// ziehharmonika, https://github.com/Arcwise/ziehharmonika
// MIT License
!function(e){var l;e.fn.ziehharmonika=function(s,o){if("object"!=typeof s&&void 0!==s||(l=e.extend({headline:"h3",prefix:!1,highlander:!0,collapsible:!1,arrow:!0,collapseIcons:{opened:"&ndash;",closed:"+"},collapseIconsAlign:"right",scroll:!0},s)),"open"==s){l.highlander&&e(this).ziehharmonika("forceCloseAll");var i=e(this);return e(this).addClass("active").next("div").slideDown(400,function(){var s,a;l.collapseIcons&&e(".collapseIcon",i).html(l.collapseIcons.opened),!1!==o&&(s=e(this).prev(l.collapseIcons),e("html, body").animate({scrollTop:e(s).offset().top},400,a))}),this}if("close"==s||"forceClose"==s){if("close"==s&&!l.collapsible&&1==e(l.headline+'[class="active"]').length)return this;i=e(this);return e(this).removeClass("active").next("div").slideUp(400,function(){l.collapseIcons&&e(".collapseIcon",i).html(l.collapseIcons.closed)}),this}"closeAll"==s?e(l.headline).ziehharmonika("close"):"forceCloseAll"==s&&e(l.headline).ziehharmonika("forceClose"),l.prefix&&e(l.headline,this).attr("data-prefix",l.prefix),l.arrow&&e(l.headline,this).append('<div class="arrowDown"></div>'),l.collapseIcons&&e(l.headline,this).each(function(s,o){e(this).hasClass("active")?e(this).append('<div class="collapseIcon">'+l.collapseIcons.opened+"</div>"):e(this).append('<div class="collapseIcon">'+l.collapseIcons.closed+"</div>")}),"left"==l.collapseIconsAlign&&e(".collapseIcon, "+l.headline).addClass("alignLeft"),e(l.headline,this).click(function(){e(this).hasClass("active")?e(this).ziehharmonika("close"):e(this).ziehharmonika("open",l.scroll)})}}(jQuery);