<?php

$module_directory = 'tiny_newsletter';
$module_name = 'tiny_newsletter';
$module_function = 'page';
$module_version = '0.4.1';
$module_status	= '';
$module_platform = '1.4';
$module_author = 'Chio Maisriml, Bernd Michna, Florian Meerwinck';
$module_license = 'CC-BY-NC';
$module_license_terms	= '';
$module_requirements	= '-';
$module_description = '';

?>
