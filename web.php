<?php
//web.php stellt die Webversion des Newletters dar.
//Nicht jeder Newsletter ist oeffentlich!

//http://wbce.at/tpls/modules/tiny_newsletter/web.php?n=10&a=azo9xp1n47zg
//http://localhost/2017/_DEV/wbce-1.1.10/modules/tiny_newsletter/web.php?n=2

function tnl_show_message($mod_dir, $message) {
	$path = WB_PATH.'/modules/'.$mod_dir.'/templates/tnl.inc/message.html';
	$content = file_get_contents($path);
	if (strpos($content, '[MESSAGE]') === false) {return('no valid template found');}


	$vars = array('[WB_URL]','[MOD_DIR]', '[MESSAGE]');
	$values = array(WB_URL, $mod_dir, $message);
	$content = str_replace($vars, $values, $content);

	echo $content;

}

require('../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;

require_once(WB_PATH.'/framework/class.wb.php');
$wb = new wb;
$t = time();

$tnl_id = isset($_GET['n']) ? intval($_REQUEST['n']) : 0;
if ($tnl_id == 0) {
	header("HTTP/1.1 301 Moved Permanently");
	header('Location:'.WB_URL);
	die();
}

$auth = isset($_GET['a']) ? $_GET['a'] : '';
if ($auth != '') {
	$auth = preg_replace("/[^a-z0-9,.]+/", "", $auth);
	if (strlen($auth) != 12) $auth = '';
}

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

//ist der newsletter oeffentlich?
$sql = "SELECT tnl_status, tnl_min_active FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters WHERE tnl_id = $tnl_id";
$res = $database->query($sql);
if ( $res->numRows() !=  1) { tnl_show_message($mod_dir, $MOD_TINY_NEWSLETTER['WEB_NL_NOT_FOUND']); die(); }
$row = $res->fetchRow();
$tnl_status = (int) $row['tnl_status'];
if ( $tnl_status < 2) { tnl_show_message($mod_dir, $MOD_TINY_NEWSLETTER['WEB_NL_NOT_ACTIVE']); die(); }

$tnl_min_active =  (int) $row['tnl_min_active'];
if ( $tnl_min_active > 1 AND $auth == '') { tnl_show_message($mod_dir, $MOD_TINY_NEWSLETTER['WEB_NL_UNKNOWN_RECEIVER']); die(); } //Wenn der newsletter nicht oeffentlich ist, muss ein auth angegeben sein.

$addr_id = 0; $addr_type = 0; $addr_status = 1;  $addr_name = ''; //Status 1: Jeder darf oeffentliche NEwsletter sehen, also auch ohne auth.

if ($auth != '') {
	//User ist ange
	//Das gleiche wie in action.php
	$sql = "SELECT addr_id, addr_hasseen FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_idstr2 = '$auth' AND addr_hasseen NOT LIKE '%,$tnl_id,%'";
	$res = $database->query($sql);
	if ( $res->numRows() ==  1) {
		$row = $res->fetchRow();
		$addr_id = $row['addr_id'];
		$addr_hasseen = $row['addr_hasseen'];
		if ($addr_hasseen == '') {
			$addr_hasseen  = ','.$tnl_id.',';
		} else {
			$addr_hasseen  = ','.$tnl_id.$addr_hasseen;
			$addr_hasseen = str_replace(',,',',',$addr_hasseen);
		}
		$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_hasseen = '$addr_hasseen' WHERE addr_id = '$addr_id'";
		$database->query($sql);
	}

	//Nochmal $addr_id holen
	$sql = "SELECT addr_id, addr_name, addr_type, addr_status FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_idstr2 = '$auth'";
	$res = $database->query($sql);
	if ( $res->numRows() ==  1) {
		$row = $res->fetchRow();
		$addr_id = $row['addr_id'];
		$addr_name = $row['addr_name'];
		$addr_type = (int) $row['addr_type'];
		if ($addr_name == '') {$addr_type = 0;}

		$addr_status = (int) $row['addr_status'];
		if ($addr_status < 1) {$addr_status = 1;} //Status 1: Jeder darf oeffentliche NEwsletter sehen, also auch ohne auth.
	}
}

//Darf der Empfaenger diesen NEwsletter sehen?
//echo $addr_status;
//echo $tnl_min_active;
if ($addr_status < $tnl_min_active) { tnl_show_message($mod_dir, $MOD_TINY_NEWSLETTER['WEB_NL_NO_PERMISSION']); die(); }

//Gibt es ein Archiv?:

$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_archive WHERE tnl_id = $tnl_id";
$res = $database->query($sql);
if ( $res->numRows() ==  1) {
	//eine archivierte Kopie existiert:
	//Diese enthaelt als Platzhalter nur mehr [SALUTATION]
	$row = $res->fetchRow();
	$tnl_html = $row['tnl_html'];
	$tnl_archived = $row['tnl_archived'];

	//personalisieren:
	require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');
	$settings = tnl_LoadSettings ($tablename);
	$genderarray = json_decode($settings['genderarray'], true);

	$tnl_salutation = $genderarray['gender_a-'.$addr_type];
	$tnl_salutation = str_replace('[NAME]', $addr_name, $tnl_salutation);
	$content = $tnl_html;
	$content = str_replace('[TR_ELEMENT]', '', $content);
	$content = str_replace('[SALUTATION]', $tnl_salutation, $content);

	$tnl_archived = gmdate(DATE_FORMAT, $tnl_archived);
	$temp = str_replace('[ARCHIVED_TIME]',$tnl_archived,$MOD_TINY_NEWSLETTER['WEB_NL_ARCHIVED']);
	$content = str_replace('</body', $temp.'</body', $content);


	echo 	$content;
	die();

} else {
	$tnl_showmode = 'webversion';
	require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/build_mail.inc.php');
}

?>
