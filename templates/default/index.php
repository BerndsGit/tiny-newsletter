<?php if(!defined('WB_URL')) { die(); }

$content = file_get_contents($path.'index.html');
if (strpos($content, '[MAIL_BODY]') === false) {die('no valid template found');}

//replace placeholders with values
$content = str_replace('[TNL_TEMPLATE_DIR]', $tnl_template_dir, $content);

echo $content;

?>
