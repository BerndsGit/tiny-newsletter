<?php if(!defined('WB_URL')) { die(); }

//fetch $tnl_template_data_Arr selection as chosen in template_settings
$style = $tnl_template_data_Arr['style'];
if ($style == 'default') {$style = 'newsletter';}

// is there an index.html file, and does it at least one TNL placeholder?
$content = file_get_contents($path.'index.html');
if (strpos($content, '[MAIL_BODY]') === false) {die('no valid template found');}

$bgcol='#F0F0F0';

switch ($style) {
	case 'newsletter':
		$bgcol='#fef6df'; 
		$col='#577aa0';
		$linkcol='#ce5853';
		$font='Helvetica, Arial, sans-serif';
		break;
	case 'update':
		$bgcol = '#cccccc';
		$col = '#000000';
		$linkcol='#666666';
		$font='Courier New, Courier,fixed';
		break;
}

//replace placeholders with values
$content = str_replace('[MAIN_STYLE]', $style, $content);
$content = str_replace('[BGCOL]', $bgcol, $content);
$content = str_replace('[COL]', $col, $content);
$content = str_replace('[LINKCOL]', $linkcol, $content);
$content = str_replace('[FONT]', $font, $content);
$content = str_replace('[TNL_TEMPLATE_DIR]', $tnl_template_dir, $content);

//output
echo $content;

?>
