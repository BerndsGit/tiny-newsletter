<?php if(!defined('WB_URL')) { die(); }

require_once(WB_PATH . '/modules/'.$mod_dir.'/templates/latest-news/template-functions.php');

$template = file_get_contents($path.'index.html');
if (strpos($template, '[MAIL_BODY]') === false) {die('no valid template found');}

//replace placeholders with values
$template = str_replace('[TNL_TEMPLATE_DIR]', $tnl_template_dir, $template);


$nwi_template = '
	  <!--[if mso]><tr><td width="192" style="width: 192px;" valign="top"><![endif]-->

      
        <table width="192" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row" style="width: 192px;">
          <tr>
            <td class="col" valign="top" style="padding-left:12px;padding-right:12px;padding-top:18px;padding-bottom:12px">
              <table border="0" cellpadding="0" cellspacing="0" class="img-wrapper">
                <tr>
                  <td style="padding-bottom:18px"><a href="[LINK]"><img src="[PICTURE]" border="0" alt=""  hspace="0" vspace="0" style="max-width:100%;" class="image"/></td>
                </tr>
              </table>
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:16px;line-height:22px;font-weight:600;color:#2469A0;padding-bottom:6px"><a href="[LINK]">[TITLE]</a></td>
                </tr>
              </table>
              <div class="col-copy" style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;text-align:left;color:#333333">[SHORT]</div>
              <br>
            </td>
          </tr>
        </table>

        
          <!--[if mso]></td><![endif]-->
';



if (isset($tnl_template_data_Arr['nwilist'])) {
	$what = $tnl_template_data_Arr['nwilist'];	
	$teasers = tnl_getNwiTeasers($nwi_template, $what, 3 );	
} else {
	$teasers = '';
}


$template = str_replace('[NWI_TEASERS_LIST]', $teasers, $template);
echo $template;

?>