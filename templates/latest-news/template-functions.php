<?php
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

function tnl_getNwiTeasers($template_str='', $sec = '', $limit = 3) {
	global $database;
	
	$the_nwis = implode(',', $sec);
	$theq = "SELECT * FROM `".TABLE_PREFIX."mod_news_img_posts` WHERE active = 1 AND post_id IN (".$the_nwis.") ORDER BY published_when DESC LIMIT ".$limit;	
	
	$picture_dir = WB_URL.MEDIA_DIRECTORY.'/.news_img/';
	
	$query_nwi = $database->query($theq);
	$num_nwi = $query_nwi->numRows();
	
	$teasers = '';
	if ($num_nwi > 0) {
		//echo $headline ;
		
		$vars = array('[ID]','[LINK]', '[PICTURE]', '[TITLE]', '[SHORT]');
		while($nwi = $query_nwi->fetchRow()) {
			$nwi_id = $nwi['post_id'];			
			$title = $nwi['title'];			
			$short = $nwi['content_short'];
			$picture = $picture_dir.'/'.$nwi_id.'/'.$nwi['image'];
			$link = WB_URL.PAGES_DIRECTORY.$nwi['link'].PAGE_EXTENSION;
			
			$values = array($nwi_id, $link, $picture, $title, $short);
			
			
			
			$block = str_replace($vars, $values, $template_str);			
			$teasers .= $block ;		
		}
	}
	
	return $teasers;

}



?>