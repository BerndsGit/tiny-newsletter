<?php if(!defined('WB_URL')) { die(); }

require_once(WB_PATH . '/modules/'.$mod_dir.'/templates/latest-topics/template-functions.php');

$template = file_get_contents($path.'index.html');
if (strpos($template, '[MAIL_BODY]') === false) {die('no valid template found');}

//machma gleich hier:
$template = str_replace('[TNL_TEMPLATE_DIR]', $tnl_template_dir, $template);


$topics_template = '
<tr>
<td class="tp_thumb_td"  style="width:90px;vertical-align:top;">
<img class="tp_thumb" style="width:80px; margin-bottom:10px;" src="[PICTURE_DIR]/thumbs/[PICTURE]" alt="[TITLE]" />
</td>
<td style="vertical-align:top;">
<h3 style="margin:0 0 5px 0"><a href="[LINK]">[TITLE]</a></h3>
<p>[SHORT_DESCRIPTION]</p>
[TOPIC_SHORT]
<p>&nbsp;</p>
</td>
</tr>
';



if (isset($tnl_template_data_Arr['topicslist'])) {
	$what = $tnl_template_data_Arr['topicslist'];
	$teasers = '<table style="clear:both; widht:100%;" cellpadding="0" cellspacing="0" style="border:0; border-collapse:collapse;">';
	$teasers .= tnl_getTopicTeasers($topics_template, $what, 10 );
	$teasers .= '</table>';
} else {
	$teasers = '';
}


$template = str_replace('[TOPICS_TEASERS_LIST]', $teasers, $template);
echo $template;

?>