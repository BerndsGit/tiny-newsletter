<?php
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

function tnl_getTopicTeasers($template_str='', $what = '', $limit = 3, $start = 0, $mod_dir = 'topics',  $picture_dir = MEDIA_DIRECTORY.'/topics-pictures', $topics_directory = WB_URL.PAGES_DIRECTORY.'/topics/') {
	global $database;
	
	$mode = 'picturedir';
	if (is_int($what)) { //what is eine Zahl
		$mode = 'section';
	} else {
		if (is_array($what)) { //what is ein array
			$mode = 'topics';	
		} else {
			if ($what=='') {$what = $picture_dir; } else {$picture_dir = $what;}
		}
	}
		
	
	if (!is_dir(WB_PATH.$picture_dir)) { 		
		return '<h2>picture_dir does not exist</h2>';
	}
	
	
	
	if ($mode == 'picturedir') {		
		//Get the topics-sections with the same picture_dir;
		$theq = "SELECT section_id, page_id FROM ".TABLE_PREFIX."mod_".$mod_dir."_settings WHERE picture_dir = '".$picture_dir."'";	
		$query = $database->query($theq);
		$sectionsArr = array();				
		if($query->numRows() > 0) {
			while($thesection = $query->fetchRow()) { 
				$pid = $thesection['page_id'];
				//CHeck if the page is visible or hidden. Others will not be listed.
				$theq = "SELECT visibility FROM ".TABLE_PREFIX."pages WHERE page_id =".$pid;			
				$query2 = $database->query($theq);
				$thepage = $query2->fetchRow();
				if ($thepage['visibility'] != 'public' AND $thepage['visibility'] != 'hidden' ) {continue; } 
		
				//OK, get it
				$sectionsArr[] = $thesection['section_id']; 
			}
		}		
		//are there still sections left?
		if (count($sectionsArr) < 1) {	
			return '<h2>no sections found</h2>';
		}
		
		$the_sections = implode(',', $sectionsArr);
		$theq = "SELECT * FROM `".TABLE_PREFIX."mod_".$mod_dir."` WHERE active > 3 AND section_id IN (".$the_sections.") ORDER BY published_when DESC LIMIT ".$limit;
	
	}
	
	
	if ($mode == 'section') {
		$theq = "SELECT * FROM `".TABLE_PREFIX."mod_".$mod_dir."` WHERE active > 3 AND section_id = $what ORDER BY published_when DESC LIMIT ".$limit;	
	}
	
	if ($mode == 'topics') {
		$the_topics = implode(',', $what);
		$theq = "SELECT * FROM `".TABLE_PREFIX."mod_".$mod_dir."` WHERE active > 3 AND topic_id IN (".$the_topics.") ORDER BY published_when DESC LIMIT ".$limit;	
	}
	
	
	
	$picture_dir = WB_URL.$picture_dir;
	
	$query_topics = $database->query($theq);
	$num_topics = $query_topics->numRows();
	
	$teasers = '';
	if ($num_topics > 0) {
		//echo $headline ;
		
		$vars = array('[LINK]', '[PICTURE_DIR]', '[PICTURE]', '[TITLE]', '[SHORT_DESCRIPTION]', '[TOPIC_SHORT]');
		while($topic = $query_topics->fetchRow()) {
			$comments_count = $topic['comments_count'];
			$active = $topic['active'];
			$title = $topic['title'];
			$short_description = $topic['short_description'];
			$topic_short = $topic['content_short'];
			$picture = $topic['picture'];
			$link = $topics_directory.$topic['link'].PAGE_EXTENSION;
			
			$values = array($link, $picture_dir, $picture, $title, $short_description, $topic_short);
			
			
			
			$block = str_replace($vars, $values, $template_str);			
			$teasers .= $block ;		
		}
	}
	
	return $teasers;

}



?>