<?php
//show.php macht:
//Darstellung von previews, versenden von testmails, archivierung ueber
//Fuer dieses Script muss man angemeldet und berechtigt sein.

require('../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;


require_once(WB_PATH.'/framework/class.admin.php');
$admin = new admin('Modules', 'module_view', false, false);
if (!($admin->is_authenticated() && $admin->get_permission($mod_dir, 'module'))) { die("Sorry, no access");}

/*
require_once WB_PATH.'/framework/class.frontend.php';
$wb = new wb;
if (!$wb->is_authenticated()) { die("Sorry, no access");}
require_once WB_PATH.'/framework/class.wb.php';
//require_once WB_PATH.'/framework/frontend.functions.php';
*/

$t = time();

if ( isset($_GET['tnl_id']) AND is_numeric($_GET['tnl_id']) AND $_GET['tnl_id'] > 0) {
	$tnl_id = (int)$_GET['tnl_id'];
	$tnl_showmode = 'preview';
	if ( isset($_GET['mode']) AND $_GET['mode'] == 'previewtext') {
		$tnl_showmode = 'previewtext';
	}
	$simulation = 1;


	if ( isset($_GET['mode']) AND $_GET['mode'] == 'testmail') {
		$tnl_showmode = 'testmail';
		$simulation = 0;
	}

	if ( isset($_GET['mode']) AND $_GET['mode'] == 'archive') {
		$tnl_showmode = 'archive';
		$simulation = 0;
	}

	if ( isset($_GET['mode']) AND $_GET['mode'] == 'unarchive') {
		$sql = "DELETE FROM  ".TABLE_PREFIX."mod_".$tablename."_archive WHERE tnl_id = $tnl_id";
		$query = $database->query($sql);
		echo 'DELETED';
		die();
	}

	require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/build_mail.inc.php');
}

?>
