<?php
//AJAX Gegenstueck aus view.php:
//Versendet eMail und gibt Meldungen aus
//kann auch direkt aufgerufen werden wenn keine Seite page_id gefunden wurde

require('../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;

require_once(WB_PATH.'/framework/class.wb.php');
$wb = new wb;

//==================================================================
//Tracking Image:
if ( isset($_GET['pic']) AND  isset($_GET['tnl_id']) ) {
	//http://wbce.at/tpls/modules/tiny_newsletter/action.php?pic=2gb99dk7hw74&tnl_id=1
	
	$tnl_id = (int) $_GET['tnl_id'];
	
	$pic = $_GET['pic'];
	if ($pic === '0') {
		$is_testmail = true;
	} else {
		$is_testmail = false;		
		$pic = filter_var(trim($pic), FILTER_SANITIZE_STRING);
		$pic = preg_replace("/[^a-z0-9,.]+/", "", $pic);
	}
	
	if ( ($is_testmail == true OR strlen($pic) == 12) AND $tnl_id > 0 ) {  //formal gueltig
		if ($is_testmail == false) {
			$sql = "SELECT addr_id, addr_hasseen FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_idstr2 = '$pic' AND addr_hasseen NOT LIKE '%,$tnl_id,%'";
			//die($sql);
			$res = $database->query($sql);
			if ( $res->numRows() ==  1) {				
				$row = $res->fetchRow();
				$addr_id = $row['addr_id'];			
				$addr_hasseen = $row['addr_hasseen'];			
				if ($addr_hasseen == '') {
					$addr_hasseen  = ','.$tnl_id.',';
				} else {
					$addr_hasseen  = ','.$tnl_id.$addr_hasseen;
					$addr_hasseen = str_replace(',,',',',$addr_hasseen);
				}			
				$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_hasseen = '$addr_hasseen' WHERE addr_id = '$addr_id'";
				
				$database->query($sql);						
			}
		}
		
		$file = WB_PATH.'/modules/'.$mod_dir.'/img/tracker.png';
		$type = 'image/png';
		header('Content-Type:'.$type);
		header('Content-Length: ' . filesize($file));
		readfile($file);
	}	
	die();	
}


//==================================================================

require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');
// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN-frontend.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '-frontend.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '-frontend.php');
}

require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/mailfunctions.inc.php');

$settings = tnl_LoadSettings ($tablename);
$genderarray = json_decode($settings['genderarray'], true);
$t = time();

//==================================================================
//script wurde direkt aufgerufen 
//Authentifizierung aus eMail:
if ( isset($_GET['auth'])) {
	require WB_PATH.'/modules/'.$mod_dir.'/inc/auth.inc.php';
	if ($finished != '') {	
		echo '<div class="finished '.$finishedclass.'">'.$finished.'</div>
		</div>';
		return; //Ende des Scripts		
	}
}

//Script wurde per AJAX aufgerufen:

if ($_POST) {
	//check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
		
		$output = json_encode(array( //create JSON data
			'type'=>'error', 
			'text' => 'Sorry Request must be Ajax POST'
		));
		die($output); //exit script outputting json data
    } 
	
	//Sanitize input data using PHP filter_var().
	$addr_name		= filter_var(trim($_POST["addr_name"]), FILTER_SANITIZE_STRING);
	//validate_email($email)
	$addr_email		= filter_var(trim($_POST["addr_email"]), FILTER_SANITIZE_EMAIL);
	$gender	= 		(int) $_POST["gender"];
	$pid	= 		(int) $_POST["page_id"];
	$auth		= filter_var(trim($_POST["authcode"]), FILTER_SANITIZE_STRING);
	$msg		= filter_var($_POST["msg"], FILTER_SANITIZE_STRING);
	if ($settings['use_captcha'] == 'true') :
		$captcha	= filter_var($_POST["captcha"], FILTER_SANITIZE_STRING);
	endif;
	
	$srnw =    isset($_POST['srnw']) ? intval($_POST['srnw']) : 0;
	$srnh =    isset($_POST['srnh']) ? intval($_POST['srnh']) : 0;
	$variousArr = array();
	$variousArr['srnw'] = $srnw;
	$variousArr['srnh'] = $srnh;
	$various = json_encode($variousArr);
	
	
} else {
	// Ende hier:
	die('An error occured while processing POST data');

}
	
// check captcha
if ($settings['use_captcha'] == 'true') :
	if ($captcha != $_SESSION['captcha']) :
		$output = json_encode(array('type'=>'captcha_error', 'text' => $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA']));
		die($output);
	endif;
endif;
	
//additional php validation
if(strlen($addr_name)<4){ // If length is less than 4 it will output JSON error.
	$output = json_encode(array('type'=>'name_error', 'text' => $MOD_TINY_NEWSLETTER['ERROR_NAME']));
	die($output);
}

if(!filter_var($addr_email, FILTER_VALIDATE_EMAIL)){ //email validation
	$output = json_encode(array('type'=>'email_error', 'text' => $MOD_TINY_NEWSLETTER['ERROR_MAIL']));
	die($output);
}



//======================================================================================================
//Schon vorhanden?
$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_email = '$addr_email'";
$res = $database->query($q);
if( $res->numRows() >  0) {
	$output = json_encode(array('type'=>'mail_error', 'text' => $MOD_TINY_NEWSLETTER['ERROR_MAIL_ALREADY']));
	die($output);	
}


//Block ist scheinbar obsolet:
//Wurde ein Authcode uebergeben?
if(strlen($auth) > 0){
	if(strlen($auth) == 12){ 
		$output = json_encode(array('type'=>'finished', 'text' => 'ALTE MELDUNG'));
		die($output);
	}	
} else {
	$auth == '';
}
	





if($auth == '') { //Noch kein Auth Code	
	$auth = tnl_GenerateRandomString();
	$auth_link = WB_URL.'/modules/'.$mod_dir.'/action.php?auth='.$auth;
	$remove_link = WB_URL.'/modules/'.$mod_dir.'/action.php?auth='.$auth.'&amp;do=remove';
	
	if ($pid > 0) {
		$q = "SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id = ".$pid;	
		$res = $database->query($q);
		if( $res->numRows() >  0) {
			$row = $res->fetchRow();
			$auth_link = WB_URL.PAGES_DIRECTORY.$row['link'].PAGE_EXTENSION.'?auth='.$auth.'#nlfrmblock';
			$remove_link = WB_URL.PAGES_DIRECTORY.$row['link'].PAGE_EXTENSION.'?auth='.$auth.'&amp;do=remove#nlfrmblock';
		}
	}
	
	//==============================================================================================
	//Mail an User.
	
	$subject =  tnl_GetSettings($settings, 'confirmation_mail_subject');	
	$mail_message =  tnl_GetSettings($settings, 'confirmation_mail_text');
	$mail_message = str_replace('[AUTH]',$auth,$mail_message);
	$mail_message = str_replace('[AUTH_LINK]',$auth_link,$mail_message);
	$mail_message = str_replace('[REMOVE_LINK]',$remove_link,$mail_message);
	
	$from_name = tnl_GetSettings($settings, 'confirmation_mail_name');
	$from_email = tnl_GetSettings($settings, 'confirmation_mail_email');
	
	//Anrede
	$addr_type = (int) $gender;
	if ($addr_name == '') {$addr_type = 0;}			
	$tnl_salutation = $genderarray['gender_a-'.$addr_type];
	$tnl_salutation = str_replace('[NAME]', $addr_name, $tnl_salutation);
	
	$mail_message = str_replace('[SALUTATION]', $tnl_salutation, $mail_message);
	
	//send:
	tnl_send_mail($from_email, $from_name, $addr_email, $addr_name, $subject, $mail_message, '', 0);
	
		
	//=======================================================================
	//Add to database
	$addr_email = addslashes($addr_email);
	$addr_name = addslashes($addr_name);
	$auth = addslashes($auth);
	$msg = addslashes($msg);	
	$various = addslashes($various);
	
	$q = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_addrs SET 
		addr_email = '$addr_email',
		addr_name = '$addr_name',
		addr_type = $gender,
		addr_started = $t,
		addr_idstr1 = '$auth',
		addr_msg = '$msg',
		addr_various = '$various'
		;";
	
		
	//echo $q;
	$database->query($q);


	
	
	//$output = json_encode(array('type'=>'message', 'text' => $MOD_TINY_NEWSLETTER['AUTHCODE_MESSAGE'].'<br/>'.$mail_message));
	$output = json_encode(array('type'=>'message', 'text' => tnl_GetSettings($settings, 'confirmation_text')));
	die($output);
	
} else {
	$output = json_encode(array('type'=>'error', 'text' => 'Could not send mail! Please check your PHP mail configuration.'));
	die($output);
}	


?>
