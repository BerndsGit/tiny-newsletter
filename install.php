<?php

// $Id: install.php 707 2008-02-18 18:40:08Z doc $

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2008, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

if(!defined('WB_URL')) { die(); }

//global $database;
//global $admin;

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;

$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_".$tablename."_addrs`");
$sql = 'CREATE TABLE `'.TABLE_PREFIX.'mod_'.$tablename.'_addrs` ( '
      . '`addr_id`      INT NOT NULL AUTO_INCREMENT,'
	  . '`addr_name`    VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`addr_email`   VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`addr_type`    TINYINT NOT NULL DEFAULT \'0\','
	  . '`addr_status`    TINYINT NOT NULL DEFAULT \'0\','
	  . '`addr_started` INT NOT NULL DEFAULT \'0\','
	  . '`addr_gotlast` INT NOT NULL DEFAULT \'0\','
	  
	  . '`addr_idstr1`  VARCHAR(12) NOT NULL DEFAULT \'\','
	  . '`addr_idstr2`  VARCHAR(12) NOT NULL DEFAULT \'\','
	  . '`addr_hasgot`  VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`addr_hasseen` VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`addr_various` VARCHAR(255) NOT NULL DEFAULT \'\',' 
	  
	  . '`addr_msg`  VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`addr_statsdata`  VARCHAR(255) NOT NULL DEFAULT \'0,0,0,0,0,0,0,0,0\','

      . 'PRIMARY KEY (addr_id)'
      . ' )';
$database->query($sql);

$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_".$tablename."_newsletters`");
$sql = 'CREATE TABLE `'.TABLE_PREFIX.'mod_'.$tablename.'_newsletters` ( '
      . '`tnl_id` INT NOT NULL AUTO_INCREMENT,'
	  . '`tnl_status` TINYINT NOT NULL DEFAULT \'0\','
	  . '`tnl_min_active` TINYINT NOT NULL DEFAULT \'0\','
	  	   
	  . '`tnl_published_when` INT NOT NULL DEFAULT \'0\','
      . '`tnl_published_until` INT NOT NULL DEFAULT \'0\','
	  
	  . '`tnl_subject`  VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`tnl_body` TEXT  NOT NULL, '
	  . '`tnl_body_text` TEXT  NOT NULL, '
	  . '`tnl_first_sent` INT NOT NULL DEFAULT \'0\','
	  . '`tnl_last_sent` INT NOT NULL DEFAULT \'0\','
	  . '`tnl_count_sent` INT NOT NULL DEFAULT \'0\','
	  . '`tnl_template_data` TEXT  NOT NULL, '
	  . '`tnl_statsdata`  VARCHAR(255) NOT NULL DEFAULT \'0,0,0,0,0,0,0,0,0\','
	  
      . 'PRIMARY KEY (tnl_id)'
      . ' )';
$database->query($sql);


$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_".$tablename."_settings`");
$sql = 'CREATE TABLE `'.TABLE_PREFIX.'mod_'.$tablename.'_settings` ( '
	  . '`property`  VARCHAR(255) NOT NULL DEFAULT \'\','
	  . '`value` TEXT  NOT NULL, '	  
      . 'PRIMARY KEY (property)'
      . ' )';
$database->query($sql);


$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_".$tablename."_archive`");
$sql = 'CREATE TABLE `'.TABLE_PREFIX.'mod_'.$tablename.'_archive` ( '
	  . '`tnl_id` INT NOT NULL,'
	  . '`tnl_archived` INT NOT NULL DEFAULT \'0\','	
	  . '`tnl_html` TEXT  NOT NULL, '  
      . 'PRIMARY KEY (tnl_id)'
      . ' )';
$database->query($sql);

$picpath = WB_PATH.MEDIA_DIRECTORY.'/tnl-pics';
make_dir($picpath);


?>