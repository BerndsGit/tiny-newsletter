<?php
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;

$t = time();

//======================================================================
//After installing:
function tnl_ImportSettings ($tablename, $mod_dir) {
	global $database;

	if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '-settings.php')) {
		require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '-settings.php');
	} else {
		require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN-settings.php');
	}
	if (!isset($settingsArr)) die('No settings-file found!');

	$settings = json_decode($settingsArr, true);
	foreach ($settings as $p => $v) {
		$query = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$v', property = '$p'; \n";
		$database->query($query);
	}

	$sql = "SELECT * FROM ".TABLE_PREFIX."users WHERE active > 0 ORDER BY user_id"; //No Limit
	$res = $database->query($sql);
	$testmail_adressesArr = array();
	while ( $row = $res->fetchRow() ) {
		$addr_name = $row['display_name'];
		$email = $row['email'];
		$addr_status = 3;
		$groups_id = ',,'.$row['groups_id'].',';
		if (strpos($groups_id,',1,') !== false) {$testmail_adressesArr[] = $email; $addr_status = 4;}

		if ($row['user_id'] == 1) {
			$admin_name = $addr_name; $admin_email = $email;
			$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$admin_name' WHERE property = 'confirmation_mail_name'; ";
			$database->query($query);
			$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$admin_email' WHERE property = 'confirmation_mail_email'; ";
			$database->query($query);
			$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$admin_name' WHERE property = 'newsletter_mail_name'; ";
			$database->query($query);
			$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$admin_email' WHERE property = 'newsletter_mail_email'; ";
			$database->query($query);
		}

		$login_when = $row['login_when'];
		$tmin = time() - (3600 * 24 * 6); //6 Tage
		$tmax = time() - (3600 * 30); //-1 Tage
		if ($login_when < $tmin) {$login_when = $tmin;}
		if ($login_when > $tmax) {$login_when = $tmax;}

		$addr_idstr1 = tnl_GenerateRandomString();
		$addr_idstr2 = tnl_GenerateRandomString();

		$query = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_name = '$addr_name', addr_email = '$email', addr_started = '$login_when', addr_idstr1 = '$addr_idstr1', addr_idstr2 = '$addr_idstr2', addr_status = $addr_status, addr_msg = 'users'";
		$database->query($query);
	 }

	$testmail_adresses = implode(',',$testmail_adressesArr);
	$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$testmail_adresses' WHERE property = 'testmail_adresses'";
	$database->query($query);

}
//======================================================================

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}
require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');

$paramdelimiter = '&amp;';
$params = 'page_id='.$page_id.$paramdelimiter.'section_id='.$section_id;
$tnl_url = WB_URL.'/modules/'.$mod_dir.'/admin/';

$editbase = WB_URL.'/modules/'.$mod_dir.'/admin/newsletters.php?'.$params.$paramdelimiter.'tnl_id=';

$settings = array();
$query = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_settings WHERE property ='confirmation_mail_email' ";
$res = $database->query($query);
$row = $res->fetchRow();
$value = $row['value'];
if ($value == '') {
	echo '<div class="tnl_tab_settings_warning"><h3>'.$MOD_TINY_NEWSLETTER['SETTINGS_WARNING'].'</h3>
	<h3><a class="tnl_button" href="'.$tnl_url.'settings.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['SETTINGS'].'</a></h3>
	<div style="clear:both; height;20px;"></div>
	</div>';

	//settings importieren:


	tnl_ImportSettings ($tablename, $mod_dir);
	return;
}


echo '<div class="tnl_modify">';
echo '<div id="sec_tiny_newsletter"  class="tnl_tabs">';

echo '<a class="tnl_tab_receivers" href="'.$tnl_url.'receivers.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['RECEIVERS'].'</a>';
echo '<a class="tnl_tab_newsletters" href="'.$tnl_url.'newsletters.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['NEWSLETTERS'].'</a>';

echo '<a class="tnl_tab_export" href="'.$tnl_url.'export.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['EXPORT'].'</a>';
echo '<a class="tnl_tab_statistics" href="'.$tnl_url.'statistics.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['STATISTICS'].'</a>';
echo '<a class="tnl_tab_settings" href="'.$tnl_url.'settings.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['SETTINGS'].'</a>';
echo '<a class="tnl_tab_help" href="'.$tnl_url.'help.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['HELP'].'</a>';
echo '</div><div class="tnl_tabs_bottom"></div>';

//Count receivers
echo '<div class="tnl_modify_block1">';
$query = "SELECT COUNT(*) as addr_sum FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status > 0";
$res = $database->query($query);
$row = $res->fetchRow();
$addr_sum = $row['addr_sum'];
$out = str_replace('[TNL_ADDR_SUM]',$addr_sum,$MOD_TINY_NEWSLETTER['RECEIVERS_OVERVIEW']);
echo $out;
echo '</div>';

echo '<div class="tnl_modify_block2">';
$tnl_limit = 3; $listing_style = 'modify';
require WB_PATH.'/modules/'.$mod_dir.'/inc/newsletter-list.inc.php';
echo '</div>
<div style="clear:both;height:20px;"></div>
</div>';


?>
</div>
