$(document).ready(function() {
    $("#tnl_submit_btn").click(function() {   
	    var proceed = true;
		
        //simple validation at client's end
        //loop through each field and we simply change border color to red for invalid fields		
		$("#tnl_register input[required=true], #tnl_register textarea[required=true], #tnl_register input[name=captcha]").each(function(){
			$(this).css('border-color',''); 
			if(!$.trim($(this).val())){ //if this field is empty 
				$(this).css('border-color','red'); //change border color to red   
				proceed = false; //set do not proceed flag
			}

			//check invalid email
			var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
			if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
				$(this).css('border-color','red'); //change border color to red   
				proceed = false; //set do not proceed flag
					console.log('no email');				
			}	
		});
      
        if(proceed){ //everything looks good! proceed...
         	console.log('proceed');
			//get input field values data to be sent to server
            post_data = {
				'addr_name'		: $('input[name=tnl_name]').val(), 
				'addr_email'	: $('input[name=tnl_email]').val(), 
				'authcode'		: $('input[name=tnl_authcode]').val(), 
				'gender'		: $('select[name=tnl_gender]').val(), 
				'page_id'		: $('input[name=tnl_page_id]').val(), 
				'msg'			: $('textarea[name=tnl_message]').val(),
				'srnw'			: screen.width,
				'srnh'			: screen.height,
				'captcha'		: $('input[name=captcha]').val()
			};
            
            //Ajax post data to server
            $.post(WB_URL+'/modules/'+tnl_mod_dir+'/action.php', post_data, function(response){  
				switch (response.type) {
					case 'error'			:	output = '<div class="error">'+response.text+'</div>';
												break;
					case 'captcha_error'	:	output = '<div class="error">'+response.text+'</div>';
												$('input[name=captcha]').css('border-color','red').select();
												break;
					case 'mail_error'	:		output = '<div class="error">'+response.text+'</div>';
												$('input[name=tnl_email]').css('border-color','red').select();
												break;
					case 'name_error'	:		output = '<div class="error">'+response.text+'</div>';
												$('input[name=tnl_name]').css('border-color','red').select();
												break;
					case 'message'			:	$("#tnl_register .tnl_inputblock").slideUp();
						$("#tnl_register #tnl_submit_btn").hide();
									    output = '<div class="success">'+response.text+'</div>';
												break;
				}

				$("#tnl_register #contact_results").hide().html(output).slideDown();
            }, 'json');

        }
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#tnl_register  input[required=true], #tnl_register textarea[required=true], #tnl_register input[name=captcha]").keyup(function() {
        $(this).css('border-color',''); 
        $("#contact_results").slideUp();
    });
});
