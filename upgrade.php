<?php
// Must include code to stop this file being access directly
if(defined('WB_PATH') == false) { exit("Cannot access this file directly"); }

$mod_dir = basename(dirname(__FILE__));

// Settings:
$query = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_".$mod_dir."_newsletters LIMIT 1");
//if($query->numRows() != 1) { die('No Newsletters'); }
$row = $query->fetchRow();


// Add tnl_body_text
if(!isset($row['tnl_body_text'])){
	if($database->query("ALTER TABLE `".TABLE_PREFIX."mod_".$mod_dir."_newsletters` ADD `tnl_body_text` TEXT NOT NULL")) {
		echo '<p style="color:#0bb61f; font-weight:bold;">Database Field "tnl_body_text" added successfully</p>';
	}
}

// Add tnl_template_data
if(!isset($row['tnl_template_data'])){
	if($database->query("ALTER TABLE `".TABLE_PREFIX."mod_".$mod_dir."_newsletters` ADD `tnl_template_data` TEXT NOT NULL")) {
		echo '<p style="color:#0bb61f; font-weight:bold;">Database Field "tnl_template_data" added successfully</p>';
	}
}

// Add tnl_min_active
if(!isset($row['tnl_min_active'])){
	if($database->query("ALTER TABLE `".TABLE_PREFIX."mod_".$mod_dir."_newsletters` ADD `tnl_min_active` TINYINT NOT NULL DEFAULT 0")) {
		echo '<p style="color:#0bb61f; font-weight:bold;">Database Field "tnl_min_active" added successfully</p>';
	}
}


//Addng Table 'archive'
$sql = "SELECT 1 FROM ".TABLE_PREFIX."mod_".$mod_dir."_archive";
$val = $database->query($sql);

if($val === FALSE) {
	echo '<p style="color:#0bb61f; font-weight:bold;">Database table "mod_'.$mod_dir.'_archive" is already there</p>';
} else {
	$sql = 'CREATE TABLE `'.TABLE_PREFIX.'mod_'.$mod_dir.'_archive` ( '
	  . '`tnl_id` INT NOT NULL,'
	  . '`tnl_archived` INT NOT NULL DEFAULT \'0\','
	  . '`tnl_html` TEXT  NOT NULL, '
      . 'PRIMARY KEY (tnl_id)'
      . ' )';
	$database->query($sql);

	echo '<p style="color:#0bb61f; font-weight:bold;">Database table "mod_'.$mod_dir.'_archive" added successfully</p>';
}

?>
