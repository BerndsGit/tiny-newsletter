<?php
// Include WB admin wrapper script
require(WB_PATH.'/modules/admin.php');
$admin = new admin('Modules', 'module_view', false, false);
if (!($admin->is_authenticated() && $admin->get_permission($mod_dir, 'module'))) { die("Sorry, no access");}
require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');

$paramdelimiter = '&amp;';
$params = 'page_id='.$page_id.$paramdelimiter.'section_id='.$section_id;
$tnl_url = WB_URL.'/modules/'.$mod_dir.'/admin/';

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

$settings = array();
$query = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_settings ";
$res = $database->query($query);
while ($row = $res->fetchRow()) {	
	$property = $row['property'];
	$value = $row['value'];	
	$settings[$property] = $value;
}
$genderarray = json_decode($settings['genderarray'], true);
$intval_array = json_decode($settings['intval_array'], true);




echo '<div class="tnl_tabs">';
echo '<a class="tnl_tab_modify" href="'.ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'#sec_tiny_newsletter"> &laquo; </a>';

echo '<a class="tnl_tab_receivers" href="'.$tnl_url.'receivers.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['RECEIVERS'].'</a>';
echo '<a class="tnl_tab_newsletters" href="'.$tnl_url.'newsletters.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['NEWSLETTERS'].'</a>';
echo '<a class="tnl_tab_export" href="'.$tnl_url.'export.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['EXPORT'].'</a>';

echo '<a class="tnl_tab_statistics" href="'.$tnl_url.'statistics.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['STATISTICS'].'</a>';
echo '<a class="tnl_tab_settings" href="'.$tnl_url.'settings.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['SETTINGS'].'</a>';
echo '<a class="tnl_tab_help" href="'.$tnl_url.'help.php?'.$params.'">'.$MOD_TINY_NEWSLETTER['HELP'].'</a>';
echo '</div><div class="tnl_tabs_bottom"></div>';
?>

<script>
var WB_URL = "<?php echo WB_URL; ?>";
var tnl_mod_dir = "<?php echo WB_URL.'/modules/'.$mod_dir; ?>";
var tnl_do_url = "<?php echo WB_URL.'/modules/'.$mod_dir.'/admin/do.php?'; ?>";
var tnl_modify_msg = "<b><?php echo $MOD_TINY_NEWSLETTER['FILTER_ADD'];?>:</b>";
</script>
