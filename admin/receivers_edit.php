<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
$admin = new admin();
$addr_id = $admin->get_get('id');

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

//fetch the data
$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_id=".$addr_id;
$res = $database->query($q);
$d = $res->fetchRow(MYSQL_ASSOC);

//build the status array
$aStatusNames = $MOD_TINY_NEWSLETTER['STATUS_ARRAY'];
$aStatus = [];
foreach ($aStatusNames as $key=>$name) :
	$aStatus[$key]['name'] = $name;
	$aStatus[$key]['key'] = $key - 2;
	$aStatus[$key]['selected'] = ($key - 2 == $d['addr_status']) ? 'selected' : '';
endforeach;

//build the salutations array
$genderarray = $database->get_one("SELECT `value` FROM ".TABLE_PREFIX."mod_".$tablename."_settings WHERE `property` = 'genderarray'");
$aGenderTmp = json_decode($genderarray, true);

$aSalutations = ['-','F','M','N'];
$aGender = [];
foreach ($aGenderTmp as $k=>$v) :
	if (strstr($k, '_a-')) continue;
	if ($v == '') continue;
	$i = substr($k, -1);
	if ($v == '-') $v = 'neutral';
	$aGender[$i]['name'] = $v;
	$aGender[$i]['val'] = $i;
	$aGender[$i]['short'] = $aSalutations[$i];
	$aGender[$i]['selected'] = ($i == $d['addr_type']) ? 'selected' : '';
endforeach;
?>

<h3><?=$MOD_TINY_NEWSLETTER['EDIT_RECEIVER_HEADER']?></h3>
<form method="post" action="receivers_save.php">

	<input type="hidden" name="addr_id" value="<?=$addr_id?>">
	<input type="hidden" name="page_id" value="<?=$admin->get_get('page_id')?>">
	<input type="hidden" name="section_id" value="<?=$admin->get_get('section_id')?>">
	<input type="hidden" name="what" value="update">

	<table>
		<tr>
			<td><?=$TEXT['NAME']?></td>
			<td><input type="text" name="addr_name" value="<?=$d['addr_name']?>"></td>
		</tr>
		<tr>
			<td><?=$TEXT['EMAIL']?></td>
			<td><input type="email" name="addr_email" value="<?=$d['addr_email']?>"></td>
		</tr>
		<tr>
			<td><?=$MOD_TINY_NEWSLETTER['SALUTATION']?></td>
			<td>
				<select name="addr_type">
					<?php foreach ($aGender as $gender) : ?>
						<option value="<?=$gender['val']?>" <?=$gender['selected']?>>
							<?=$gender['name']?> (<?=$gender['short']?>)
						</option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Status</td>
			<td>
				<select name="addr_status">
					<?php foreach ($aStatus as $status) : ?>
						<option value="<?=$status['key']?>" <?=$status['selected']?>>
							<?=$status['name']?> (<?=$status['key']?>)
						</option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button type="submit" style="margin-top:20px;"><?=$TEXT['SAVE']?></button>
			</td>
		</tr>
	</table>

</form>
