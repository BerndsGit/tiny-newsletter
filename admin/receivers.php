<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');
$settings = tnl_LoadSettings ($tablename);

//diverse data-arrays
$aSalutations = ['-','F','M','N'];
$genderarray = json_decode($settings['genderarray'], true);

$aGender = [];
for ($i=0; $i < 6; $i++) :
	$s = $genderarray['gender_t-'.$i];
	if ($s == '') { continue; }
	if ($s == '-') $s = 'neutral';
	$aGender[] = $s;
endfor;

//show columns for the last three newsletter?
$fShowLast = false;

require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');

$aTypes = $MOD_TINY_NEWSLETTER['STATUS_ARRAY'];

//language file for datatables
$dt_lang_file = WB_URL.'/modules/'.$mod_dir.'/languages/'.LANGUAGE.'_dataTables.json';

//fetch the address data
$query = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs ORDER BY addr_started DESC";
$res = $database->query($query);
$data = array();
$i = 0;
while ($d = $res->fetchRow()) :
	$data[$i]['id'] = $d['addr_id'];
	$data[$i]['status'] = $d['addr_status'];
	$data[$i]['status_text'] = $aTypes[$d['addr_status'] + 2];
	$data[$i]['name'] = $d['addr_name'];
	$data[$i]['email'] = $d['addr_email'];
	$data[$i]['salutation'] = $aSalutations[$d['addr_type']];
	$data[$i]['salutation_text'] = ($d['addr_type'] != '') ? $aGender[$d['addr_type']] : '';
	$data[$i]['started'] = ceil((time() - $d['addr_started']) / 86400);
	$data[$i]['gotlast'] = gmdate(DATE_FORMAT, $d['addr_gotlast']);
	$data[$i]['idstr1'] = $d['addr_idstr1'];
	$data[$i]['idstr2'] = $d['addr_idstr2'];

	$addr_hasgot = '';
	$last_hasgot = '';
	$cHasGot = '';
	if ($d['addr_hasgot'] != '') :
		$aHasGot = explode(',', substr($d['addr_hasgot'],1,-1));
		$cHasGot = count($aHasGot);
		$g = 1;
		foreach ($aHasGot as $hasgot) :
			$addr_hasgot .= $hasgot.',';
			if ($g % 6 == 0) :
				$addr_hasgot .= '<br>';
			endif;
			if ($g <= 3) :
				$last_hasgot .= $hasgot.',';
			endif;
			$g++;
		endforeach;
	endif;

	$addr_hasseen = '';
	$last_hasseen = '';
	$cHasSeen = '';
	if ($d['addr_hasseen'] != '') :
		$aHasSeen = explode(',', substr($d['addr_hasseen'],1,-1));
		$cHasSeen = count($aHasSeen);
		$s = 1;
		foreach ($aHasSeen as $hasseen) :
			$addr_hasseen .= $hasseen.',';
			if ($s % 6 == 0) :
				$addr_hasseen .= '<br>';
			endif;
			if ($s <= 3) :
				$last_hasseen .= $hasseen.',';
			endif;
			$s++;
		endforeach;
	endif;

	$data[$i]['hasgot'] = substr($addr_hasgot,0,-1);
	$data[$i]['last_hasgot'] = substr($last_hasgot,0,-1);
	$data[$i]['nr_hasgot'] = $cHasGot;;
	$data[$i]['hasseen'] = substr($addr_hasseen,0,-1);
	$data[$i]['last_hasseen'] = substr($last_hasseen,0,-1);
	$data[$i]['nr_hasseen'] = $cHasSeen;
	$aVarious = json_decode($d['addr_various'], true);
	$data[$i]['various'] = $aVarious['srnw'];
	$add_info = $d['addr_msg'];
	if (LANGUAGE == 'DE') :
		if ($add_info == 'added') :
			$add_info = 'manuell hinzugefügt';
		endif;
		if ($add_info == 'imported') :
			$add_info = 'importiert';
		endif;
	endif;
	$data[$i]['msg'] = $add_info;

	$data[$i]['msg_short'] = strtoupper(substr($d['addr_msg'],0,1));
	$data[$i]['statsdata'] = $d['addr_statsdata'];
	$i++;
endwhile;

$fHasData = (count($data) > 0) ? true : false;
?>

<link rel="stylesheet" type="text/css" href="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/datatables.css"/>
<link rel="stylesheet" type="text/css" href="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/buttons.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/tooltipster.bundle.min.css"/>
<link rel="stylesheet" type="text/css" href="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/jquery.modal.css"/>


<script src="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/datatables.min.js"></script>
<script src="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/dataTables.buttons.min.js"></script>
<script src="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/tooltipster.bundle.min.js"></script>
<script src="<?=WB_URL?>/modules/<?=$mod_dir?>/admin/DataTables/jquery.modal.min.js"></script>

<script>
	var tnl_tabgroup = 'receivers';

	$(document).ready(function() {

		//setup - add a text input to cell headers
		$('#receiversTable thead tr').clone(true).appendTo( '#receiversTable thead' );
		$('#receiversTable thead tr:eq(0) th').each(function(i) {
			if (i<=5 || i==8) {
				var title = $(this).text();
				$(this).html( '<input style="width: 99%; height: 24px; font-size:12px; padding:2px;" type="text" placeholder="" />' );
				$(this).removeAttr('title').removeClass('tooltip');
				$( 'input', this ).on('keyup change', function () {
					if ( table.column(i).search() !== this.value ) {
						table
							.column(i)
							.search(this.value)
							.draw();
					}
				});
			} else {
				$(this).html('');
			}
		});

		//init datatables
		var table = $('#receiversTable').DataTable({
			dom: 'lirBtip',
			language: {
				'url': '<?=$dt_lang_file?>'
			},
			buttons: [
				{
					text: '<?=$MOD_TINY_NEWSLETTER['CLEAR_SEARCH']?>',
					className: 'clear-filter',
					enabled: true,
					action: function(e, dt, node, config) {
						$('#receiversTable thead input').val('').change();
						$('#receiversTable').DataTable().search('').draw();
						//table.button(0).disable();
						$('#receiversTable_length').show();
					}
				}
			],
			'columnDefs': [{
				'orderable': false,
				'searchable': false,
				'targets': [9,10]
			}],
			'order': [[ 4, 'asc' ]],
			stateSave: true,
			paging: true,
			'pageLength': 20,
			'lengthMenu': [[20, 40, 60, -1], [20, 40, 60, 'All']],
			//init tooltipster
			'fnDrawCallback': function() {
				$('.tooltip').tooltipster({
					contentAsHTML : true,
					position: 'right',
					debug: false,
				});
			},
		});

		// handling and format clear button
		//$('.dt-buttons button').css('color', 'firebrick');
		//$('.dt-buttons').css('float', 'right !important').css('margin-right', '0');

		$('#receiversTable').on('search.dt', function() {
			var value = $('.dataTables_filter input').val();
			table.button(0).enable();
			//$('#receiversTable_length').hide();
		});

		//init tooltipster for header
		$('.tooltip-head').tooltipster({
			contentAsHTML : true,
			position: 'top',
			debug: false,
		});

		//handle delete
		$('.receivers-delete').click(function() {
			return confirm("<?=$MOD_TINY_NEWSLETTER['CONFIRM_DELETE']?>");
		});

		//add header word to the data-table
		$('#receiversTable_info').append('<br><b>Filter:</b>');

	});
</script>

<div class="tnl_addrlisting">
	<div id="tnl_modify_msg" style="float:left;">
		<p><b><?php echo $MOD_TINY_NEWSLETTER['FILTER_ADD'];?></b></p>
	</div>
	<div style="float:right;">
		<p><a href="receivers_add.php?page_id=<?=$page_id?>&section_id=<?=$section_id?>" rel="modal:open" class="tooltip" title="<?=$MOD_TINY_NEWSLETTER['ADD_RECEIVER']?>"><i class="fa fa-2x fa-plus-circle"></i></a></p>
	</div>
	<div id="tnl_addrlisting_listing">
		<?php if ($fHasData === true) : ?>
			<table id="receiversTable">
				<thead>
					<tr>
						<th class="tnl-status tooltip-head" title="Status">S</th>
						<th class="tnl-name"><?=$TEXT['NAME']?></th>
						<th class="tnl-email"><?=$TEXT['EMAIL']?></th>
						<th class="tnl-info tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['SALUTATION']?>">G</th>
						<th class="tnl-info tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['DAYS_ACTIVE']?>">D</th>
						<th class="tnl-info tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['ADDITIONAL_INFO']?>"><i class="fa fa-info"></th>
						<th class="tnl-got-seen tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['NEWSLETTER_SEND']?>">&sum;&nbsp;<i class="fa fa-envelope-o"></i></th>
						<?php if ($fShowLast === true) : ?>
							<th class="tnl-got-seen tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['NEWSLETTER_SEND_LAST']?>"><i class="fa fa-envelope-o"></i></th>
						<?php endif; ?>
						<th class="tnl-got-seen tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['NEWSLETTER_SEEN']?>">&sum;&nbsp;<i class="fa fa-eye"></i></th></th>
						<?php if ($fShowLast === true) : ?>
							<th class="tnl-got-seen tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['NEWSLETTER_SEEN_LAST']?>"><i class="fa fa-eye"></i></th></th>
						<?php endif; ?>
						<th class="tnl-info tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['SCREEN_SIZE']?>"><i class="fa fa-desktop"></i></th>
						<th class="tnl-action tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['EDIT_RECEIVER']?>"><i class="fa fa-edit"></th>
						<th class="tnl-action tooltip-head" title="<?=$MOD_TINY_NEWSLETTER['DELETE_RECEIVER']?>"><i class="fa fa-trash"></i></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data as $d) : ?>
						<tr>
							<td class="tnl-status addr-status<?=$d['status']?> tooltip" title="<?=$d['status_text']?>">
								<?=$d['status']?>
							</td>
							<td class="tnl-name">
								<?=$d['name']?>
							</td>
							<td class="tnl-email">
								<?=$d['email']?>
							</td>
							<td class="tnl-info tooltip" title="<?=$d['salutation_text']?>">
								<?=$d['salutation']?>
							</td>
							<td class="tnl-info">
								<?=$d['started']?>
							</td>
							<td  class="tnl-info <?= ($d['msg'] != '') ? 'tooltip' : '' ?>" title="<?=$d['msg']?>">
								<?=$d['msg_short']?>
							</td>
							<td class="tnl-got-seen <?= ($d['hasgot'] != '') ? 'tooltip' : '' ?>" title="<?=$d['hasgot']?>">
								<?=$d['nr_hasgot']?>
							</td>
							<?php if ($fShowLast === true) : ?>
								<td class="tnl-got-seen">
									<?=$d['last_hasgot']?>
								</td>
							<?php endif; ?>
							<td class="tnl-got-seen  <?= ($d['hasseen'] != '') ? 'tooltip' : '' ?>" title="<?=$d['hasseen']?>">
								<?=$d['nr_hasseen']?>
							</td>
							<?php if ($fShowLast === true) : ?>
								<td class="tnl-got-seen">
									<?=$d['last_hasseen']?>
								</td>
							<?php endif; ?>
							<td class="tnl-info">
								<?=$d['various']?>
							</td>
							<td class="tnl-action">
								<a href="receivers_edit.php?id=<?=$d['id']?>&page_id=<?=$page_id?>&section_id=<?=$section_id?>" rel="modal:open">
									<i class="fa fa-edit"></i>
								</a>
							</td>
							<td class="tnl-action">
								<?php if ($d['status'] < 1) : ?>
									<a href="receivers_delete.php?id=<?=$d['id']?>&page_id=<?=$page_id?>&section_id=<?=$section_id?>" class="receivers-delete">
										<i class="fa fa-trash" style="color:firebrick"></i>
									</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else : ?>
			<h2 class="tnl_noresults"><?=$MOD_TINY_NEWSLETTER['NO_RESULTS']?></h2>
		<?php endif; ?>
	</div>
</div>

<?php $admin->print_footer(); ?>
