<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }


$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');

$typeArr = array('-','F','M','N','Nf','Nm','Nn');

?>
<script>
var tnl_tabgroup = 'export';
</script>

<a href="#" class="tnl_nlsettings_tab tnl_tab_bcc" onclick="tnl_show_nlsettings_block('bcc');return false;">BCC</a>
<a href="#" class="tnl_nlsettings_tab tnl_tab_texttab" onclick="tnl_show_nlsettings_block('texttab');return false;">Text-TAB</a>
<a href="#" class="tnl_nlsettings_tab tnl_tab_import" onclick="tnl_show_nlsettings_block('import');return false;">Import</a>

<div style="clear:both;"></div>


<?php
//su@example.com; Ted <ted@example.com>; Mako, Ina <ina.mako@example.com>

$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status > 0 AND addr_idstr2 <> '' AND addr_email NOT LIKE '%.test' ORDER BY addr_started DESC";
$res = $database->query($sql);
$addr_emailArr = array();
$export_tab = ''; $export_tabfull = ''; $delim = "\t";
while ( $row = $res->fetchRow() ) {
	$addr_emailArr[] = $row['addr_email'];



	$line =  $row['addr_email'].$delim.$row['addr_name'].$delim.$typeArr[$row['addr_type']];
	$export_tab .= $line."\n";

	$linefull =  $row['addr_status'].$delim.$row['addr_type'].$delim.$row['addr_name'].$delim.$row['addr_email'].$delim.$row['addr_started'].$delim.$row['addr_gotlast'].$delim.$row['addr_idstr1'].$delim.$row['addr_idstr2'].$delim.$row['addr_hasgot'].$delim.$row['addr_hasseen'].$delim.$row['addr_various'].$delim.$row['addr_msg'].$delim.$row['addr_statsdata'];
	$export_tabfull .= $linefull."\n";
}
$addr_email_out = implode(';',$addr_emailArr);
?>

<div class="tnl_nlsettings_block tnl_nlsettings_bcc" id="tnl_nlsettings_block_bcc">
<h3><?php echo $MOD_TINY_NEWSLETTER['TEXT_BCC_H'] ; ?></h3>
<p><?php echo $MOD_TINY_NEWSLETTER['TEXT_BCC'] ; ?></p>
<textarea style="width:100%; height:200px;"><?php echo $addr_email_out; ?></textarea>
</div>

<div style="width:100%;" class="tnl_nlsettings_block tnl_nlsettings_texttab" id="tnl_nlsettings_block_texttab">
<h3><?php echo $MOD_TINY_NEWSLETTER['TEXT_TAB_H'] ; ?></h3>
<textarea style="width:100%; height:200px;"><?php echo $export_tab; ?></textarea>
</div>

<div class="tnl_nlsettings_block tnl_nlsettings_import" id="tnl_nlsettings_block_import">
<h3>Import</h3>

<form name="import" method="post" action="import.php">
<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />

<textarea name="tnl_import_short" style="width:100%; height:200px;"></textarea>
<table style="width:90%; max-width:800px;"><tr><td style="width:40%;">
<?=$MOD_TINY_NEWSLETTER['RECORD_DIVIDER']?>:<div>
<input type="radio" checked="checked" name="tnl_delimiter_record" value="" /><?=$MOD_TINY_NEWSLETTER['DIVIDER_LINE']?>&nbsp;&nbsp;&nbsp;
<input type="radio" name="tnl_delimiter_record" value=";" /><?=$MOD_TINY_NEWSLETTER['DIVIDER_SEMICOLON']?> (;)&nbsp;&nbsp;&nbsp;
</div></td>
<td style="width:40%;"><?=$MOD_TINY_NEWSLETTER['ITEM_DIVIDER']?>:<div>
<input type="radio" checked="checked" name="tnl_delimiter_item" value="," /><?=$MOD_TINY_NEWSLETTER['DIVIDER_COMMA']?> (,)&nbsp;&nbsp;&nbsp;
<input type="radio" name="tnl_delimiter_item" value="" /><?=$MOD_TINY_NEWSLETTER['DIVIDER_TAB']?>&nbsp;&nbsp;&nbsp;
</div></td>

<td><input type="submit" value="<?php echo $TEXT['SAVE']; ?>" /></td>
</tr></table>
</form>


<?php
$t2 = time() - 3600;
$sql = "SELECT  COUNT(*) AS zahl FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_msg = 'imported' AND addr_started > $t2";
$res = $database->query($sql);
$row = $res->fetchRow();
$zahl = $row['zahl'];
if ($zahl > 0) {
	echo '<p><a href="'.WB_URL.'/modules/'.$mod_dir.'/admin/import.php?'.$params.$paramdelimiter.'delrecent='.$zahl.'">'.$MOD_TINY_NEWSLETTER['IMPORT_DELETE'].' ('.$zahl.')</a></p>';
} else {
	echo '<p>'.$MOD_TINY_NEWSLETTER['IMPORT_HINT'].'</p>';
}

echo '</div>';


$admin->print_footer();
?>
