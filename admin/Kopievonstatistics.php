<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }


$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');


?>

<script>
	var tnl_tabgroup = 'statistics';
</script>


<h3><?php echo $MOD_TINY_NEWSLETTER['SUBSCRIPTIONS']; ?></h3>
<div style="width: 100%; max-width:500px; position:relative;">
<?php
$oneday = 86400;
$tmin = 0; $tmax=0; $addrArr = array();
$query = "SELECT addr_started FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status > 0 ORDER BY addr_started ASC";
$res = $database->query($query);
if ($res->numRows() < 2) {
echo '<h3>NoData</h3>';
return 0;
}

$sum = $res->numRows();
while ($row = $res->fetchRow()) {	
	$addr_day = floor($row['addr_started'] / $oneday);
	if ($tmin == 0) {$tmin = $addr_day;}
	$tmax = $addr_day;
	if ( isset($addrArr[$addr_day]) ) { $addrArr[$addr_day] += 1; } else {$addrArr[$addr_day] = 1;}
}	
	
$days = $tmax - $tmin;
if ($days < 1) { 
echo '<h3>No x-Axis</h3>';
return 0;
}

$perday = 1000 / $days;
$peraddr = 500 / $sum;
$nowsum  = 0;

//http://hoffmann.bplaced.net/hilfe.php?me=svg&in=svggrundform
//http://hoffmann.bplaced.net/svgueb/zeig.php?was=linie02.svg

$out = '<svg style="width:100%;"  viewBox="0 0 1000 500"
  xmlns="http://www.w3.org/2000/svg" version="1.1"
  xml:lang="de">
<rect x="0" y="0" width="1000" height="500" rx="10" ry="10" fill="rgb(100%,100%,100%)" stroke="rgb(50%,50%,50%)"  stroke-width="0" />
';

//Day lines:
$step = $perday;
if ($step < 10) {$step = 7 * $perday;}
if ($step < 10) {$step = 30.5 * $perday;}
for ($i=0;  $i < 100; $i++) {
	$x = ceil($step * $i);
	if ($x > 1000) {break;}
	$out .= '<line x1="'.$x.'" y1="0" x2="'.$x.'" y2="500" stroke="#cccccc" stroke-width="2"  stroke-opacity="0.6"  />';
}

//sum Lines
$step = $peraddr;
if ($step < 10) {$step = 5 * $peraddr;}
if ($step < 10) {$step = 50;}
for ($i=0;  $i < 100; $i++) {
	$y = 500 - ceil($step * $i);
	if ($y < 0) {break;}
	$out .= '<line x1="0" y1="'.$y.'" x2="1000" y2="'.$y.'" stroke="#cccccc" stroke-width="2"  stroke-opacity="0.6"  />
	';
}



$out .= '
<polyline fill="none" stroke="green" stroke-width="5"  points="';

foreach ($addrArr as $d => $s) {
	$d = ceil($perday * ($d - $tmin));
	//echo '<p>'.$d .'</p>';
	$nowsum = $nowsum + $s;
	$showsum = 500 - ($peraddr * $nowsum);
	
	$out .= ' '.$d.','.$showsum;  	
}

$out .= '" opacity="0.5"  stroke-linecap="round" />   
</svg>';
echo $out;

echo '<div style="position:absolute; top:10px; left:10px;">'.$MOD_TINY_NEWSLETTER['STATS_DAYS'].': '.ceil($days).'<br/>'.$MOD_TINY_NEWSLETTER['STATS_RECS'].': '.$sum.'</div>';
echo '</div>';

//var_dump($addrArr);
?>

<?php echo $MOD_TINY_NEWSLETTER['STATS_INFO']; ?>



<?php

$admin->print_footer();

/*
Muster:


<svg width="500px" height="500px" 
  viewBox="0 0 1000 1000"
  xmlns="http://www.w3.org/2000/svg" version="1.1"
  xml:lang="de">
 
	<title>Grundform: Linie</title> 
	<desc>SVG-Beispiel mit Linien</desc> 
<rect x="5" y="5" width="990" height="990" rx="10" ry="10" fill="rgb(100%,100%,100%)" stroke="rgb(50%,50%,50%)"  stroke-width="0" />

 <polyline fill="none" stroke="green" stroke-width="20" 
            points="900,50 100,300 400,600 500,200" 
	    opacity="0.5"  stroke-linecap="round" /> 
         
</svg>
*/

?>

