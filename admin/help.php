<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }


$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');


?>

<script>
	var tnl_tabgroup = 'help';
</script>

<h2><?php echo $MOD_TINY_NEWSLETTER['HELP']; ?></h2>


<?php
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/'.LANGUAGE.'-help.php');

$admin->print_footer();
?>