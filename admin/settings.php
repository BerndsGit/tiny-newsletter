<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }


$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');
$paramdelimiter = '&amp;';
$params = 'page_id='.$page_id.$paramdelimiter.'section_id='.$section_id;

// Ist das richtig? brauchen wir das?
if (!defined('WYSIWYG_EDITOR') OR WYSIWYG_EDITOR=="none" OR !file_exists(WB_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php')) {
	function show_wysiwyg_editor($name,$id,$content,$width,$height) {
		echo '<textarea name="'.$name.'" id="'.$id.'" rows="30" cols="3" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
	}
} else {
	$id_list=array("confirmation_mail_text");
	require(WB_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
}

$use_captcha = tnl_GetSpecialSettings($settings, 'use_captcha');
if ($use_captcha == '') :
	$q = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_settings SET `property` = 'use_captcha', `value` = 'false'";
	$database->query($q);
endif;
$use_captcha_checked = ($use_captcha == 'true') ? 'checked' : '';
?>

<script>
	var tnl_tabgroup = 'settings';
</script>
<form name="form1" method="post" action="settings_save.php">
<input type="submit" value="<?php echo $TEXT['SAVE']; ?>" />
<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
<input type="hidden" name="lastdone" value="<?php echo (int) tnl_GetSettings($settings, 'lastdone'); ?>" />

<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['newsletter_form_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['newsletter_form_H']; ?></h3>
		<textarea class="tnl_settings_medium" name="newsletter_form"><?php echo tnl_GetSpecialSettings($settings, 'newsletter_form'); ?></textarea>
		<p>
			<input type="hidden" name="use_captcha" value="false">
			<?php echo $MOD_TINY_NEWSLETTER['use_captcha']; ?> <input type="checkbox" name="use_captcha" value="true" <?=$use_captcha_checked?>>
		</p>
	</div>
<div style="clear:both;"></div>
</div>

<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['genderarray_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['genderarray_H']; ?></h3>
<table style="width:100%;">
<tr>
<td style="width:10%;">G</td>
<td style="width:30%;">Select</td>
<td><?php echo $MOD_TINY_NEWSLETTER['GENDER']; ?></td>
</tr>
<tr>

<tr>
<tr>
<td>F</td>
<td><input type="text" name="gender_t-1" id="gender_t-1" value="<?php echo $genderarray['gender_t-1']; ?>" /></td>
<td><input type="text" name="gender_a-1" id="gender_a-1" value="<?php echo $genderarray['gender_a-1']; ?>" /></td>
</tr>
<tr>
<tr>
<td>M</td>
<td><input type="text" name="gender_t-2" id="gender_t-2" value="<?php echo $genderarray['gender_t-2']; ?>" /></td>
<td><input type="text" name="gender_a-2" id="gender_a-2" value="<?php echo $genderarray['gender_a-2']; ?>" /></td>
</tr>
<tr>
<tr class="settings_gender_tr_n">
<td>N</td>
<td><input type="text" name="gender_t-3" id="gender_t-3" value="<?php echo $genderarray['gender_t-3']; ?>" /></td>
<td><input type="text" name="gender_a-3" id="gender_a-3" value="<?php echo $genderarray['gender_a-3']; ?>" /></td>
</tr>
<tr class="settings_gender_tr_nf">
<td>N(F)</td>
<td><input type="text" name="gender_t-4" id="gender_t-4" value="<?php echo $genderarray['gender_t-4']; ?>" /></td>
<td><input type="text" name="gender_a-4" id="gender_a-4" value="<?php echo $genderarray['gender_a-4']; ?>" /></td>
</tr>
<tr class="settings_gender_tr_nm">
<td>N(M)</td>
<td><input type="text" name="gender_t-5" id="gender_t-5" value="<?php echo $genderarray['gender_t-5']; ?>" /></td>
<td><input type="text" name="gender_a-5" id="gender_a-5" value="<?php echo $genderarray['gender_a-5']; ?>" /></td>
</tr>
<tr>
<tr>
<td>?</td>
<td><input type="text" name="gender_t-0" id="gender_t-0" value="-" /></td>
<td><input type="text" name="gender_a-0" id="gender_a-0" value="<?php echo $genderarray['gender_a-0']; ?>" /></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>

<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['confirmation_text_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['confirmation_text_H']; ?></h3>
		<textarea class="tnl_settings_medium" name="confirmation_text"><?php echo tnl_GetSpecialSettings($settings, 'confirmation_text'); ?></textarea>
	</div>
<div style="clear:both;"></div>
</div>




<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['confirmation_various_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['confirmation_various_H']; ?></h3>
		<div class="halfformbox">
		<h4><?php echo $MOD_TINY_NEWSLETTER['confirmation_mail_name']; ?></h4>
		<input type="text" class="tnl_settings_medium" name="confirmation_mail_name" value="<?php echo tnl_GetSpecialSettings($settings, 'confirmation_mail_name'); ?>" />
		</div>
		<div class="halfformbox">
		<h4><?php echo $MOD_TINY_NEWSLETTER['confirmation_mail_email']; ?></h4>
		<input type="text" class="tnl_settings_medium" name="confirmation_mail_email" value="<?php echo tnl_GetSpecialSettings($settings, 'confirmation_mail_email'); ?>" />
		</div><div style="clear:both;"></div>
		<h4><?php echo $MOD_TINY_NEWSLETTER['confirmation_mail_subject']; ?></h4>
		<input type="text" class="tnl_settings_medium" name="confirmation_mail_subject" value="<?php echo tnl_GetSpecialSettings($settings, 'confirmation_mail_subject'); ?>" />
		
	</div>
	<div style="clear:both;"></div>
</div>
<div style="clear:both;">		
	<?php echo $MOD_TINY_NEWSLETTER['confirmation_body_T']; ?>
	
	<!--textarea class="tnl_settings_high" name="confirmation_mail_text"><?php echo tnl_GetSpecialSettings($settings, 'confirmation_mail_text'); ?></textarea-->
	<?php show_wysiwyg_editor("confirmation_mail_text","confirmation_mail_text",tnl_GetSpecialSettings($settings, 'confirmation_mail_text'),"100%","300px","MyToolbar"); ?>
	<script>
	// config the editor toolbar
	CKEDITOR.config.toolbar_MyToolbar = ([
			['Format'],['Bold','Italic','-','RemoveFormat'],['Link','Unlink','Anchor'],['NumberedList','BulletedList'],['Image','oembed','Table','HorizontalRule','SpecialChar','ckawesome'],['Source']
		]);
	CKEDITOR.toolbar = 'MyToolbar';
</script>
</div>

<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['confirmation_finished1_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['confirmation_finished1_H']; ?></h3>
		<textarea class="tnl_settings_medium" name="confirmation_finished1"><?php echo tnl_GetSpecialSettings($settings, 'confirmation_finished1'); ?></textarea>
	</div>
<div style="clear:both;"></div>
</div>

<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['confirmation_removed_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['confirmation_removed_H']; ?></h3>
		<textarea class="tnl_settings_medium" name="confirmation_removed"><?php echo tnl_GetSpecialSettings($settings, 'confirmation_removed'); ?></textarea>
	</div>
<div style="clear:both;"></div>
</div>


<?php //==================================================================================================== ?>
<div class="tnl_divider"></div>
<h2><?php echo $MOD_TINY_NEWSLETTER['all_mails_H']; ?></h2>

<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['default_template_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['default_template_H']; ?></h3>
		<!--textarea class="tnl_settings_medium" name="default_template"><?php echo tnl_GetSpecialSettings($settings, 'default_template'); ?></textarea-->
		<select name="default_template" class="select-field">
		<?php
		$path = WB_PATH.'/modules/'.$mod_dir.'/templates/';
		$dirs = array();
		$directory = new DirectoryIterator($path);
		foreach ($directory as $fileinfo) {
			if ($fileinfo->isDir()) {
				$dn = $fileinfo->getFilename();
				if (strpos($dn,'.') === false) {$dirs[] = $fileinfo->getFilename();}
			}
		}
		$default_template = tnl_GetSpecialSettings($settings, 'default_template');		
		foreach ($dirs as $d) {
			$d = str_replace($path, '', $d);
			if ($d == $default_template) {$sel = ' selected="selected" '; } else {$sel = '';}
			echo '<option '.$sel.' value="'.$d.'">'.$d.'</option>';
		}		
		?>
		
		</select>
	
	
	</div>
<div style="clear:both;"></div>
</div>


<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['newsletter_various_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['newsletter_various_H']; ?></h3>
		<div class="halfformbox">
		<h4><?php echo $MOD_TINY_NEWSLETTER['newsletter_mail_name']; ?></h4>
		<input type="text" class="tnl_settings_medium" name="newsletter_mail_name" value="<?php echo tnl_GetSpecialSettings($settings, 'newsletter_mail_name'); ?>" />
		</div>
		<div class="halfformbox">
		<h4><?php echo $MOD_TINY_NEWSLETTER['newsletter_mail_email']; ?></h4>
		<input type="text" class="tnl_settings_medium" name="newsletter_mail_email" value="<?php echo tnl_GetSpecialSettings($settings, 'newsletter_mail_email'); ?>" />
		</div><div style="clear:both;"></div>
		
		<h4><?php echo $MOD_TINY_NEWSLETTER['newsletter_mail_subject']; ?></h4>
		<input type="text" class="tnl_settings_medium" name="newsletter_mail_subject" value="<?php echo tnl_GetSpecialSettings($settings, 'newsletter_mail_subject'); ?>" />
		
	</div>
	<div style="clear:both;"></div>
</div>



<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['all_mails_appendix_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['all_mails_appendix_H']; ?></h3>
		<textarea class="tnl_settings_medium2" name="all_mails_appendix"><?php echo tnl_GetSpecialSettings($settings, 'all_mails_appendix'); ?></textarea>
	</div>
<div style="clear:both;"></div>

	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['all_mails_appendix_text_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['all_mails_appendix_text_H']; ?></h3>
		<textarea class="tnl_settings_medium2" name="all_mails_appendix_text"><?php echo tnl_GetSpecialSettings($settings, 'all_mails_appendix_text'); ?></textarea>
	</div>
<div style="clear:both;"></div>
</div>


<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['SOME_INTEGERS']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['SOME_INTEGERS2']; ?></h3>
<table style="width:100%;">

<tr>
<td><?php echo $MOD_TINY_NEWSLETTER['intval_block']; ?></td>
<td  style="width:100px;"><input type="text" name="intval_block" value="<?php echo tnl_GetSpecialSettings($settings,'intval_block'); ?>" /></td>
</tr>

<tr>
<td><?php echo $MOD_TINY_NEWSLETTER['intval_block_delay']; ?></td>
<td><input type="text" name="intval_block_delay" value="<?php echo tnl_GetSpecialSettings($settings,'intval_block_delay'); ?>" /></td>
</tr>

<tr>
<td><?php echo $MOD_TINY_NEWSLETTER['intval_receiver_delay']; ?></td>
<td><input type="text" name="intval_receiver_delay" value="<?php echo tnl_GetSpecialSettings($settings,'intval_receiver_delay'); ?>" /></td>
</tr>

</table>
</div>
<div style="clear:both;"></div>
</div>


<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['testmail_adresses_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['testmail_adresses_H']; ?></h3>
		<input name="testmail_adresses" value="<?php echo tnl_GetSpecialSettings($settings, 'testmail_adresses'); ?>" />
	</div>
<div style="clear:both;"></div>
</div>
<div class="tnl_settings_block">
	<div class="tnl_settings_info"><?php echo $MOD_TINY_NEWSLETTER['tracking_element_T']; ?></div>
	<div class="tnl_settings_element">
		<h3><?php echo $MOD_TINY_NEWSLETTER['tracking_element_H']; ?></h3>
		<input name="tracking_element" value="<?php echo tnl_GetSpecialSettings($settings, 'tracking_element'); ?>" />
		<div style="font-size:0.85em"><?php echo $MOD_TINY_NEWSLETTER['tracking_element_typ']; ?></div>
	</div>
<div style="clear:both;"></div>
</div>
<div class="tnl_divider"></div>
<div style="height:20px;clear:both;"></div>

<input type="submit" value="<?php echo $TEXT['SAVE']; ?>" />
</form>


<?php 
/* was soll das bewirken? ich nehms mal raus. -fm
<div class="tnl_footerbutton"><a href="?<?php echo $params.$paramdelimiter; ?>save_lang=1">Save /languages/MY-settings.php</a></div> */

if ( isset($_GET['save_lang']) AND $_GET['save_lang'] == 1 ) {

	$settings['confirmation_mail_name'] = '';
	$settings['confirmation_mail_email'] = '';
	$settings['newsletter_mail_name'] = '';
	$settings['newsletter_mail_email'] = '';
	$settings['testmail_adresses'] = '';
	

	$value = '<?php 
	$settingsArr = \''.json_encode($settings).'\';
	?>';
	$p =  WB_PATH.'/modules/'.$mod_dir.'/languages/MY-settings.php';
	file_put_contents ($p, $value);
}

/*
// Say that a new record has been added, then redirect to modify page
if ($database->is_error()) {
	$admin->print_error($database->get_error(), WB_URL.'/modules/'.$mod_name.'/modify_item.php?page_id='.$page_id.'&section_id='.$section_id.'&item_id='.$item_id);
} else {
	$admin->print_success($TEXT['SUCCESS'], WB_URL.'/modules/'.$mod_name.'/modify_item.php?page_id='.$page_id.'&section_id='.$section_id.'&item_id='.$item_id.'&from=add_item');
}
*/

// Print admin footer
$admin->print_footer();
?>
