<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }


$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');
// include jscalendar-setup
$jscal_use_time = true; // whether to use a clock, too
require_once(WB_PATH."/include/jscalendar/wb-setup.php");

?>
<script>
	var tnl_tabgroup = 'newsletters';
</script>
<style>
	<?php require_once(WB_PATH.'/include/jscalendar/calendar-system.css'); ?>
</style>

<?php

$t = time();
$editbase = WB_URL.'/modules/'.$mod_dir.'/admin/newsletters.php?'.$params.$paramdelimiter.'tnl_id=';

//Settings an Template:
require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');
$settings = tnl_LoadSettings ($tablename);
$default_template = tnl_GetSettings($settings, 'default_template');
if ($default_template == '') {$default_template = 'default';}


//Add a new newsletter?
if ( isset($_GET['tnl_id'])AND $_GET['tnl_id'] == 'add') {
	$tnl_subject = tnl_GetSettings($settings, 'newsletter_mail_subject');
	if ($tnl_subject == '') {$tnl_subject = 'No Default-Subject defined';}
	$tnl_published_when = $t;
	$tnl_published_until = $t + (3600 * 24 * 15);

	$sql = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_newsletters (tnl_subject, tnl_body, tnl_published_when, tnl_published_until) VALUES ('$tnl_subject','','$tnl_published_when', '$tnl_published_until')";
	$query = $database->query($sql);
	$last_insert = $database->get_one("SELECT LAST_INSERT_ID()");
	$tnl_id = $last_insert;

	$topl =  WB_URL.'/modules/'.$mod_dir.'/admin/newsletters.php?page_id='.$page_id.'&section_id='.$section_id.'&tnl_id='.$tnl_id;
	echo '
	<script>
	top.location.href="'.$topl.'";
	</script>
	';
}


if ( isset($_GET['tnl_id']) AND is_numeric($_GET['tnl_id']) AND $_GET['tnl_id'] > 0) {
	//Single Newsletter Edit

	$tnl_id = (int)  $_GET['tnl_id'];
	$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters WHERE tnl_id = $tnl_id";

	$res = $database->query($sql);
	$row = $res->fetchRow();
	$tnl_status = (int) $row['tnl_status'];
	$tnl_min_active = (int) $row['tnl_min_active'];

	$tnl_published_when =  (int) $row['tnl_published_when'];
	$tnl_published_until =  (int) $row['tnl_published_until'];
	$tnl_subject = $row['tnl_subject'];
	$tnl_body = $row['tnl_body'];
	$tnl_body_text = $row['tnl_body_text'];

	$tnl_template_data_Arr = json_decode($row['tnl_template_data'], true);
	if (!is_array($tnl_template_data_Arr)) {$tnl_template_data_Arr = array(); }



	// Ist das richtig? brauchen wir das?
	if (!defined('WYSIWYG_EDITOR') OR WYSIWYG_EDITOR=="none" OR !file_exists(WB_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php')) {
		function show_wysiwyg_editor($name,$id,$content,$width,$height) {
			echo '<textarea name="'.$name.'" id="'.$id.'" rows="30" cols="3" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
		}
	} else {
		$id_list=array("confirmation_mail_text");
		require(WB_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
	}

	?>
	<form class="tnl_min_active<?php echo $tnl_min_active; ?>" name="modify" id="modify" method="post" action="newsletters_save.php">
	<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
	<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
	<input type="hidden" name="tnl_id" value="<?php echo $tnl_id; ?>" />
	<!--input type="hidden" name="tnl_status" value="<?php echo $tnl_status; ?>" /-->




	<?php // Content ====================================================================== ?>

	<div style="clear:both;">
	<table style="width:99%"><tr>
	<td><?php echo $MOD_TINY_NEWSLETTER['NL_SUBJECT']?>:<br/><input type="text" id="tnl_subject" name="tnl_subject" value="<?php echo htmlspecialchars($tnl_subject); ?>" /></td>
	<td style="width:90px;"><?php echo $MOD_TINY_NEWSLETTER['NL_GROUPS']?>:<br/>
	<select style="width:80px;" name="tnl_min_active" class="select-field"  onchange="tnl_min_active_changed();">
		<option value="1"><?php echo $MOD_TINY_NEWSLETTER['NL_min_statusArr'][0]; ?></option>
		<option <?php if ($tnl_min_active == 3) echo 'selected="selected" '; ?>value="3"><?php echo $MOD_TINY_NEWSLETTER['NL_min_statusArr'][2]; ?></option>
		<option <?php if ($tnl_min_active == 4) echo 'selected="selected" '; ?>value="4"><?php echo $MOD_TINY_NEWSLETTER['NL_min_statusArr'][3]; ?></option>
	</select>
	</td>
	<td style="width:90px;">Status:<br/>
	<select style="width:80px;" name="tnl_status" class="select-field"  onchange="tnl_status_changed();">
		<option value="0"><?php echo $MOD_TINY_NEWSLETTER['NL_statusArr'][0]; ?></option>
		<!--option <?php if ($tnl_status == 1) echo 'selected="selected" '; ?>value="1"><?php echo $MOD_TINY_NEWSLETTER['NL_statusArr'][1]; ?></option-->
		<option <?php if ($tnl_status == 2) echo 'selected="selected" '; ?>value="2"><?php echo $MOD_TINY_NEWSLETTER['NL_statusArr'][2]; ?></option>
		<option <?php if ($tnl_status == 3) echo 'selected="selected" '; ?>value="3"><?php echo $MOD_TINY_NEWSLETTER['NL_statusArr'][3]; ?></option>
	</select>
	</td>
	<td style="width:90px;">&nbsp;<br/>
	<input style="float:right;" type="submit" onclick="return confirm('<?php echo $MOD_TINY_NEWSLETTER['CONFIRM_SAVE_NEWSLETTER']?>');" value="<?php echo $TEXT['SAVE']; ?>" />
	</td>
	</tr></table>
		<p  style="margin-bottom:0;"><?php echo $MOD_TINY_NEWSLETTER['NL_EDITHINT']; ?></p>

		<!--textarea class="tnl_settings_high" name="confirmation_mail_text"><?php echo $tnl_body; ?></textarea-->
		<?php show_wysiwyg_editor("tnl_body","tnl_body",htmlspecialchars($tnl_body),"80%","300px","MyToolbar"); ?>
	</div>
	<div style="clear:both; height:20px;"></div>



	<?php //Settings ====================================================================== ?>
	<div style="clear:both;"></div>
	<a href="#" class="tnl_nlsettings_tab tnl_tab_time" onclick="tnl_show_nlsettings_block('time');return false;"><?php echo $MOD_TINY_NEWSLETTER['NL_TIMER']?></a>
	<a href="#" class="tnl_nlsettings_tab tnl_tab_template" onclick="tnl_show_nlsettings_block('template');return false;"><?php echo $MOD_TINY_NEWSLETTER['N_TEMPLATE_SETTINGS']?></a>
	<a href="#" class="tnl_nlsettings_tab tnl_tab_text" onclick="tnl_show_nlsettings_block('text');return false;"><?php echo $MOD_TINY_NEWSLETTER['NL_TEXT_ONLY']?></a>
	<a href="#" class="tnl_nlsettings_tab tnl_tab_testing" onclick="tnl_show_nlsettings_block('testing');return false;"><?php echo $MOD_TINY_NEWSLETTER['NL_PREVIEW']?></a>
	<a href="#" class="tnl_nlsettings_tab tnl_tab_archive" onclick="tnl_show_nlsettings_block('archive');return false;"><?php echo $MOD_TINY_NEWSLETTER['NL_ARCHIVE']?></a>
	<div style="clear:both;"></div>

	<table class="tnl_nlsettings_block tnl_nlsettings_time" id="tnl_nlsettings_block_time" ><tr>


	<td style="width: 180px; background:#eff;">
	Start:<br/><input type="text" id="publishdate" name="publishdate" value="<?php if($tnl_published_when==0) echo $tnl_published_when; else print date($jscal_format, $tnl_published_when);?>" style="width: 120px;" />
	<img src="<?php echo THEME_URL ?>/images/clock_16.png" id="publishdate_trigger" style="cursor: pointer;" title="<?php echo $TEXT['CALENDAR']; ?>" onmouseover="this.style.background='lightgrey';" onmouseout="this.style.background=''" alt="" />
	<img src="<?php echo THEME_URL ?>/images/clock_del_16.png" style="cursor: pointer;" title="<?php echo $TEXT['DELETE_DATE']; ?>" onmouseover="this.style.background='lightgrey';" onmouseout="this.style.background=''" onclick="document.modify.publishdate.value=''" alt=""/>
	<div class="time-help"><?php echo $MOD_TINY_NEWSLETTER['TIME_HINT_START']; ?></div>
	</td>

	<td style="width: 180px; background:#eef;">
	Stop:<br/>
	<input type="text" id="enddate" name="enddate" value="<?php if($tnl_published_until==0) print ""; else print gmdate($jscal_format, $tnl_published_until)?>" style="width: 120px;" />
	<img src="<?php echo THEME_URL ?>/images/clock_16.png" id="enddate_trigger" style="cursor: pointer;" title="<?php echo $TEXT['CALENDAR']; ?>" onmouseover="this.style.background='lightgrey';" onmouseout="this.style.background=''" alt=""/>
	<img src="<?php echo THEME_URL ?>/images/clock_del_16.png" style="cursor: pointer;" title="<?php echo $TEXT['DELETE_DATE']; ?>" onmouseover="this.style.background='lightgrey';" onmouseout="this.style.background=''" onclick="document.modify.enddate.value=''" alt=""/>
	<div class="time-help"><?php echo $MOD_TINY_NEWSLETTER['TIME_HINT_STOP']; ?></div>
	</td>
	</table><!-- //table class="tnl_nlsettings_tab tnl_nlsettings_time" id="tnl_nlsettings_tab_time" -->




	<div class="tnl_nlsettings_block tnl_nlsettings_template" id="tnl_nlsettings_block_template" >
	<p>Template: <b><?php echo $default_template; ?></b></p>
	<?php
	$inc = WB_PATH.'/modules/'.$mod_dir.'/templates/'.$default_template.'/template_settings.php';
	if (file_exists($inc)) {require_once($inc);} else {echo '<p>'.$MOD_TINY_NEWSLETTER['NO_TEMPLATE_SETTINGS'].'</p>';}
	?>
	</div>


	<div class="tnl_nlsettings_block tnl_nlsettings_text" id="tnl_nlsettings_block_text" >
	<textarea name="tnl_body_text" style="padding:5px; height:150px; width:98%"><?php echo $tnl_body_text ?></textarea>


	</div>





	<div class="tnl_nlsettings_block tnl_nlsettings_testing" id="tnl_nlsettings_block_testing">
	<a class="tnl_button" target="_blank" href="../show.php?tnl_id=<?php echo $tnl_id; ?>"><?php echo $MOD_TINY_NEWSLETTER['NL_PREVIEW']?></a>
	<a class="tnl_button" target="_blank" href="../show.php?tnl_id=<?php echo $tnl_id; ?>&amp;mode=previewtext"><?php echo $MOD_TINY_NEWSLETTER['NL_TEXT_ONLY_PREVIEW']?></a>
	<!--a class="tnl_button" target="_blank" href="../show.php?tnl_id=<?php echo $tnl_id; ?>&amp;mode=testmail">Testmail</a-->
	<a class="tnl_button" href="#" onclick="tnl_send_testmail(<?php echo $tnl_id; ?>); return false;"><?php echo $MOD_TINY_NEWSLETTER['NL_TEST_MAIL']?></a>

	<div id="tnl_testing_message"></div>

	</div>




<!-- Die Archivfunktion ist ausgeblendet, weil es noch nicht m�glich ist, unterschiedliche Templates pro Newsletter zu speichern. -->

	<div class="tnl_nlsettings_block tnl_nlsettings_archive" id="tnl_nlsettings_block_archive">
	<?php
	$tnl_archived = tnl_get_archived_time ($tablename, $tnl_id);
		if ($tnl_archived == 0) {
			$data = '<span class="tnl_not_achived">'.$MOD_TINY_NEWSLETTER['NL_NOT_ARCHIVED'].'</span>';
		} else {
			$temp = str_replace('[ARCHIVED_TIME]',gmdate(DATE_FORMAT, $tnl_archived),$MOD_TINY_NEWSLETTER['NL_ARCHIVED']);
			$data = '<span class="tnl_achived">'.$temp.'</span>';
		}
		echo '<p id="tnl_is_achived_message">'.$data.'</p>';
		?>
		<a class="tnl_button" href="#" onclick="tnl_make_archive(<?php echo $tnl_id; ?>); return false;"><?php echo $MOD_TINY_NEWSLETTER['NL_ARCHIVE']; ?></a>
		<?php if ($tnl_archived  != 0) {
		echo '<a class="tnl_button" href="#" onclick="tnl_un_archive('.$tnl_id.'); return false;">'.$MOD_TINY_NEWSLETTER['NL_UNARCHIVE'].'</a>';
		} ?>
		<div id="tnl_archive_message"></div>
	</div>


	<div style="height:20px; clear:both"></div>
	<a class="tnl_delete_button" style="display:block;  float:right;" href="<?php echo WB_URL.'/modules/'.$mod_dir.'/admin/newsletters.php?'.$params.$paramdelimiter.'delete='.$tnl_id; ?>" onclick="return confirm('<?php echo $MOD_TINY_NEWSLETTER['CONFIRM_DELETE_NEWSLETTER']?>');"><span class="fa fa-fw fa-trash"></span><?php echo $TEXT['DELETE']?></a>
	<?php // Content ====================================================================== ?>

	<input type="submit"  onclick="return confirm('<?php echo $MOD_TINY_NEWSLETTER['CONFIRM_SAVE_NEWSLETTER']?>');" value="<?php echo $TEXT['SAVE']; ?>" />
	<input type="hidden" name="doduplicate" id="doduplicate" value="0" />
	<input name="duplicate" type="submit" value="<?php echo $MOD_TINY_NEWSLETTER['SAVE_AND_DUPLICATE']?>" style="margin-left:20px" onclick="document.getElementById('doduplicate').value = '1'; return confirm('<?php echo $MOD_TINY_NEWSLETTER['CONFIRM_SAVE_NEWSLETTER']?>');"/>

	</form>


	<script type="text/javascript">
	Calendar.setup(
		{
			inputField  : "publishdate",
			ifFormat    : "<?php echo $jscal_ifformat ?>",
			button      : "publishdate_trigger",
			firstDay    : <?php echo $jscal_firstday ?>,
			<?php if(isset($jscal_use_time) && $jscal_use_time==TRUE) { ?>
				showsTime   : "true",
				timeFormat  : "24",
			<?php } ?>
			date        : "<?php echo $jscal_today ?>",
			range       : [1970, 2037],
			step        : 1
		}
	);
	Calendar.setup(
		{
			inputField  : "enddate",
			ifFormat    : "<?php echo $jscal_ifformat ?>",
			button      : "enddate_trigger",
			firstDay    : <?php echo $jscal_firstday ?>,
			<?php if(isset($jscal_use_time) && $jscal_use_time==TRUE) { ?>
				showsTime   : "true",
				timeFormat  : "24",
			<?php } ?>
			date        : "<?php echo $jscal_today ?>",
			range       : [1970, 2037],
			step        : 1
		}
	);

	// config the editor toolbar
	CKEDITOR.config.toolbar_MyToolbar = ([
			['Format'],['Bold','Italic','-','RemoveFormat'],['Link','Unlink','Anchor'],['NumberedList','BulletedList'],['Image','oembed','Table','HorizontalRule','SpecialChar','ckawesome'],['Source']
		]);
	CKEDITOR.toolbar = 'MyToolbar';

</script>



<?php
} else {
//================================================================================================================
//delete a newsletter?:
	if ( isset($_GET['delete']) AND is_numeric($_GET['delete']) ) {
		$delete = (int) $_GET['delete'];

		$sql = "SELECT addr_id, addr_hasgot FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_hasgot LIKE '%,$delete,%'";
		//die($sql);
		$res = $database->query($sql);
		if ( $res->numRows() > 0) {
			echo '<h3>'.$MOD_TINY_NEWSLETTER['NL_CANNOT_DELETE'].'</h3>';
		} else {
			$sql = "DELETE FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters WHERE tnl_id = $delete LIMIT 1";
			$database->query($sql);
		}
	}
//================================================================================================================

	$tnl_limit = 20; $listing_style = 'newsletter';
	require WB_PATH.'/modules/'.$mod_dir.'/inc/newsletter-list.inc.php';

}
?>


<div class="tnl_dev_only tnl_button"><a target="_blank" href="<?php echo WB_URL.'/modules/'.$mod_dir.'/do.php?simulation=1'; ?>">Open do.php?simulation=1</a></div>
<?php
if ( isset($_GET['testreceivers']) AND $_GET['testreceivers'] == 1 ) {
//include WB_PATH.'/modules/'.$mod_dir.'/inc/testdata.inc.php';
}


$admin->print_footer();
?>
