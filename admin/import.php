<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

function tnl_GetNameFromEmail($addr_email) {
	$p = strpos($addr_email,'@');
	$addr_name = substr($addr_email, 0, $p);
	if ($addr_name == 'office') {
		return substr($addr_email, $p+1, strlen($addr_email));
	}

	$addr_name = str_replace('.',' ',$addr_name);
	$arr = explode(' ',$addr_name);
	$nn = '';
	foreach ($arr as $n) { $nn .= ' '.ucfirst($n); }
	return trim($nn);
}




$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');

$typeArr = array('-','F','M','N','Nf','Nm','Nn');

//su@example.com; Ted <ted@example.com>; Mako, Ina <ina.mako@example.com>

$sql = "SELECT addr_email FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status > 0 AND addr_idstr2 <> '' AND addr_email NOT LIKE '%.test' ORDER BY addr_started DESC";
$res = $database->query($sql);
$addr_emailArr = array();
while ( $row = $res->fetchRow() ) {
	$addr_emailArr[] = $row['addr_email'];
}

$t = time();
$errorArr = array();
$okArr = array();
$tnl_delimiter_record = "\n";
if ( isset($_POST['tnl_delimiter_record']) AND strlen($_POST['tnl_delimiter_record']) == 1 ) { $tnl_delimiter_record = $_POST['tnl_delimiter_record']; }

$tnl_delimiter_item = "\t";
if ( isset($_POST['tnl_delimiter_item']) AND strlen($_POST['tnl_delimiter_item']) == 1 ) { $tnl_delimiter_item = $_POST['tnl_delimiter_item']; }




if ( isset($_POST['tnl_import_short']) AND strlen($_POST['tnl_import_short']) > 5 ) {
	$tnl_import_short_lines = explode($tnl_delimiter_record, $_POST['tnl_import_short']);
	foreach ($tnl_import_short_lines as $line) {
		//if (strpos($line,"\t") === false) continue;
		$line = trim($line);
		$lineArr = explode($tnl_delimiter_item, $line);
		$items = count($lineArr);
		//echo '$items:'.$items.'</br>';
		if ($items < 1) {
			echo '<p>Wrong number of items per line ('.count($lineArr).')</p>';
			continue;
		}

		$addr_email =trim($lineArr[0]);
		$addr_email_test = filter_var($addr_email, FILTER_SANITIZE_EMAIL);
		if ($addr_email != $addr_email_test OR substr_count($addr_email,'@')!= 1 ) {
			$errorArr[] = 'Wrong addr_email: '.$addr_email;
			continue;
		}
		if (in_array($addr_email, $addr_emailArr)) {
			$errorArr[] = 'eMail exists, continue; '.$addr_email;
			continue;
		}

		$addr_name = '';
		$addr_type = 0;

		if ($items > 1) { //auch Name
			$addr_name = addslashes(trim($lineArr[1]));

			if ($items > 2) { //auch Geschlecht
				$addr_type = trim($lineArr[2]);
				//echo '$addr_type:'.$addr_type.'</br>';

				if (in_array($addr_type, $typeArr)) {
					$position = array_search($addr_type, $typeArr);
					//echo '$position:'.$position.'</br>';
					$addr_type = $position;
				}
			}
		} else {
			$addr_name = tnl_GetNameFromEmail($addr_email);
		}


		$addr_idstr1 = tnl_GenerateRandomString();
		$addr_idstr2 = tnl_GenerateRandomString();

		$sql = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_started ='$t', addr_status = 1, addr_type = '$addr_type', addr_email = '$addr_email', addr_name = '$addr_name', addr_idstr1 = '$addr_idstr1', addr_idstr2 = '$addr_idstr2', addr_msg = 'imported'";
		$res = $database->query($sql);

		$okArr[] = $addr_email.$tnl_delimiter_item.$addr_name.$tnl_delimiter_item.$addr_type;

	}
	echo '<div class="import_ok"><h3>'.$MOD_TINY_NEWSLETTER['IMPORTED'].': '.count($okArr).'</h3><p>'.implode('<br />',$okArr).'</p></div>';
	echo '<div class="import_error"><h3>'.$MOD_TINY_NEWSLETTER['IMPORT_ERROR'].': '.count($errorArr).'</h3><p>'.implode('<br />',$errorArr).'</p></div>';

} else {
	if (!isset($_GET['delrecent'])) {
		echo '<div class="import_error"><h3>'.$MOD_TINY_NEWSLETTER['NOTHING_TO_IMPORT'] .'</h3></div>';
	}
}


$t2 = time() - 3600;
$sql = "SELECT  COUNT(*) AS zahl FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_msg = 'imported' AND addr_started > $t2";
$res = $database->query($sql);
$row = $res->fetchRow();
$zahl = $row['zahl'];
if ($zahl > 0) {
	//ein kleiner Schutz: die Zahl muss uebereinstimmen:
	if ( isset($_GET['delrecent'])AND $_GET['delrecent'] == $zahl) {
		$sql = "DELETE FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_msg = 'imported' AND addr_started > $t2";
		$res = $database->query($sql);
		echo '<p><b>'.$zahl.' '.$MOD_TINY_NEWSLETTER['IMPORTED_DELETED'].'.</b></p>';
	} else {
		echo '<p><a href="'.WB_URL.'/modules/'.$mod_dir.'/admin/import.php?'.$params.$paramdelimiter.'delrecent='.$zahl.'">'.$MOD_TINY_NEWSLETTER['DELETE_IMPORTED'].' ('.$zahl.')</a></p>';
	}
}

echo '<p><a href="'.WB_URL.'/modules/'.$mod_dir.'/admin/export.php?'.$params.'">'.$TEXT['BACK'].'</a></p><br clear="all" />';

?>
<script>
var tnl_tabgroup = 'export';
</script>
<?php

$admin->print_footer();
?>
