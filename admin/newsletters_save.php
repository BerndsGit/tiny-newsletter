<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');

require_once(WB_PATH."/include/jscalendar/jscalendar-functions.php");

if ( isset($_POST['tnl_id']) AND is_numeric($_POST['tnl_id']) AND $_POST['tnl_id'] > 0) {
	$tnl_id = (int)  $_POST['tnl_id'];
	$tnl_status = (int)  $_POST['tnl_status'];
	$tnl_min_active = (int)  $_POST['tnl_min_active'];
	
	$tnl_subject = $admin->add_slashes($_POST['tnl_subject']); 
	$tnl_body = $admin->add_slashes($_POST['tnl_body']);
	$tnl_body_text = $admin->add_slashes(strip_tags($_POST['tnl_body_text']));
	
	//var_dump($_POST['tnl_template_data']);
	//die();
	//die('$tnl_backlog'.$tnl_backlog);	
	$tnl_published_when = jscalendar_to_timestamp($admin->get_post_escaped('publishdate'));
	$tnl_published_until = jscalendar_to_timestamp($admin->get_post_escaped('enddate'));
	
	//tnl_template_data
	$sql = "SELECT tnl_template_data FROM ".TABLE_PREFIX."mod_".$tablename. "_newsletters WHERE tnl_id = '$tnl_id'";
	$query = $database->query($sql);
	$row = $query->fetchRow();
	$tnl_template_data_Arr = json_decode($row['tnl_template_data'], true);
	if (!is_array($tnl_template_data_Arr)) {$tnl_template_data_Arr = array(); }
	if ( isset($_POST['tnl_template_data']) ) {
		foreach ($_POST['tnl_template_data'] as $k => $v) {
			$tnl_template_data_Arr[$k] = $v;
			//if (array_key_exists ( $k , $tnl_template_data ) ) {$tnl_template_data[$k] = $v;} else {$tnl_template_data[$k] = $v;}		
		}
	} else {
		$tnl_template_data_Arr = array();
	}
	$tnl_template_data = $admin->add_slashes(json_encode($tnl_template_data_Arr));
	//echo $out;
	//die();

	
	
	// Update row
	$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename. "_newsletters  SET tnl_subject = '$tnl_subject', tnl_body = '$tnl_body', tnl_body_text = '$tnl_body_text', tnl_published_when = '$tnl_published_when', tnl_published_until = '$tnl_published_until',  tnl_status = '$tnl_status',  tnl_min_active = '$tnl_min_active', tnl_template_data = '$tnl_template_data'  WHERE tnl_id = '$tnl_id'";
	//echo $sql;
	$database->query($sql);
	
	
	if ( isset($_POST['doduplicate']) AND $_POST['doduplicate'] == 1) {
		// Insert row
		$t = time(); $t2 =  time() + (24*3600*15);
		$sql = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename. "_newsletters  SET tnl_subject = 'COPY: $tnl_subject', tnl_body = '$tnl_body', tnl_body_text = '$tnl_body_text', tnl_published_when = '$t', tnl_published_until = '$t2',  tnl_status = '0', tnl_min_active = 1, tnl_template_data = '$tnl_template_data'";
		//echo $sql;
		$database->query($sql);
		$tnl_id = $database->get_one("SELECT LAST_INSERT_ID()");	
	}
	
	
	//die();
}

$modifyurl = WB_URL.'/modules/'.$mod_dir.'/admin/newsletters.php?page_id='.$page_id.'&section_id='.$section_id.'&tnl_id='.$tnl_id;

// Check if there is a db error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), $modifyurl);
} else {
	$admin->print_success($TEXT['SUCCESS'], $modifyurl);	
}
$admin->print_footer();
?>