<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
$admin = new admin();

$page_id = $admin->get_get('page_id');
$section_id = $admin->get_get('section_id');

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

//build the salutations array
$genderarray = $database->get_one("SELECT `value` FROM ".TABLE_PREFIX."mod_".$tablename."_settings WHERE `property` = 'genderarray'");
$aGenderTmp = json_decode($genderarray, true);

$aSalutations = ['-','F','M','N'];
$aGender = [];
foreach ($aGenderTmp as $k=>$v) :
	if (strstr($k, '_a-')) continue;
	if ($v == '') continue;
	$i = substr($k, -1);
	if ($v == '-') $v = 'neutral';
	$aGender[$i]['name'] = $v;
	$aGender[$i]['val'] = $i;
	$aGender[$i]['short'] = $aSalutations[$i];
endforeach;
?>

<h3><?=$MOD_TINY_NEWSLETTER['ADD_RECEIVER_HEADER']?></h3>
<form method="post" action="receivers_save.php">

	<input type="hidden" name="page_id" value="<?=$page_id?>">
	<input type="hidden" name="section_id" value="<?=$section_id?>">
	<input type="hidden" name="what" value="insert">

	<table>
		<tr>
			<td><?=$TEXT['NAME']?></td>
			<td><input type="text" name="addr_name" required></td>
		</tr>
		<tr>
			<td><?=$TEXT['EMAIL']?></td>
			<td><input type="email" name="addr_email" required></td>
		</tr>
		<tr>
			<td><?=$MOD_TINY_NEWSLETTER['SALUTATION']?></td>
			<td>
				<select name="addr_type">
					<?php foreach ($aGender as $gender) : ?>
						<option value="<?=$gender['val']?>">
							<?=$gender['name']?> (<?=$gender['short']?>)
						</option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div class="mail-msg" style="color:firebrick; font-weight:bold; display:none;"></div>
				<button type="submit" id="add-submit-btn" style="margin-top:20px;"><?=$TEXT['SAVE']?></button>
			</td>
		</tr>
	</table>

</form>

<script>
	$(document).ready(function() {
		$('form').submit(function(e) {
			e.preventDefault();
			url = '<?=WB_URL?>/modules/<?=$mod_dir?>/admin/receivers_save.php';
			console.log(url);
			post_data = {
				what 		: 'precheck',
				addr_email	: $('input[name=addr_email]').val(),
			};
			console.log(post_data);
			$.post(url, post_data, function(response){
				console.log(response);
				if (response.type == 'error') {
					$('.mail-msg').html(response.text).show();
					$('input[name=addr_email]').select();
				} else {
					$('form').unbind('submit');
					$('#add-submit-btn').click();
				}
			}, 'json');
		});

		$('input[name=addr_email]').keyup(function() {
			$('.mail-msg').html('').hide();
		});

	});
</script>
