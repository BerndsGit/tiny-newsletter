<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
require_once(WB_PATH . '/modules/'.$mod_dir.'/admin/permissioncheck.php');

//var_dump($settings);
//die();
$genderarray = array();
$intval_array = array();
foreach ($_POST as $property=>$value) {
	if ($property == 'section_id') continue;
	if ($property == 'page_id') continue;

	if (substr($property,0,7) == 'gender_') {
		$genderarray[$property] = $value;
		continue;
	}

	if (substr($property,0,7) == 'intval_') {
		$value = (int) $value;
	}

	$value = "'".$admin->add_slashes(trim(($value)))."'";
	if (isset($settings[$property])) {
		$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = $value WHERE property = '$property'";
	} else {
		$sql = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_settings SET value = $value, property = '$property'";
	}
	//echo $sql.'<br/>';
	$database->query($sql);


}

$value = json_encode($genderarray);
$value = $admin->add_slashes($value); //slashes dazu
if (isset($settings['genderarray'])) {
	$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$value' WHERE property = 'genderarray'";
} else {
	$sql = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$value', property = 'genderarray'";
}
//echo $sql.'<br/>';
$database->query($sql);

?>


<?php

// Say that a new record has been added, then redirect to modify page
if ($database->is_error()) {
	$admin->print_error($database->get_error(), WB_URL.'/modules/'.$mod_dir.'/admin/settings.php?page_id='.$page_id.'&section_id='.$section_id);
} else {
	$admin->print_success($TEXT['SUCCESS'], WB_URL.'/modules/'.$mod_dir.'/admin/settings.php?page_id='.$page_id.'&section_id='.$section_id);
}


// Print admin footer
$admin->print_footer();
?>
