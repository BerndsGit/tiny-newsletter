<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;

require(WB_PATH.'/modules/admin.php');
$admin = new admin('Modules', 'module_view', false, false);
$addr_id = $admin->get_get('id');
$page_id = $admin->get_get('page_id');
$section_id = $admin->get_get('section_id');

$query = "DELETE FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_id = ".$addr_id;
$database->query($query);

if ($database->is_error()) {
	$admin->print_error($database->get_error(), WB_URL.'/modules/'.$mod_dir.'/admin/receivers.php?page_id='.$page_id.'&section_id='.$section_id);
} else {
	$admin->print_success($TEXT['DELETED'], WB_URL.'/modules/'.$mod_dir.'/admin/receivers.php?page_id='.$page_id.'&section_id='.$section_id);
}

// Print admin footer
$admin->print_footer();

