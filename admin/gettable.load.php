<?php
//http://localhost/2017/_DEV/wbce-1.1.10/modules/wbce_newsletter/admin/gettable.load.php?u=de%20da&m=medi%20die&a=35fg

//===========================================================================
//Typical Admin Head:
require('../../../config.php');
if(!defined('WB_PATH')) { 	die("sorry, no access..");}
$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;
$tsrs_frontendedit = false;

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

if (!class_exists('admin', false)) {
	include(WB_PATH.'/framework/class.admin.php');
}	
$admin = new admin('Pages', 'pages_modify', false, true);
$page_id = intval((isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : 0));



if (!($admin->is_authenticated() && $admin->get_permission($mod_dir, 'module'))) {
    echo '0|no rights to execute requested module ['.$mod_dir.']';
} else {
	$tsrs_frontendedit = true;
}



require_once(WB_PATH."/framework/functions.php");
//===========================================================================

$addr_id =    isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
$addr_name =  isset($_REQUEST['addr_name'])   ? $_REQUEST['addr_name'] : '';
$addr_email = isset($_REQUEST['addr_email'])   ? $_REQUEST['addr_email'] : '';
$addr_status =     isset($_REQUEST['addr_status']) ? $_REQUEST['addr_status'] : '-';
if ($addr_status !== '') {$addr_status = intval($addr_status);}
$gender =     isset($_REQUEST['gender']) ? intval($_REQUEST['gender']) : 0;
$auth = 	  isset($_REQUEST['auth'])   ? $_REQUEST['auth'] : '';

$typeArr = array('-','F','M','N','Nf','Nm','Nn');

$searchArr = array();
//Sanitize input data using PHP filter_var().
if ($addr_name != '')  { $searchArr['addr_name'] = explode(' ',$addr_name);	}
if ($addr_email != '') { $searchArr['addr_email'] = explode(' ',$addr_email); }
if ($auth != '') { $searchArr['addr_idstr2'] = explode(' ',$auth); }

$query = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs ";
//Filter?
$qstr = 'WHERE';
foreach ($searchArr as $k => $vArr) {
	if (count($vArr) > 1) {
		foreach ($vArr as $v) { $v=addslashes($v); $qstr .= "AND $k LIKE  '%$v%' "; }		
	} else {
	 	$qstr .= 'AND '.$k.' LIKE \'%'.$v=addslashes($vArr[0]).'%\' ';	
	}
}

if ($addr_status !== '') {	$qstr .= 'AND addr_status = '.$addr_status.' ';}
if ($gender != 0) {	$qstr .= 'AND addr_type = '.$gender.' ';}

if ($qstr != 'WHERE') {
	$qstr = str_replace('WHEREAND', 'WHERE', $qstr);
	$query .= $qstr;
}


$query .= " ORDER BY addr_started DESC LIMIT 50";
$outp = '';
$t = time();
$res = $database->query($query);
while ($row = $res->fetchRow()) {	
	$addr_id = $row['addr_id'];
	$d = ceil(($row['addr_started'] - $t) / 86400);
	
	$datablock = $row['addr_idstr1']; $datablockclass = '';
	if ($row['addr_idstr2'] != '') {$datablock .= ' | '.$row['addr_idstr2']; $datablockclass = 'confirmed';}
	$datablock .= ' | '.gmdate(DATE_FORMAT, $row['addr_gotlast']);	
	$datablock = '<div class="addr_data '.$datablockclass.'" title="'.$datablock.'">&nbsp;</div>';
	
	
	if ($row['addr_various'] == '') {
		$addr_various = '';
		$srnw = 0;
	} else {	
		$addr_various = json_decode($row['addr_various'], true);			
		$srnw = (int) $addr_various['srnw'];		
	}
	$addr_status = $row['addr_status'];
	$trclass='addr_status'.$addr_status;
	
	$addr_has_block = '<div class="addr_has_block addr_hasgot">'.$row['addr_hasgot'].'</div><div class="addr_has_block addr_hasseen">'.$row['addr_hasseen'].'</div><div class="addr_has_block addr_various">'.$srnw.'</div>';
	
	
	$outp .= '<tr  class="'.$trclass.'" id="tnl_tabletr-'.$addr_id.'">
	<td class="tnl_status"><span data-status="'.$addr_status.'">'.$addr_status.'</span></td><td class="tnl_name">'.$row['addr_name'].'</td><td class="tnl_email">'.$row['addr_email'].'</td><td class="tnl_type" data-type="'.$row['addr_type'].'">'.$typeArr[$row['addr_type']].'</td><td class="tnl_dago">'.$d.'</td>
	<td class="tnl_data">'.$datablock.$addr_has_block.'</td><td class="tnl_action"><a class="tnl_edit" href="#" onclick="tnl_edit_addr('.$addr_id.'); return false;"></a></td>	
	</tr>';
}

if ($outp != '') {
echo '<table>
<tr><td class="tnl_status">&nbsp;</td><td class="tnl_name">'.$TEXT['NAME'].'</td><td class="tnl_email">'.$TEXT['EMAIL'].'</td><td class="tnl_type">G</td><td class="tnl_dago">-D</td><td class="tnl_various">Data</td><td class="tnl_action">&nbsp;</td></tr>
'.$outp.'
</table>';
} else {
echo '<h2 class="tnl_noresults">'.$MOD_TINY_NEWSLETTER['NO_RESULTS'].'</h2>';
}

?>
