<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

//precheck email
$admin = new admin();
if ($_POST['what'] == 'precheck') :
	$addr_email = $admin->get_post('addr_email');
	$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_email = '$addr_email'";
	$res = $database->query($q);
	if( $res->numRows() >  0) :
		$output = json_encode(array('type'=>'error', 'text' => $MOD_TINY_NEWSLETTER['ERROR_MAIL_ALREADY']));
	else :
		$output = json_encode(array('type'=>'ok', 'text' => 'ok'));
	endif;
	die($output);
endif;

require(WB_PATH.'/modules/admin.php');
$admin = new admin('Modules', 'module_view', false, false);
$what = $admin->get_post('what');
$page_id = $admin->get_post('page_id');
$section_id = $admin->get_post('section_id');

//update an existing dataset
if ($what == 'update') :
	$addr_id = $admin->get_post('addr_id');
	$addr_name = $admin->get_post('addr_name');
	$addr_email = $admin->get_post('addr_email');
	$addr_type = $admin->get_post('addr_type');
	$addr_status = $admin->get_post('addr_status');

	$print_success = $MESSAGE['RECORD_MODIFIED_SAVED'];

	$q = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET
				addr_name = '$addr_name',
				addr_email = '$addr_email',
				addr_type = $addr_type,
				addr_status = $addr_status
				WHERE addr_id = $addr_id";
endif;

//save a new dataset
if ($what == 'insert') :
	$addr_name = $admin->get_post('addr_name');
	$addr_email = $admin->get_post('addr_email');
	$addr_type = $admin->get_post('addr_type');
	$addr_status = 1;
	$addr_started = time();
	$addr_msg = 'added';

	$print_success = $MESSAGE['RECORD_NEW_SAVED'];

	$q = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_addrs
			(addr_name, addr_email, addr_type, addr_status, addr_started, addr_msg)
			VALUES
			('$addr_name', '$addr_email', $addr_type, $addr_status, $addr_started, '$addr_msg')";
endif;

$database->query($q);

if ($database->is_error()) {
	$admin->print_error($database->get_error(), WB_URL.'/modules/'.$mod_dir.'/admin/receivers.php?page_id='.$page_id.'&section_id='.$section_id);
} else {
	$admin->print_success($print_success, WB_URL.'/modules/'.$mod_dir.'/admin/receivers.php?page_id='.$page_id.'&section_id='.$section_id);
}

// Print admin footer
$admin->print_footer();
