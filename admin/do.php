<?php
require('../../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$pA = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
array_pop ($pA);
$mod_dir = array_pop ($pA );
$tablename = $mod_dir;

$t = time();
$addr_id = 0;
if(isset($_REQUEST['addr_id']) AND is_numeric($_REQUEST['addr_id'])) { $addr_id = (int) $_REQUEST['addr_id'];}
require_once(WB_PATH.'/framework/class.admin.php');
$admin = new admin('Modules', 'module_view', false, false);

if (!($admin->is_authenticated() && $admin->get_permission($mod_dir, 'module'))) { die("Sorry, no access");}

require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');

if(isset($_REQUEST['do'])) { 
	$do = $_REQUEST['do'];
	
	//Remove
	if ($do == 'remove') {
		//beim 2. Mal: loeschen
		$sql = "DELETE FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status < 0 AND addr_id = $addr_id";
		$database->query($sql);
		
		//beim 1. Mail: nuer vormerken
		$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_status='-1' WHERE addr_id = $addr_id";
		if ( $database->query($sql) ) {
			$output = json_encode(array('type'=>'success', 'text' => 'User '.$addr_id.'removed'));		
		} else {
			$output = json_encode(array('type'=>'error', 'text' => 'Could not delete addr'));		
		}
		die($output);
	}
	
	//Add OR Modify:
	if ($do == 'add') {
		//Sanitize input data using PHP filter_var().
		$addr_name = filter_var(trim($_REQUEST["addr_name"]), FILTER_SANITIZE_STRING);
		$addr_email = filter_var(trim($_REQUEST["addr_email"]), FILTER_SANITIZE_EMAIL);
		$gender	= 		(int) $_REQUEST["gender"];
		$addr_status	= (int) $_REQUEST["addr_status"];
		
		//Schon vorhanden?
		$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_email = '$addr_email' AND addr_id <> '$addr_id'";
		$res = $database->query($q);
		if( $res->numRows() >  0) {
			$output = json_encode(array('type'=>'error', 'text' => 'Duplicate email'));
			die($output);
		}
		
		
		if ($addr_name == '' OR $addr_email == '' OR $gender == 0) {
			$output = json_encode(array('type'=>'error', 'text' => 'Missing Data'));
			die($output);
		} 
		
		//Modify?
		if ($addr_id > 0) {
			$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_email = '$addr_email' AND addr_id = '$addr_id'";
			$res_addrs = $database->query($q);
			$row_addrs = $res_addrs->fetchRow();
			$addr_idstr2 = $row_addrs['addr_idstr2'];			
						
			if ($addr_status > 0) {
				//Hat er auch addr_idstr2? Wenn nein, muss er ihn hier bekommen, sonst gibt es Verwirrung
				if ($addr_idstr2 == '') {$addr_idstr2 = tnl_GenerateRandomString();}				
			}
			
		
		
		
			$q = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET 
			addr_email = '$addr_email',
			addr_name = '$addr_name',
			addr_type = $gender,
			addr_status = $addr_status,
			addr_idstr2 = '$addr_idstr2'
			WHERE addr_id = $addr_id
			;";
			
			if ( $database->query($q) ) {
				$output = json_encode(array('type'=>'success', 'text' => 'User modified'));		
			} else {
				$output = json_encode(array('type'=>'error', 'text' => 'Could not modify addr'));		
			}
		
		} else {
	//add:
	
			$addr_idstr1 = tnl_GenerateRandomString();
			$addr_idstr2 = tnl_GenerateRandomString();
			
			
			//Schon vorhanden?
			$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_email = '$addr_email'";
			$res = $database->query($q);
			if( $res->numRows() >  0) {
				$output = json_encode(array('type'=>'error', 'text' => 'Already there'));
				die($output);
			}
		
		
			$q = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_addrs SET 
			addr_email = '$addr_email',
			addr_name = '$addr_name',
			addr_type = $gender,
			addr_status = $addr_status,
			addr_started = $t,
			addr_idstr1 = '$addr_idstr1',
			addr_idstr2 = '$addr_idstr2',
			addr_msg = 'added'
			;";
			
			
			if ( $database->query($q) ) {
				$output = json_encode(array('type'=>'success', 'text' => 'User added'));		
			} else {
				$output = json_encode(array('type'=>'error', 'text' => 'Could not add addr'));		
			}
		}
		die($output);
	}
}


?>
