<?php

/*** Modify ************************************************************/
$MOD_TINY_NEWSLETTER['RECEIVERS'] = 'Empf&auml;nger';
$MOD_TINY_NEWSLETTER['NEWSLETTERS'] = 'Newsletter';
$MOD_TINY_NEWSLETTER['STATISTICS'] = 'Statistik';
$MOD_TINY_NEWSLETTER['EXPORT'] = 'Export/Import';
$MOD_TINY_NEWSLETTER['SETTINGS'] = 'Einstellungen';
$MOD_TINY_NEWSLETTER['HELP'] = 'Hilfe';

$MOD_TINY_NEWSLETTER['RECEIVERS_OVERVIEW'] = '<div class="tnl_addr_sum">[TNL_ADDR_SUM]</div><div>Empf&auml;nger</div>';
$MOD_TINY_NEWSLETTER['SETTINGS_WARNING'] = 'Bitte als erstes die Einstellungen anpassen!';
$MOD_TINY_NEWSLETTER['REC_NAME'] = 'Name:';
$MOD_TINY_NEWSLETTER['REC_MAIL'] = 'E-Mail:';
$MOD_TINY_NEWSLETTER['REC_SALUTATION'] = 'Anrede:';
$MOD_TINY_NEWSLETTER['SUBSCRIPTIONS'] = 'Anmeldungen';
$MOD_TINY_NEWSLETTER['STATS_INFO'] = '<p>Daten inkl. Testuser<br/>
Es werden bereits sehr viele statistische Daten gesammelt.<br />
Mehr Auswertungen werden nachgereicht, wenn der erste Nutzer dieses Moduls mehr als 30 Adressen und 3 Newsletter hat ;-)</p>
';
$MOD_TINY_NEWSLETTER['STATS_DAYS'] = 'Tage';
$MOD_TINY_NEWSLETTER['STATS_RECS'] = 'Abonnenten';

/*** Tab Receivers *********************************************************/
$MOD_TINY_NEWSLETTER['NO_RESULTS'] = 'Nichts gefunden';
$MOD_TINY_NEWSLETTER['FILTER_ADD'] = 'Liste filtern | Eintrag hinzufügen / bearbeiten';
$MOD_TINY_NEWSLETTER['CLEAR_SEARCH'] = 'Filter löschen';
$MOD_TINY_NEWSLETTER['SALUTATION'] = 'Anrede';
$MOD_TINY_NEWSLETTER['DAYS_ACTIVE'] = 'Tage seit Anmeldung';
$MOD_TINY_NEWSLETTER['ADDITIONAL_INFO'] = 'Zusatz Info';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEND'] = 'Newsletter gesendet';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEEN'] = 'Newsletter geöffnet';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEND_LAST'] = 'Die letzten 3<br>gesendeten Newsletter';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEEN_LAST'] = 'Die letzten 3<br>geöffneten Newsletter';
$MOD_TINY_NEWSLETTER['SCREEN_SIZE'] = 'Bildschirmbreite<br>(bei Anmeldung)';
$MOD_TINY_NEWSLETTER['EDIT_RECEIVER'] = 'editieren';
$MOD_TINY_NEWSLETTER['DELETE_RECEIVER'] = 'löschen (nur möglich für<br>Empfänger mit Status < 1)';
$MOD_TINY_NEWSLETTER['ADD_RECEIVER'] = 'Empfänger hinzufügen';
$MOD_TINY_NEWSLETTER['STATUS_ARRAY'] = array('vom Empfänger abgemeldet','manuell abgemeldet','nicht bestätigt','manuell bestätigt','vom Empfänger bestätigt','besondere Empfänger Gruppe 1','besondere Empfänger Gruppe 2');
$MOD_TINY_NEWSLETTER['CONFIRM_DELETE'] = 'Wirklich löschen?';
$MOD_TINY_NEWSLETTER['EDIT_RECEIVER_HEADER'] = 'Newsletter Empfänger bearbeiten';
$MOD_TINY_NEWSLETTER['ADD_RECEIVER_HEADER'] = 'Newsletter Empfänger hinzufügen';

/*** Tab Newsletter ********************************************************/
$MOD_TINY_NEWSLETTER['NL_statusArr'] = array('Entwurf', 'Gestoppt', 'VERSENDEN!','Fertig', 'Abgelaufen');
$MOD_TINY_NEWSLETTER['NL_min_statusArr'] = array('Alle', '', 'Bes. Empfänger 1 und 2','Bes. Empfänger 2', '');
$MOD_TINY_NEWSLETTER['TIME_HINT_START'] = 'Fr&uuml;hestens ab dieser Zeit wird der Newsletter versendet';
$MOD_TINY_NEWSLETTER['TIME_HINT_STOP'] = 'Ab diesem Zeitpunkt wird dieser Newsletter nicht mehr versendet';

$MOD_TINY_NEWSLETTER['NL_ARCHIVED'] = 'Archiviert am [ARCHIVED_TIME]';
$MOD_TINY_NEWSLETTER['NL_NOT_ARCHIVED'] = 'Nicht archiviert';
$MOD_TINY_NEWSLETTER['NL_ARCHIVE'] = 'Jetzt archivieren';
$MOD_TINY_NEWSLETTER['NL_UNARCHIVE'] = 'Archiv l&ouml;schen';

$MOD_TINY_NEWSLETTER['ADD_NEWSLETTER'] = 'Neuer Newsletter';
$MOD_TINY_NEWSLETTER['NO_NEWSLETTER'] = 'Kein Newsletter vorhanden';
$MOD_TINY_NEWSLETTER['NL_EDITHINT'] = '<b>Inhalt</b><br>Bitte beachten: Die Anzeige hier kann deutlich von Ergebnis abweichen.<br>Lieber weniger statt mehr Formatierungen verwenden!';

$MOD_TINY_NEWSLETTER['CONFIRM_SAVE_NEWSLETTER'] = 'Sind Sie sicher, die Änderungen speichern zu wollen? \nFalls Sie den Status VERSENDEN ausgewählt haben, startet unmittelbar nach dem Speichern der Änderungen der Newsletterversand!';
$MOD_TINY_NEWSLETTER['CONFIRM_DELETE_NEWSLETTER'] = 'Sind Sie sicher, diese Ausgabe des Newsletters löschen zu wollen?';
$MOD_TINY_NEWSLETTER['SAVE_AND_DUPLICATE'] = 'Speichern & Duplizieren';
$MOD_TINY_NEWSLETTER['TEST_SENT_TO'] = 'Testmail gesendet an ';
$MOD_TINY_NEWSLETTER['TEST_SENT_ERROR'] = 'Fehler beim Versand der Testmail an ';


$MOD_TINY_NEWSLETTER['NL_SUBJECT'] = 'Betreff';
$MOD_TINY_NEWSLETTER['NL_TIMER'] = 'Zeitsteuerung';
$MOD_TINY_NEWSLETTER['N_TEMPLATE_SETTINGS'] = 'Template-Einstellungen';
$MOD_TINY_NEWSLETTER['NO_TEMPLATE_SETTINGS'] = 'Keine weiteren Template-Einstellungen verfügbar.';
$MOD_TINY_NEWSLETTER['NL_TEXT_ONLY'] = 'Nur-Text-Version';
$MOD_TINY_NEWSLETTER['NL_PREVIEW'] = 'Vorschau';
$MOD_TINY_NEWSLETTER['NL_ARCHIVE'] = 'Archiv';
$MOD_TINY_NEWSLETTER['NL_TEXT_ONLY_PREVIEW'] = 'Nur-Text-Vorschau';
$MOD_TINY_NEWSLETTER['NL_GROUPS'] = 'Gruppen';
$MOD_TINY_NEWSLETTER['NL_TEST_MAIL'] = 'Test-Mail';
$MOD_TINY_NEWSLETTER['NL_CANNOT_DELETE'] = 'Ein bereits versendeter Newsletter kann nicht gelöscht werden.';

/*** Tab Export/Import *****************************************************/
$MOD_TINY_NEWSLETTER['TEXT_BCC_H'] = 'Ausgabe f&uuml;r BCC';
$MOD_TINY_NEWSLETTER['TEXT_BCC'] = 'Alle E-Mail-Adressen f&uuml;r das BCC-Feld des Mail-Clients deiner Wahl:';
$MOD_TINY_NEWSLETTER['TEXT_TAB_H'] = 'Text mit Tabulator';
$MOD_TINY_NEWSLETTER['IMPORT_HINT'] = 'Bei Import-Problemen können alle in der letzten Stunde importierten Mails entfernt werden (ohne die bestehenden Empfänger zu tangieren), um einen weiteren Importversuch durchzuführen.';
$MOD_TINY_NEWSLETTER['IMPORT_DELETE'] = 'Zuletzt importierte Empfänger löschen';
$MOD_TINY_NEWSLETTER['RECORD_DIVIDER'] = 'Datensatz Trenner';
$MOD_TINY_NEWSLETTER['ITEM_DIVIDER'] = 'Daten Trenner';
$MOD_TINY_NEWSLETTER['DIVIDER_LINE'] = 'Zeilenumbruch';
$MOD_TINY_NEWSLETTER['DIVIDER_SEMICOLON'] = 'Semikolon';
$MOD_TINY_NEWSLETTER['DIVIDER_TAB'] = 'Tabulator';
$MOD_TINY_NEWSLETTER['DIVIDER_COMMA'] = 'Komma';
$MOD_TINY_NEWSLETTER['IMPORTED'] = 'Importiert';
$MOD_TINY_NEWSLETTER['IMPORT_ERROR'] = 'Fehler';
$MOD_TINY_NEWSLETTER['NOTHING_TO_IMPORT'] = 'Nichts zu importieren';
$MOD_TINY_NEWSLETTER['DELETE_IMPORTED'] = 'Zuletzt hinzugefügte Adressen löschen';
$MOD_TINY_NEWSLETTER['IMPORTED_DELETED'] = 'zuletzt hinzugefügte Adressen gelöscht';


/*** Tab Settings ******************************************************/
$MOD_TINY_NEWSLETTER['newsletter_form_H'] = 'Anmelde-Formular';
$MOD_TINY_NEWSLETTER['newsletter_form_T'] = '<h3>HTML</h3>Platzhalter (zwingend): <br /><b>[FORMBODY]</b>';
$MOD_TINY_NEWSLETTER['use_captcha'] = 'CAPTCHA (Bot-Schutz) im Anmeldeformular anzeigen';
$MOD_TINY_NEWSLETTER['genderarray_H'] = 'Anrede';
$MOD_TINY_NEWSLETTER['genderarray_T'] = '<h3>HTML</h3>Leere Select-Felder werden nicht ausgegeben.';
$MOD_TINY_NEWSLETTER['confirmation_text_H'] = 'Hinweis zur E-Mail';
$MOD_TINY_NEWSLETTER['confirmation_text_T'] = '<h3>HTML</h3>Der Text, mit dem in der Anmeldebox auf die versendete E-Mail hingewiesen wird.';

$MOD_TINY_NEWSLETTER['confirmation_various_T'] = '<h3>&nbsp;</h3>Absender der Best&auml;tigungs-E-Mail';
$MOD_TINY_NEWSLETTER['confirmation_various_H'] = 'E-Mail zur Best&auml;tigung';
$MOD_TINY_NEWSLETTER['confirmation_body_T'] = '<h3>Mail-Body</h3>
Zwingend:<br/>
&lt;a href=&quot;[AUTH_LINK]&quot;&gt;Best&auml;tigen&lt;/a&gt;<br />
&lt;a href=&quot;[REMOVE_LINK]&quot;&gt;L&ouml;schen/Abmelden&lt;/a&gt;<br/>
Optional: [SALUTATION]';


$MOD_TINY_NEWSLETTER['confirmation_mail_name'] = 'Name des Absenders:';
$MOD_TINY_NEWSLETTER['confirmation_mail_email'] = 'E-Mail-Adresse des Absenders:';
$MOD_TINY_NEWSLETTER['confirmation_mail_subject'] = 'Betreff:';

$MOD_TINY_NEWSLETTER['all_mails_H'] = 'Newsletter';

$MOD_TINY_NEWSLETTER['default_template_H'] = 'Template';
$MOD_TINY_NEWSLETTER['default_template_T'] = '<h3>&nbsp;</h3>Das Template, das zur Newsletterdarstellung (HTML-Mail/Browseransicht) verwendet wird';

$MOD_TINY_NEWSLETTER['newsletter_various_T'] = '<h3>&nbsp;</h3>Absender des Newsletters.<br/>Betreff ist die Standardvorgabe f&uuml;r alle Newsletter und kann dort individuell ge&auml;ndert werden';
$MOD_TINY_NEWSLETTER['newsletter_various_H'] = 'Newsletter Absender';

$MOD_TINY_NEWSLETTER['newsletter_mail_name'] = 'Name des Absenders:';
$MOD_TINY_NEWSLETTER['newsletter_mail_email'] = 'E-Mail-Adresse des Absenders:';
$MOD_TINY_NEWSLETTER['newsletter_mail_subject'] = 'Betreff:';


$MOD_TINY_NEWSLETTER['all_mails_appendix_H'] = 'Newsletter-Footer';
$MOD_TINY_NEWSLETTER['all_mails_appendix_T'] = '<h3>HTML</h3>Dieser Block wird an jeden HTML-Newsletter angeh&auml;ngt.<br/>Platzhalter:
<p>&lt;a href=&quot;[WEB_VERSION]&quot;&gt;Im Browser anschauen&lt;/a&gt;<br>&lt;a href=&quot;[REMOVE_LINK]&quot;&gt;Abmelden&lt;/a&gt;</p>';

$MOD_TINY_NEWSLETTER['all_mails_appendix_text_H'] = 'Newsletter-Footer Text-Version';
$MOD_TINY_NEWSLETTER['all_mails_appendix_text_T'] = '<h3>TEXT</h3>Dieser Block wird an die Text-Version angeh&auml;ngt.<p>Im Browser anschauen: [WEB_VERSION]<br>Abmelden: [REMOVE_LINK]</p>';

$MOD_TINY_NEWSLETTER['SOME_INTEGERS'] = 'Hier kann die Anzahl der versendeten Mails, bezogen auf einen bestimmten Zeitraum, angepasst werden. Dies kann notwendig sein, um serverseitige Begrenzungen nicht zu überschreiten. Bitte nur Zahlen angeben.';
$MOD_TINY_NEWSLETTER['SOME_INTEGERS2'] = 'Grenzwerte';
$MOD_TINY_NEWSLETTER['intval_block'] = 'Max. Mails, die pro Versandvorgang gleichzeitg verschickt werden (Standard: 5)';
$MOD_TINY_NEWSLETTER['intval_block_delay'] = 'Min. Sekunden zwischen den Versandvorgängen (Standard: 60)';
$MOD_TINY_NEWSLETTER['intval_receiver_delay'] = 'Min. Stunden zwischen 2 Mails an den gleichen Empf&auml;nger (Standard: 24)';

$MOD_TINY_NEWSLETTER['testmail_adresses_H'] = 'Testmails';
$MOD_TINY_NEWSLETTER['testmail_adresses_T'] = '<h3>&nbsp;</h3>Komma-separiert:<br/>mail1@example.com,mail2@example.com';

$MOD_TINY_NEWSLETTER['confirmation_finished1_H'] = 'Meldung nach Registrierung';
$MOD_TINY_NEWSLETTER['confirmation_finished1_T'] = '<h3>HTML</h3>Der Text erscheint, nachdem der Best&auml;tigungslink in der E-Mail aufgerufen wurde.';

$MOD_TINY_NEWSLETTER['confirmation_removed_H'] = 'Meldung nach Austragen';
$MOD_TINY_NEWSLETTER['confirmation_removed_T'] = '<h3>HTML</h3>Der Text erscheint, nachdem der Link zum Austragen in der E-Mail aufgerufen wurde.';

$MOD_TINY_NEWSLETTER['tracking_element_H'] = 'Tracking-Element';
$MOD_TINY_NEWSLETTER['tracking_element_T'] = '<h3>HTML-Tag</h3>Das Element, das das Tracking ausl&ouml;st.<br/>Feld leer lassen, wenn kein Tracking erwünscht ist.';
$MOD_TINY_NEWSLETTER['tracking_element_typ'] = 'typisch: &lt;img src="[WB_URL]/modules/[MOD_DIR]/action.php?pic=[AUTH_CODE]&tnl_id=[TNL_ID]" alt="" /&gt;';

/*** Newsletter Web-Version und Archiv *********************************/
$MOD_TINY_NEWSLETTER['WEB_NL_NOT_FOUND'] = 'Newsletter nicht gefunden';
$MOD_TINY_NEWSLETTER['WEB_NL_NOT_ACTIVE'] = 'Newsletter ist nicht aktiv';
$MOD_TINY_NEWSLETTER['WEB_NL_UNKNOWN_RECEIVER'] = 'Unbekannter Empfänger';
$MOD_TINY_NEWSLETTER['WEB_NL_NO_PERMISSION'] = 'Der Zugriff auf diesen Newsletter ist nicht gestattet';
$MOD_TINY_NEWSLETTER['WEB_NL_ARCHIVED'] = '<h3 style="text-align:center;" class="archived-version">Archivierte Version vom [ARCHIVED_TIME]</h3>';

include 'DE-frontend.php';
