<style type="text/css">
.nldoku .addr-status-2  {background:#de0303; color:#fff;} /* unsubscribed*/
.nldoku .addr-status-1  {background:#de5b03;} /*manually removed */
.nldoku .addr-status0   {background:#ffcc00;} /*NOT confirmed */
.nldoku .addr-status1   {background:#b5e314;} /*manually confirmed */
.nldoku .addr-status2   {background:#75d11a;} /*confirmed by receiver*/
.nldoku .addr-status3   {background:#21a3f5; color:#fff;} /*well known address */
.nldoku .addr-status4   {background:#7133ff; color:#fff;} /*VIP, Admin */
</style>

<div class="ziehharmonika">
  <h3>Allgemein</h3>
  <div>
    <p>Tiny Newsletter (kurz TNL) erfüllt die Basisfunktionen eines Newslettersystems, d.h. Anzeige eines Registrierungsformulars mit Double Opt-In im Frontend, Empfängerverwaltung, Erstellen und Versenden von Newslettern.</p>
	<p>Sobald einmal die Einstellungen angepasst worden sind, werden beim Aufruf eines TNL-Abschnitts im Backend zunächst die Anzahl der Empfänger und die im Modul gespeicherten Newsletter angezeigt. Über die Reiternavigation können die Eingabemasken für Funktionen und Einstellungen abgerufen werden. <strong>Achtung:</strong> Vor dem Wechsel des Reiters sind etwaige Eingaben zu speichern, da diese sonst verworfen werden.<br />
	Aus den Reitern heraus kann die Einstiegsseite über den ersten Reiter "«" aufgerufen werden.</p>
	
	<p>Der Versand des Newsletters erfolgt nach und nach (Genaueres dazu s.u., Einstellungen &gt; Grenzwerte), Auslöser ist jeweils das Laden des Anmeldeformulars im Frontend. Falls der Newsletter auf einer wenig frequentierten Seite ist, kann es daher sinnvoll sein, einen <a href="https://www.cronjob.de/" target="_blank"><span class="fa fa-fw fa-external-link"></span>Cronjob</a> einzurichten, der in regelmäßigen Abständen die Seite <strong><?php echo WB_URL; ?>/modules/tiny_newsletter/do.php</strong> aufruft.</p>
	
	
	<p>Für den bestimmungsgemäßen und gesetzeskonformen Einsatz des Newslettertools (DSGVO usw.) ist ausschließlich der jeweilige Newsletter-Anbieter verantwortlich. Die Ersteller des Moduls Tiny Newsletter lehnen jegliche Haftbarmachung im Falle von etwaigen Rechtsverstößen durch die Verwendung dieses Moduls ab.</p>
	
  </div>

  <h3>Einstellungen</h3>
  <div>
    <p>Auf dem Reiter Einstellungen können die im Frontend-Registrierungsformular, in E-Mail-Benachrichtigungen sowie auf An- und Abmelde-Bestätigungsseiten angezeigten Texte angepasst werden. Darüber hinaus können hier E-Mail-Absender-Adressen und -Namen für von TNL generierte Mails hinterlegt und Versandfrequenz konfiguriert werden. Ob via PHPmail oder SMTP versendet wird, hängt von den Einstellungen bei den WBCE-Grundeinstellungen ab.</p>
	
	<dl>
		<dt>Anmeldeformular:</dt><dd> Überschrift und ggf. weiterer Text, der im Anmeldeformular im Frontend angezeigt werden soll. Der Platzhalter [FORMBODY] generiert die Eingabefelder, Änderungen am Formular selbst sind nur durch Anpassungen an der view.php möglich.*<br />
		Benutzereingaben im Frontend werden auf Plausibilität geprüft (Mindestlänge von 4 Zeichen für Eingaben im Feld "Name", korrekte E-Mail-Syntax). Zum Schutz vor automatisierten Bot/Spam-Anmeldungen kann ein CAPTCHA aktiviert werden. Es wird dann der bei dem WBCE-Admintool "CAPTCHA und Advanced Spam Protection" eingestellte CAPTCHA-Typ verwendet.
		</dd>
		<dt>Anrede:</dt><dd> Anrede, die im Frontend-Registrierungsformular zur Auswahl steht und die dann gem. Empfängereinstellungen in der Double-Opt-In-Mail, sofern diese den Platzhalter [SALUTATION] enthält, und im Newsletter verwendet wird.</dd>
		<dt>Hinweis zur E-Mail:</dt><dd> Text, der im Frontend-Formular nach erfolgreichem Ausfüllen angezeigt wird.</dd>
		<dt>E-Mail zur Bestätigung:</dt><dd> Hier kann die Double-Opt-In-Mail angepasst werden. An die hier als Absender angegebene Mailadresse wird eine Benachrichtigungsmail gesendet, sobald sich ein neuer Empfänger erfolgreich registriert.</dd>
		<dt>Meldung nach Registrierung/nach Austragen:</dt><dd> Hier kann der im Frontend nach dem jeweiligen Vorgang angezeigte Text angepasst werden.</dd>
		<dt>Template:</dt><dd> Das für die Newsletter verwendete Template. Zu Templates siehe unten.</dd>
		<dt>Newsletter-Footer / Newsletter-Footer Text-Version:</dt><dd>Dieser Text wird an die HTML-Mail bzw. Nur-Text-Version angefügt. Der Austragelink sollte nicht entfernt werden. Für den rechtssicheren Versand kann es möglicherweise erforderlich sein, hier die Angaben aus dem Website-Impressum zu ergänzen. (Die Übernahme von Website-Inhalten per Sectionpicker o.ä. ist nicht möglich, die Angaben müssen direkt in das Feld eingefügt werden.)</dd>
		<dt>Grenzwerte:</dt><dd> Der Versand des Newsletters an die Empfänger erfolgt nicht auf einmal, sondern nach und nach. Auslöser dafür ist der Aufruf des Registrierungsformulars im Frontend. Zur Vermeidung von Blacklisting sollten nicht zu viele Mails auf einmal versendet werden. Die Vorgabewerte sind so gewählt, um das Blacklisting-Risiko (Klassifizierung als Spammer) möglichst zu minimieren, führen aber andererseits dazu, dass es durchaus mehrere Tage dauern kann, einen Newsletter mit einer hohen Zahl an Empfängern zu versenden. Konkrete Empfehlungen für eine entsprechende Anpassung können nicht gegeben werden, da dies sehr stark von den sender- und empfängerseitigen Einstellungen abhängt.</dd>
		<dt>Testmails:</dt><dd> Vor dem Versand des Newsletters (siehe unten) kann - und sollte - eine 1:1-Vorschau mit einer zufällig generierten Anrede an die hier angegebene(n) Adressen gesendet werden, um die Newsletterausgabe auf korrekte Darstellung und Fehlerfreiheit (eingebundene Bilder, Verlinkungen...) zu überprüfen.</dd>
		<dt>Tracking-Element:</dt><dd> Standardvorgabe ist Schattenkanten-Grafik, die unauffällig unter das Titelbild eines Newsletters eingefügt wird. Anhand des Abrufs der Grafik kann dann TNL erfassen, wie oft der Newsletter insgesamt und von wem geöffnet wurde - vorausgesetzt, beim Empfänger ist das Laden von Grafiken in Mails aktiviert (was oft nicht der Fall ist). Soll kein Tracking erfolgen, dieses Feld leer lassen.</dd>
	</dl>
  </div>
  
  <h3>Empfänger</h3>
  <div>
	<p>Auf dem Reiter Empfänger können Empfängerdaten eingesehen und verwaltet werden.</p>
	<dl>
		<dt>Empfänger hinzufügen:</dt>
		<dd>Im Normalfall registrieren sich die Newsletterempfänger selbsttätig über das im Frontend angezeigte Formular. Wenn es erforderlich ist und das Einverständnis der betroffenen Person nachweislich vorliegt, können durch Anklicken des Buttons <span class="fa fa-fw fa-plus-circle"></span> oben rechts einzelne weitere Einträge vorgenommen werden. (Sollen mehrere Empfänger angelegt werden, Importfunktion nutzen, s.u.).</dd>
		<dt>Filtern/Suchen; Filter löschen:</dt>
		<dd>Die Empfängerliste kann anhand der angezeigten Eingabefelder nach bestimmten Kriterien gefiltert werden, sodass nur übereinstimmende Zeilen angezeigt werden. Um die Filterung aufzuheben, Button "Filter löschen" anklicken.</dd>
		<dt>Empfängerliste:</dt>
		<dd>Die Empfängerliste ist unterteilt in die Spalten S (Status), Name, E-Mail, G (Anrede), D (Datum seit Registrierung), i (Zusatzinfo), &Sigma; <i class="fa fa-envelope-o"></i> (Anzahl Newsletter, die dieser Empfänger erhalten hat), &Sigma; <i class="fa fa-eye"></i> (Anzahl Newsletter, die dieser Empfänger gelesen hat), <i class="fa fa-desktop"></i> (Bildschirmauflösung); über die Schaltfläche <i class="fa fa-edit"></i> können die Daten des betr. Empfängers bearbeitet werden. Die Löschfunktion <i class="fa fa-trash"></i> steht nur bei Benutzern zur Verfügung, deren Status (s.u.) auf "manuell abgemeldet" oder "vom Empfänger abgemeldet" steht. Die Liste kann nach den jeweiligen Spalten auf- und absteigend sortiert werden.</dd>
		<dt>Empfängerstatus/-gruppen:</dt>
		<dd>Empfänger können Stati von -2 bis 4 haben, die anhand des angezeigten Wertes sowie des Farbcodes ersichtlich sind. <br />
		<table border="0" class="nldoku">
		<tr>
			<td class="tnl-status addr-status-2">-2</td>
			<td>vom Empfänger abgemeldet</td>
			<td>kommt faktisch nicht vor, da der Eintrag eines Empfängers beim Austragen aus dem Verteiler komplett gelöscht wird.</td>
		</tr>
		<tr>
			<td class="tnl-status addr-status-1">-1</td>
			<td>manuell abgemeldet</td>
			<td>Empfänger, der durch den Newsletteranbieter übers Backend für den Newslettempfang gesperrt wurde.</td>
		</tr>
		<tr>
			<td class="tnl-status addr-status0">0</td>
			<td>nicht bestätigt</td>
			<td>Empfänger, der das Frontend-Registrierungsformular abgesendet hat, aber die Registrierung noch nicht per Klick auf den Link in der Double-Opt-In-Mail bestätigt hat.</td>
		</tr>
		<tr>
			<td class="tnl-status addr-status1">1</td>
			<td>manuell bestätigt</td>
			<td>Empfänger, der durch den Newsletteranbieter übers Backend manuell eingetragen oder importiert wurde.</td>
		</tr>
		<tr>
			<td class="tnl-status addr-status2">2</td>
			<td>vom Empfänger bestätigt</td>
			<td>Empfänger, der das Frontend-Registrierungsformular ausgefüllt und auf den Link in der Double-Opt-In-Mail geklickt hat.</td>
		</tr>
		<tr>
			<td class="tnl-status addr-status3">3 / 4</td>
			<td>Besondere Empfänger Gruppe</td>
			<td>Newsletter können für Testzwecke nur an unterschiedliche Gruppen versendet werden. Da es allerdings noch keine Möglichkeit für die Empfänger gibt, die Gruppenzuordnung bei der Registrierung selbst vorzunehmen, und auch eine gesammelte Änderung der Gruppenzuordnung nicht bzw. nur durch direkte Datenbankmanipulation* möglich ist, sind diese Gruppen faktisch noch irrelevant.</td>
		</tr>
		</table>
		<dt>Zurück / n / Nächste:</dt>
		<dd>Paginierung der Empfängertabelle im Backend, Wechsel zwischen den einzelnen Seiten.</dd>
	</dl>
  </div>
  <h3>Newsletter</h3>
  <div>
	<p>Auf diesem Reiter werden alle gespeicherten Newsletter angezeigt und natürlich kann hier ein neuer Newsletter verfasst werden.</p>
	<dl>
		<dt>Neuer Newsletter:</dt>
		<dd>Die Eingabefelder für das Verfassen eines neuen Newsletters werden angezeigt. Details s.u.</dd>
		<dt>Vorhandene Newsletter:</dt>
		<dd>In der Liste werden die im System gespeicherten Newsletter angezeigt. Der Balken neben dem Newslettertitel zeigt bei Newslettern im Status "VERSENDEN" an, ob der Newsletterversand aktuell erfolgt oder abgeschlossen ist. Darüber hinaus ist hier ersichtlich, ob der Zeitraum, während dem der Newsletter versendet werden soll, noch nicht erreicht oder überschritten wurde.<br />
		Um einen Newsletter zu bearbeiten, dessen Titel oder das Bearbeiten-Symbol ganz rechts anklicken.</dd>
	</dl>
 
	<p>Nach dem Anklicken des Buttons "Neuer Newsletter" oder eines vorhandenen Eintrages auf dem Reiter "Newsletter" öffnet sich die Eingabemaske zum Erstellen bzw. Bearbeiten eines Newsletters.</p>
	<dl>
		<dt>Betreff:</dt>
		<dd>Wird als Betreff (Subject) der Newsletter-Ausgabe verwendet. </dd>
		<dt>Gruppen:</dt>
		<dd>Hier kann ausgewählt werden, ob der Newsletter an alle aktiven, registrierten Empfänger oder nur an bestimmte Empfänger (die einer der beiden Sondergruppen zugeordnet sind) versendet werden soll.</dd>
		<dt>Status:</dt>
		<dd>Mit dem Status wird der Versand des Newsletters - ggfs. abhängig von der Zeitsteuerung - gestartet und gestoppt. Sobald der Status auf "VERSENDEN!" gestellt und die Änderung gespeichert wurde, erfolgt der Versand des Newsletters, wenn das Frontend-Registrierungsformular durch den Newsletteranbieter oder beliebige Besucher aufgerufen wurde, und zwar so lange, bis alle Empfänger den Newsletter erhalten haben, der Versandzeitraum beendet ist oder der Newsletterstatus manuell auf "Entwurf" oder "Fertig" gestellt wurde. <strong>Um bei Irrtümern/Problemen usw. den Newsletterversand abzubrechen, den Status auf "Entwurf" oder "Fertig" stellen und Änderung speichen.</strong></dd>
		<dt>Speichern:</dt>
		<dd>Den aktuellen Status und Bearbeitungsstand speichern. Aufgrund der Auswirkung der Statusänderung erfolgt dies stets erst nach Bestätigung der angezeigten Rückfrage.</dd>
		<dt>Inhalt:</dt>
		<dd>Der eigentliche Inhalt des Newsletters. Anrede und Empfängername brauchen in diesem Feld nicht mit eingetragen zu werden, sondern werden automatisch ergänzt. Der hier eingegebene Inhalt wird dann von den Elementen umgeben, die im Newslettertemplate vor und nach dem Platzhalter [MAIL_BODY] stehen (mehr zu Templates s.u.).<br>
		Normale Text- und Absatzformatierungen wie fett, kursiv, Aufzählung etc. können problemlos verwendet werden. Auch Sprungmarken (Anker), auf der Website im Media-Verzeichnis gespeicherte Bilder und Dokumente können in den Text eingefügt werden. <br>Es ist aber nicht möglich, Inhalte per Sectionpicker oder sonstiger Droplets zu erzeugen. Zudem sollte aufgrund der eingeschränkten Anzeigemöglichkeiten vieler Mailclients die Formatierung insgesamt so einfach wie möglich gehalten werden.</dd> 
		<dt>Zeitsteuerung:</dt>
		<dd>Zeitraum, während dem der Newsletter versendet wird. Achtung: Die Uhrzeit ist in GMT anzugeben, d.h. es wird die auf dem Server eingestellte und nicht die lokale Zeitzone verwendet.<br/>
		Standardmäßig erhalten "Nachzügler", also Empfänger, die sich während der Laufzeit des Newsletters registrieren, wenn der Newsletterversand schon an alle bis dahin registrierten Empfänger abgeschlossen ist, den Newsletter nicht, da nach dem Versand des Newsletters an den letzen Empfänger in der Liste der Newsletterstatus auf "Fertig" gesetzt wird. Ist dieses Verhalten nicht gewünscht und sollen auch Empfänger, die sich während des Newsletter-Versandzeitraums registrieren, noch (ggfs. sofort) die jeweilige Newsletterausgabe erhalten, in der do.php die Zeilen 
		<code>$sql3 = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_newsletters SET tnl_status = 3 WHERE tnl_id = $tnl_id";
		$database->query($sql3);
		</code><br />
		auskommentieren bzw. entfernen*.
		<dt>Template-Einstellungen:</dt>
		<dd>Sofern das gewählte Template Varianten unterstützt, kann hier die Variante ausgewählt werden, oder es werden weitere templatespezifische Einstellungen angezeigt. Die Auswahl des verwendeten Templates selbst ist nur bei den Einstellungen möglich und wird auf alle Newsletter angewendet.</dd>
		<dt>Nur-Text-Version:</dt>
		<dd>Für Empfänger, deren Mailclient HTML-Mails nicht anzeigt, kann hier eine Plain-Text-Version des Newsletters hinterlegt werden. Logischerweise sind hier keine HTML-Formatierungen zulässig, Umbrüche und Absätze erscheinen in der Mail dann genau so wie in diesem Feld angegeben. Von etwaigen Darstellungsfehlern in der Browser-Vorschau sollte man sich nicht übermäßig irritieren lassen.</dd>
		<dt>Vorschau:</dt>
		<dd>Vor dem Versand des Newsletters kann (und sollte) die jeweils gespeicherte (!) Version im Browser angezeigt und an den/die bei den Einstellungen hinterlegten Mailadressen versendet werden.</dd>
		<dt>Speichern und Duplizieren:</dt>
		<dd>Der angezeigte Bearbeitungsstand des Newsletters wird gespeichert und eine Kopie davon wird angelegt (in dieser Reihenfolge!). Das bedeutet: Wenn ein vorhandener Newsletter als Kopiervorlage herhalten soll, die Kopiervorlage selbst jedoch unverändert bleiben soll, so muss auf Speichern und Kopieren geklickt werden, <strong>ohne</strong> dass vorher irgendwelche Änderungen an der Kopiervorlage vorgenommen wurden.</dd>
		<dt>Löschen:</dt>
		<dd>Löscht den aktuell angezeigten Newsletter unwiderruflich. Sobald ein Newsletter versendet und von mindestens einem Empfänger gelesen wurde, kann der Newsletter jedoch nicht mehr gelöscht werden.</dd> 
	</dl>
  </div>
  
  <h3>Export/Import</h3>
  <div>
	<dl>
		<dt>BCC</dt>
		<dd>Alle Empfängeradressen, die mindestens einen Newsletter erhalten haben, werden hier mit Semikolon getrennt angezeigt und können dann per Copy & Paste (z.B.) ins BCC-Feld des Mailclients übernommen werden.</dd>
		<dt>Text-TAB</dt>
		<dd>Alle Empfänger, die mindestens einen Newsletter erhalten haben, werden als Liste im Format <code>Mailadresse[Tabulator]Name[Tabulator]Anrede</code> angezeigt und können so per Copy & Paste (z.B.) in ein Tabellenprogramm übertragen werden.</dd>
		<dt>Import</dt>
		<dd>Über das Import-Feld können mehrere Adressen auf einmal importiert werden. Zulässige Formate (Anrede = M, F oder N):<br />
		<code>
		Mailadresse[Tabulator]Name[Tabulator]Anrede<br />
		Mailadresse[Tabulator]Name[Tabulator]Anrede<br />
		Mailadresse[Tabulator]Name[Tabulator]Anrede<br />
		</code>
		<br />oder </br />
		<code>
		Mailadresse,Name,Anrede<br />
		Mailadresse,Name,Anrede<br />
		Mailadresse,Name,Anrede<br />
		</code>
		<br />oder </br />
		<code>
		Mailadresse<br />
		Mailadresse<br />
		Mailadresse<br />
		</code>
		<br />oder </br />
		<code>
		Mailadresse;Mailadresse;Mailadresse
		</code>
		<br />
		<br />
		Das Importformat muss unterhalb des Eingabefeldes korrekt ausgewählt werden, andernfalls werden die Adressen nicht importiert.		
		<br/>
		Die während der letzten Stunde importierten Adressen können bei Problemen aus dem Verteiler gelöscht werden.<br/>
		Beim manuellen Import ist zu bedenken, dass das Einverständnis zur Datenverarbeitung und -verwendung der betroffenen Empfänger nachweislich vorliegen muss. Andernfalls ist die Verwendung illegal.
	</dl>
  </div>
  <h3>Statistik</h3>
  <div>
  Der Rede nicht wert.
  </div>
  <h3>Templates</h3>
  <div>
	<p>Das Template, das für die Darstellung des Newsletters in HTML-fähigen Newsletterclients und in der Browseransicht verwendet wird, kann selbst gestaltet werden. Es ist auch möglich, mehrere Templates anzulegen, verwendet wird aber immer nur dasjenige, welches bei den Newslettereinstellungen ausgewählt wird (auch rückwirkend für bereits versandte Newsletterausgaben). Das TNL-Template hat nichts mit dem WBCE-Seitentemplate zu tun. Es ist daher auch nicht möglich, die Template-Formatierungen bereits im WYSIWYG-Editor im Backend darzustellen, sondern nur über die Vorschaufunktion des Moduls.</p>
	<p>Viele Mailclients unterstützen HTML-Formatierungen nur rudimentär. Bevor es ans Templatedesignen geht, ist es ratsam, sich darüber schlau zu machen, welche Formatierungen/Befehle sicher angewendet werden können. Dies hier zu erklären, würde den Rahmen sprengen. Es gibt eine Vielzahl von Seiten, auf denen frei verfügbare Templates für Newsletter heruntergeladen werden können, die dann als TNL-Template aufbereitet werden können.*.</p>
	
	<dl>
		<dt>Speicherort</dt>
		<dd>TNL-Templates müssen im Verzeichnis <code>/modules/tiny_newsletter/templates</code> jeweils in einem eigenen Unterverzeichnis gespeichert werden. TNL nimmt an, dass alle Verzeichnisse unterhalb von <code>templates</code> Templates beinhalten, und listet diese in der Template-Auswahl mit ihrem Namen auf. Der Verzeichnisname darf keine Leerzeichen, Sonderzeichen und Umlaute beinhalten.</dd>
		<dt>Bestandteile</dt>
		<dd>Ein TNL-Template besteht mindestens aus den Dateien <code>index.html</code> und <code>index.php</code>. <br />
		Sofern ein Template spezielle Funktionen mitbringen soll*, wie z.B. unterschiedliche Styles, oder spezielle Funktionen zur Übernahme von Inhalten aus anderen Modulen, sind diese in den Dateien <code>template_settings.php</code> (Darstellung der spezifischen Eingabe-/Auswahlfelder im Backend) und <code>template_functions.php</code> (Anwendung der jeweiligen Einstellungen/Funktionen auf die Darstellung der einzelnen Newsletterausgaben) zu hinterlegen. Beispielumsetzungen siehe vorh. Templates. Es sei aber gleich dazu gesagt, dass das alles andere als einfach ist.</dd>
		<dt>Verfügbare Platzhalter</dt>
		<dd>Im TNL-Template stehen Platzhalter zur Verfügung, die durch den jeweiligen Wert ersetzt werden. Weitere Platzhalter können in den zum Template gehörenden PHP-Dateien definiert werden.* Standardmäßig vorhanden sind:
		<table border="0" class="nldoku">
			<tr>
				<td>[MAIL_BODY]</td>
				<td>zwingend</td>
				<td>Text des Newsletters (Eingabefeld "Inhalt" im Backend)</td>
			</tr>
			<tr>
				<td>[MAIL_SUBJECT]</td>
				<td>optional</td>
				<td>Betreff / Überschrift</td>
			</tr>
			<tr>
				<td>[SALUTATION]</td>
				<td>optional</td>
				<td>Anrede + Name des jeweiligen Newsletterempfängers</td>
			</tr>
			<tr>
				<td>[MAIL_FOOTER]</td>
				<td>empfohlen</td>
				<td>Bei den Einstellungen hinterlegter Newsletter-Footer</td>
			</tr>		
		</table>
		</dd>
		
	</dl>
	
  </div>
  
</div>



<p>* erfordert PHP/HTML-Kentnisse</p>
<p>In dieser Dokumentation wird im Interesse der Leserlichkeit das generische Maskulinum verwendet. Alle Begriffe wie "Empfänger" usw. schließen selbstverständlich Empfängerinnen etc. ein.</p>



<script>
$('.ziehharmonika').ziehharmonika();

</script>