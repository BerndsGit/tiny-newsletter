<?php
//Modul Description
$module_description = 'Submissions for a newsletter';

//Variables for the Frontend
//$MOD_TINY_NEWSLETTER['VIEW_TITLE']        = 'Newsletter bestellen:';
$MOD_TINY_NEWSLETTER['NAME']  = '<b>Name</b> zB.: Dr John Doe';
$MOD_TINY_NEWSLETTER['EMAIL']       = '<b>eMail</b>';
$MOD_TINY_NEWSLETTER['GENDER']   = 'Salutation';
$MOD_TINY_NEWSLETTER['AUTHCODE_FIELD'] = 'Auth Code';
$MOD_TINY_NEWSLETTER['SUBMIT'] = 'Sign in';

$MOD_TINY_NEWSLETTER['ERROR_NAME'] = 'Please enter a name (at least 4 characters)!';
$MOD_TINY_NEWSLETTER['ERROR_MAIL'] = 'Please enter a valid Email address!';
$MOD_TINY_NEWSLETTER['ERROR_GENDER'] = 'No salutation submitted';
$MOD_TINY_NEWSLETTER['ERROR_AUTHCODE'] = 'Code is required';
$MOD_TINY_NEWSLETTER['ERROR_MAIL_ALREADY'] = 'Email address is already registred';

$MOD_TINY_NEWSLETTER['UNKNOWN_AUTH'] = 'Unknown Code<br/>This account might have been already deleted.';
$MOD_TINY_NEWSLETTER['UNKNOWN_AUTH0'] = 'You can\'t use the unsubscribe link in test emails!';

//Wenn erneut bestaetigt wird, aber bereits eingetragen:
$MOD_TINY_NEWSLETTER['FINISHED2'] = 'Registration is finished! You will now recieve the newslettter.';

$MOD_TINY_NEWSLETTER['AUTH_CONFIRMED_SUBJECT'] = 'Neue Anmeldung fuer den Newsletter';
$MOD_TINY_NEWSLETTER['AUTH_CONFIRMED_CONTENT']  = "Ein neuer Empfaenger hat soeben die Anmeldung bestaetigt: \n[NAME], \n[EMAIL], \n\n[WB_URL]";
