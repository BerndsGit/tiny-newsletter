<?php

/*** Modify ************************************************************/
$MOD_TINY_NEWSLETTER['RECEIVERS'] = 'Recipients';
$MOD_TINY_NEWSLETTER['NEWSLETTERS'] = 'Newsletter';
$MOD_TINY_NEWSLETTER['STATISTICS'] = 'Statistics';
$MOD_TINY_NEWSLETTER['EXPORT'] = 'Export/Import';
$MOD_TINY_NEWSLETTER['SETTINGS'] = 'Settings';
$MOD_TINY_NEWSLETTER['HELP'] = 'Help';

$MOD_TINY_NEWSLETTER['RECEIVERS_OVERVIEW'] = '<div class="tnl_addr_sum">[TNL_ADDR_SUM]</div><div>Recipients</div>';
$MOD_TINY_NEWSLETTER['SETTINGS_WARNING'] = 'Please configure some default values at "Settings" at first!';
$MOD_TINY_NEWSLETTER['REC_NAME'] = 'Name:';
$MOD_TINY_NEWSLETTER['REC_MAIL'] = 'Email:';
$MOD_TINY_NEWSLETTER['REC_SALUTATION'] = 'Salutation:';
$MOD_TINY_NEWSLETTER['SUBSCRIPTIONS'] = 'Subscriptions';
$MOD_TINY_NEWSLETTER['STATS_INFO'] = '<p>Data incl. test accounts<br/>
The tool collects a lot of information already.<br />
More diagrams etc. will be available in an upcoming release of Tiny Newsletter - when the a module user has reached more than 30 recipients and has issued 3 newsletters ;-)</p>
';
$MOD_TINY_NEWSLETTER['STATS_DAYS'] = 'Days';
$MOD_TINY_NEWSLETTER['STATS_RECS'] = 'Recipients';

/*** Tab Recipients *********************************************************/
$MOD_TINY_NEWSLETTER['NO_RESULTS'] = 'No results';
$MOD_TINY_NEWSLETTER['FILTER_ADD'] = 'Filter list | Add / Remove entry';
$MOD_TINY_NEWSLETTER['CLEAR_SEARCH'] = 'Reset filter';
$MOD_TINY_NEWSLETTER['SALUTATION'] = 'Salutation';
$MOD_TINY_NEWSLETTER['DAYS_ACTIVE'] = 'Days since subscription';
$MOD_TINY_NEWSLETTER['ADDITIONAL_INFO'] = 'Additional Info';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEND'] = 'Newsletter send';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEEN'] = 'Newsletter seen';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEND_LAST'] = 'The last 3<br>sent Newsletter';
$MOD_TINY_NEWSLETTER['NEWSLETTER_SEEN_LAST'] = 'The last 3<br>seen Newsletter';
$MOD_TINY_NEWSLETTER['SCREEN_SIZE'] = 'Screenwidth<br>(on subscription)';
$MOD_TINY_NEWSLETTER['EDIT_RECEIVER'] = 'Edit';
$MOD_TINY_NEWSLETTER['DELETE_RECEIVER'] = 'Delete (only possible for<br>recipients with status < 1)';
$MOD_TINY_NEWSLETTER['ADD_RECEIVER'] = 'Add recipient';
$MOD_TINY_NEWSLETTER['STATUS_ARRAY'] = array('unsubcribed by recipient','unsubcribed manually','not confirmed','confirmed manually','confirmed by recipient','special recipients group 1','special recipients group 2');
$MOD_TINY_NEWSLETTER['CONFIRM_DELETE'] = 'Really delete?';
$MOD_TINY_NEWSLETTER['EDIT_RECEIVER_HEADER'] = 'Edit newsletter recipient';
$MOD_TINY_NEWSLETTER['ADD_RECEIVER_HEADER'] = 'Add newsletter recipient';

/*** Tab Newsletter ********************************************************/
$MOD_TINY_NEWSLETTER['NL_statusArr'] = array('Draft', 'Halted', 'LIVE!','Finished', 'Outdated');
$MOD_TINY_NEWSLETTER['NL_min_statusArr'] = array('Everybody', '', 'Special recipients 1&2','Special recipients 2', '');
$MOD_TINY_NEWSLETTER['TIME_HINT_START'] = 'The newsletter will not be sent out before this time';
$MOD_TINY_NEWSLETTER['TIME_HINT_STOP'] = 'The newsletter will not be sent out after this time';

$MOD_TINY_NEWSLETTER['NL_ARCHIVED'] = 'Archived on [ARCHIVED_TIME]';
$MOD_TINY_NEWSLETTER['NL_NOT_ARCHIVED'] = 'Not archived';
$MOD_TINY_NEWSLETTER['NL_ARCHIVE'] = 'Archive now';
$MOD_TINY_NEWSLETTER['NL_UNARCHIVE'] = 'Delete archived version';

$MOD_TINY_NEWSLETTER['ADD_NEWSLETTER'] = 'New newsletter';
$MOD_TINY_NEWSLETTER['NO_NEWSLETTER'] = 'No newsletter available';
$MOD_TINY_NEWSLETTER['NL_EDITHINT'] = '<b>Content</b><br>Please notice: The newsletter might look quite different from what you are seeing here.<br>You should keep the mail body as simple as possible.';

$MOD_TINY_NEWSLETTER['CONFIRM_SAVE_NEWSLETTER'] = 'Are you sure you want to save the changes? \ nIf you have selected the status LIVE!, the newsletter will be sent immediately after saving the changes!';
$MOD_TINY_NEWSLETTER['CONFIRM_DELETE_NEWSLETTER'] = 'Are you sure you want to delete this issue of the newsletter?';
$MOD_TINY_NEWSLETTER['SAVE_AND_DUPLICATE'] = 'Save & Duplicate';
$MOD_TINY_NEWSLETTER['TEST_SENT_TO'] = 'Testmail sent to ';
$MOD_TINY_NEWSLETTER['TEST_SENT_ERROR'] = 'Error sending testmail to ';


$MOD_TINY_NEWSLETTER['NL_SUBJECT'] = 'Subject';
$MOD_TINY_NEWSLETTER['NL_TIMER'] = 'Timer';
$MOD_TINY_NEWSLETTER['N_TEMPLATE_SETTINGS'] = 'Template settings';
$MOD_TINY_NEWSLETTER['NO_TEMPLATE_SETTINGS'] = 'No further template settings available.';
$MOD_TINY_NEWSLETTER['NL_TEXT_ONLY'] = 'Text-only version';
$MOD_TINY_NEWSLETTER['NL_PREVIEW'] = 'Preview';
$MOD_TINY_NEWSLETTER['NL_ARCHIVE'] = 'Archive';
$MOD_TINY_NEWSLETTER['NL_TEXT_ONLY_PREVIEW'] = 'Text-only preview';
$MOD_TINY_NEWSLETTER['NL_GROUPS'] = 'Groups';
$MOD_TINY_NEWSLETTER['NL_TEST_MAIL'] = 'Test-Mail';
$MOD_TINY_NEWSLETTER['NL_CANNOT_DELETE'] = 'Cannot delete a newsletter that has already been sent';

/*** Tab Export/Import *****************************************************/
$MOD_TINY_NEWSLETTER['TEXT_BCC_H'] = 'Output for BCC';
$MOD_TINY_NEWSLETTER['TEXT_BCC'] = 'All recievers email addresses for the BCC field of your newsletter client:';
$MOD_TINY_NEWSLETTER['TEXT_TAB_H'] = 'Tabulator seperated text';
$MOD_TINY_NEWSLETTER['IMPORT_HINT'] = 'If you encounter any problems while importing recipients, you can delete the recipients which were added in the last hour (without impact on the existing recievers), so you can start another importing approach.';
$MOD_TINY_NEWSLETTER['IMPORT_DELETE'] = 'Delete recently imported recipients';
$MOD_TINY_NEWSLETTER['RECORD_DIVIDER'] = 'Record divider';
$MOD_TINY_NEWSLETTER['ITEM_DIVIDER'] = 'Item divider';
$MOD_TINY_NEWSLETTER['DIVIDER_LINE'] = 'Linebreak';
$MOD_TINY_NEWSLETTER['DIVIDER_SEMICOLON'] = 'Semicolon';
$MOD_TINY_NEWSLETTER['DIVIDER_TAB'] = 'Tab';
$MOD_TINY_NEWSLETTER['DIVIDER_COMMA'] = 'Comma';
$MOD_TINY_NEWSLETTER['IMPORTED'] = 'Imported';
$MOD_TINY_NEWSLETTER['IMPORT_ERROR'] = 'Error';
$MOD_TINY_NEWSLETTER['NOTHING_TO_IMPORT'] = 'Nothing to import';
$MOD_TINY_NEWSLETTER['DELETE_IMPORTED'] = 'Delete recently added addresses';
$MOD_TINY_NEWSLETTER['IMPORTED_DELETED'] = 'last added addresses deleted';


/*** Tab Settings ******************************************************/
$MOD_TINY_NEWSLETTER['newsletter_form_H'] = 'Sign in form';
$MOD_TINY_NEWSLETTER['newsletter_form_T'] = '<h3>HTML</h3>Placeholders (mandatory): <br /><b>[FORMBODY]</b>';
$MOD_TINY_NEWSLETTER['use_captcha'] = 'Bot protection of sign in form by CAPTCHA';
$MOD_TINY_NEWSLETTER['genderarray_H'] = 'Salutation';
$MOD_TINY_NEWSLETTER['genderarray_T'] = '<h3>HTML</h3>Empty select boxes will not be displayed.';
$MOD_TINY_NEWSLETTER['confirmation_text_H'] = 'Email notice';
$MOD_TINY_NEWSLETTER['confirmation_text_T'] = '<h3>HTML</h3>The text signalizing the confirmation email which will be displayed in the sign in form';

$MOD_TINY_NEWSLETTER['confirmation_various_T'] = '<h3>&nbsp;</h3>Sender address of the confirmation email';
$MOD_TINY_NEWSLETTER['confirmation_various_H'] = 'Confirmation email';
$MOD_TINY_NEWSLETTER['confirmation_body_T'] = '<h3>Mail-Body</h3>
Mandatory:<br/>
&lt;a href=&quot;[AUTH_LINK]&quot;&gt;Confirm&lt;/a&gt;<br />
&lt;a href=&quot;[REMOVE_LINK]&quot;&gt;Delete/sign out&lt;/a&gt;<br/>
Optional: [SALUTATION]';


$MOD_TINY_NEWSLETTER['confirmation_mail_name'] = 'Sender name:';
$MOD_TINY_NEWSLETTER['confirmation_mail_email'] = 'Sender email address:';
$MOD_TINY_NEWSLETTER['confirmation_mail_subject'] = 'Subject:';

$MOD_TINY_NEWSLETTER['all_mails_H'] = 'Newsletter';

$MOD_TINY_NEWSLETTER['default_template_H'] = 'Template';
$MOD_TINY_NEWSLETTER['default_template_T'] = '<h3>&nbsp;</h3>The template which will be used for HTML mail / view in browser';

$MOD_TINY_NEWSLETTER['newsletter_various_T'] = '<h3>&nbsp;</h3>Newsletter sender.<br/>Subject will be used as default for any newsletter but can be changed in each newsletter issue.';
$MOD_TINY_NEWSLETTER['newsletter_various_H'] = 'Newsletter sender';

$MOD_TINY_NEWSLETTER['newsletter_mail_name'] = 'Sender name:';
$MOD_TINY_NEWSLETTER['newsletter_mail_email'] = 'Sender email address:';
$MOD_TINY_NEWSLETTER['newsletter_mail_subject'] = 'Subject:';


$MOD_TINY_NEWSLETTER['all_mails_appendix_H'] = 'Newsletter footer';
$MOD_TINY_NEWSLETTER['all_mails_appendix_T'] = '<h3>HTML</h3>This pararagraph will be attached to each newsletter.<br/>Placeholders:
<p>&lt;a href=&quot;[WEB_VERSION]&quot;&gt;Open in Browser&lt;/a&gt;<br>&lt;a href=&quot;[REMOVE_LINK]&quot;&gt;Remove from list&lt;/a&gt;</p>';

$MOD_TINY_NEWSLETTER['all_mails_appendix_text_H'] = 'Newsletter footer for plain text version';
$MOD_TINY_NEWSLETTER['all_mails_appendix_text_T'] = '<h3>TEXT</h3>This paragraph will be attached to each plain text newsletter.<p>Placeholders: Open in Browser: [WEB_VERSION]<br>Remove from list: [REMOVE_LINK]</p>';

$MOD_TINY_NEWSLETTER['SOME_INTEGERS'] = 'Here you can limit the amount of emails which are sent in a certain value. This might be necessary if the mail server has limitations on this (and most servers have). Please enter only numbers.';
$MOD_TINY_NEWSLETTER['SOME_INTEGERS2'] = 'Limitations';
$MOD_TINY_NEWSLETTER['intval_block'] = 'Maximum of emails which are sent per interval (Default: 5)';
$MOD_TINY_NEWSLETTER['intval_block_delay'] = 'Minimum of seconds between the intervals (Default: 60)';
$MOD_TINY_NEWSLETTER['intval_receiver_delay'] = 'Minimum of hours between two newsletter to the same recipient (Default: 24)';

$MOD_TINY_NEWSLETTER['testmail_adresses_H'] = 'Test emails';
$MOD_TINY_NEWSLETTER['testmail_adresses_T'] = '<h3>&nbsp;</h3>comma seperated:<br/>mail1@example.com,mail2@example.com';

$MOD_TINY_NEWSLETTER['confirmation_finished1_H'] = 'Successful registration message';
$MOD_TINY_NEWSLETTER['confirmation_finished1_T'] = '<h3>HTML</h3>This text will be displayed when a recipient has clicked on the link in the confirmation email.';

$MOD_TINY_NEWSLETTER['confirmation_removed_H'] = 'Unsubscribe message';
$MOD_TINY_NEWSLETTER['confirmation_removed_T'] = '<h3>HTML</h3>This text will be displayed when a recipient has clicked on the sign out link.';

$MOD_TINY_NEWSLETTER['tracking_element_H'] = 'Tracking code';
$MOD_TINY_NEWSLETTER['tracking_element_T'] = '<h3>HTML-Tag</h3>The code for newsletter tracking. Let this value empty if you do not want any tracking.';
$MOD_TINY_NEWSLETTER['tracking_element_typ'] = 'typical: &lt;img src="[WB_URL]/modules/[MOD_DIR]/action.php?pic=[AUTH_CODE]&tnl_id=[TNL_ID]" alt="" /&gt;';

/*** Newsletter Web-Version und Archiv *********************************/
$MOD_TINY_NEWSLETTER['WEB_NL_NOT_FOUND'] = 'Newsletter not found';
$MOD_TINY_NEWSLETTER['WEB_NL_NOT_ACTIVE'] = 'Newsletter is not active';
$MOD_TINY_NEWSLETTER['WEB_NL_UNKNOWN_RECEIVER'] = 'Unknown recipient';
$MOD_TINY_NEWSLETTER['WEB_NL_NO_PERMISSION'] = 'Access to this newsletter is not allowed';
$MOD_TINY_NEWSLETTER['WEB_NL_ARCHIVED'] = '<h3 style="text-align:center;" class="archived-version">Archived version from [ARCHIVED_TIME]</h3>';

include 'EN-frontend.php';
