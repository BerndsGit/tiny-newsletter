<?php
//Modul Description
$module_description = 'Registrierung für Newsletter';

//Variables for the Frontend
//$MOD_TINY_NEWSLETTER['VIEW_TITLE']        = 'Newsletter bestellen:';
$MOD_TINY_NEWSLETTER['NAME']  = '<b>Name</b> zB.: Dr. Erika Mustermann';
$MOD_TINY_NEWSLETTER['EMAIL']       = '<b>E-Mail</b>';
$MOD_TINY_NEWSLETTER['GENDER']   = '<b>Anrede</b>';
$MOD_TINY_NEWSLETTER['AUTHCODE_FIELD'] = 'Auth Code';
$MOD_TINY_NEWSLETTER['SUBMIT'] = 'Anmelden';

$MOD_TINY_NEWSLETTER['ERROR_NAME'] = 'Bitte Namen angeben (mind. 4 Zeichen)!';
$MOD_TINY_NEWSLETTER['ERROR_MAIL'] = 'Bitte g&uuml;ltige eMail-Adresse angeben!';
$MOD_TINY_NEWSLETTER['ERROR_GENDER'] = 'Keine Anrede ausgewählt!';
$MOD_TINY_NEWSLETTER['ERROR_AUTHCODE'] = 'Code ist n&ouml;tig';
$MOD_TINY_NEWSLETTER['ERROR_MAIL_ALREADY'] = 'Die E-Mail-Adresse ist bereits registriert';

$MOD_TINY_NEWSLETTER['UNKNOWN_AUTH'] = 'Unbekannter Code<br/>Eventuell wurde der Account schon gel&ouml;scht.';
$MOD_TINY_NEWSLETTER['UNKNOWN_AUTH0'] = 'In Testmails kann der Newsletter nicht abbestellt werden!';

//Wenn erneut bestetigt wird, aber bereits eingetragen:
$MOD_TINY_NEWSLETTER['FINISHED2'] = 'Der Vorgang ist abgeschlossen. Sie empfangen ab jetzt unseren Newsletter.';

$MOD_TINY_NEWSLETTER['AUTH_CONFIRMED_SUBJECT'] = 'Neue Anmeldung fuer den Newsletter';
$MOD_TINY_NEWSLETTER['AUTH_CONFIRMED_CONTENT']  = "Ein neuer Empfaenger hat soeben die Anmeldung bestaetigt: \n[NAME], \n[EMAIL], \n\n[WB_URL]";
