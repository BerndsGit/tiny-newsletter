<?php
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

//Listing
$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters ORDER BY tnl_published_when DESC LIMIT ".$tnl_limit;
$outp = '';

$res = $database->query($sql);
$c = 0;
while ($row = $res->fetchRow()) {	
	$tnl_id =  (int) $row['tnl_id'];
	$tnl_status =  (int) $row['tnl_status'];
	$tnl_min_active = (int) $row['tnl_min_active'];
	$tnl_published_when = $row['tnl_published_when'];
	$tnl_published_until = $row['tnl_published_until'];
	$tnl_subject = $row['tnl_subject'];
	$tnl_statsdata_Arr = explode(',',$row['tnl_statsdata']);
	$prozent = $tnl_statsdata_Arr[1];
	if ($prozent > 100) {$prozent = 100;}
	
	$data = gmdate(DATE_FORMAT, $tnl_published_when).' - ';
	if ($tnl_published_until > 0) $data .= gmdate(DATE_FORMAT, $tnl_published_until);
	$dateclass='active';
	if ($tnl_published_when != 0 AND $tnl_published_when > $t) $dateclass = 'waiting';
	if ($tnl_published_until != 0 AND $tnl_published_until < $t) $dateclass = 'outdated';
	$data = '<span  class="'. $dateclass.'">'.$data.'</span>';
	
	$data .= '<span class="tnl_dev_only"><br/>'.$row['tnl_statsdata'].'</span>';
	
	$tnl_archived = tnl_get_archived_time ($tablename, $tnl_id);
	if ($tnl_archived == 0) {
		$data .= '<br/><span class="tnl_not_achived">'.$MOD_TINY_NEWSLETTER['NL_NOT_ARCHIVED'].'</span>';
	} else {
		$temp = str_replace('[ARCHIVED_TIME]',gmdate(DATE_FORMAT, $tnl_archived),$MOD_TINY_NEWSLETTER['NL_ARCHIVED']);
		$data .= '<br/><span class="tnl_achived">'.$temp.'</span>';
	}
	
	
	
	$tr = '<tr class="tnl_status tnl_status'.$tnl_status.' tnl_min_active'.$tnl_min_active.'">';
	
	
	//Status:
	$statustext = $MOD_TINY_NEWSLETTER['NL_statusArr'][$tnl_status];
	$statuswidth = 100;
	if ($tnl_status == 2) {$statustext = '&nbsp;'; $statuswidth = $prozent;}
	$tr .= '<td class="tnl_progress"><div class="tnl_progress_outer"><div class="tnl_progress_inner" style="width:'.$statuswidth.'%">'.$statustext.'</div></div></td>';
	
	$tr .= '<td class="tnl_subject"><a href="'.$editbase.$tnl_id.'">'.$tnl_subject.'</a></td><td class="tnl_data">'.$data .'</td>
	<td class="tnl_action"><a href="'.$editbase.$tnl_id.'"><span class="fa fa-fw fa-2x fa-edit"></span></a></td></tr>';
	
	$outp .= $tr;
	$c++;
}

if ($listing_style == 'newsletter') {
	echo '<a class="tnl_button" href="'.$editbase.'add">'.$MOD_TINY_NEWSLETTER['ADD_NEWSLETTER'].'</a>';
}

if ($c == 0) {
	if ($listing_style == 'modify') {
		echo  '<h3>'.$MOD_TINY_NEWSLETTER['NO_NEWSLETTER'].'</h3><br style="clear:both;">';
		echo '<a class="tnl_button" href="'.$editbase.'add">'.$MOD_TINY_NEWSLETTER['ADD_NEWSLETTER'].'</a>';
	}
} else {
	echo '<table class="tnl_listing">'.$outp.'</table>';
}


	
	
?>