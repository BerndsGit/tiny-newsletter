<?php
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

//Needed vars:
// $tnl_id, Newsletter ID
// $tnl_showmode: 'preview', 'previewtext','testmail', 'send', 'webversion', 'archive'
//bei 'send': addr_Arr array(2,6,8)
// webversion

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '.php');
}

require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/mailfunctions.inc.php');
require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/functions.php');

$t = time();

//==================================================================================================
//Load Setting
if (!isset($settings) ) {
	$settings = tnl_LoadSettings ($tablename);
	if (is_array($settings)) {
		$genderarray = json_decode($settings['genderarray'], true);
		//$intval_array = json_decode($settings['intval_array'], true);
	} else {
		die('no Settings found');
	}
}

//Sender:
$from_email = tnl_GetSettings($settings,'newsletter_mail_email');
$from_name = tnl_GetSettings($settings,'newsletter_mail_name');

//various:
$mail_footer = tnl_GetSettings($settings,'all_mails_appendix');
$mail_footer_text = tnl_GetSettings($settings,'all_mails_appendix_text');

//tracking_element
$tracking_element_raw = trim(tnl_GetSettings($settings,'tracking_element'));
if ($tracking_element_raw != '') {
	$tracking_element_raw = str_replace('[WB_URL]', WB_URL, $tracking_element_raw);
	$tracking_element_raw = str_replace('[MOD_DIR]', $mod_dir, $tracking_element_raw);
	$tracking_element_raw = str_replace('[TNL_ID]', $tnl_id, $tracking_element_raw);
}
//still left: [[AUTH_CODE]]

$webversion_link_raw = WB_URL.'/modules/'.$mod_dir.'/web.php?n='.$tnl_id.'&amp;a=';

//======================================================================

//Load Newsletter
$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters WHERE tnl_id = $tnl_id";
$res = $database->query($sql);
if ($res->numRows() == 1) {
	$row = $res->fetchRow();
	$tnl_status = (int) $row['tnl_status'];

	$tnl_first_sent = (int) $row['tnl_first_sent'];
	$tnl_published_when = (int) $row['tnl_published_when'];
	$tnl_published_until = (int) $row['tnl_published_until'];
	$tnl_subject = htmlspecialchars($row['tnl_subject']);
	$tnl_body = $row['tnl_body'];
	$tnl_body_text = $row['tnl_body_text'];

	$tnl_statsdata = $row['tnl_statsdata'];

	$tnl_template_data_Arr = json_decode($row['tnl_template_data'], true);
	if (!is_array($tnl_template_data_Arr)) {$tnl_template_data_Arr = array(); }

} else {
	die('no Newsletter found');
}

//======================================================================
//Load Template
$tnl_template = tnl_GetSettings($settings,'default_template');
if ($tnl_template == '') {$tnl_template = 'default';}
$path = WB_PATH.'/modules/'.$mod_dir.'/templates/'.$tnl_template.'/';
$tnl_template_dir = WB_URL.'/modules/'.$mod_dir.'/templates/'.$tnl_template.'/';

if (!file_exists($path.'index.php')) {
	$path = WB_PATH.'/modules/'.$mod_dir.'/templates/default/';
	$tnl_template_dir = WB_URL.'/modules/'.$mod_dir.'/templates/default/';
}
if (!file_exists($path.'index.php')) {die('no template!');}




//Get Content in buffer for Droplets and/or Filter operations
ob_start();
include $path.'index.php';
$output = ob_get_clean();



if ($tnl_showmode == 'webversion' OR $tnl_showmode == 'archive') { //kein Footer in der Web- und Archivversion, weil dieser den Abmelden-Link enthaelt
	$mail_footer = '';
	$settings['all_mails_appendix_text'] = '';
}

$vars = array('[MAIL_BODY]','[MAIL_SUBJECT]', '[MAIL_FOOTER]');
$values = array($tnl_body, $tnl_subject, $mail_footer);
$output = str_replace($vars, $values, $output);

//======================================================================

//wie in /index.php:
if (!isset($admin)) { $admin = new admin(); }
// Module is installed?
if (file_exists(WB_PATH . '/modules/output_filter/index.php')) {
	include_once WB_PATH . '/modules/output_filter/index.php';
	if (function_exists('executeFrontendOutputFilter')) {
		$output = executeFrontendOutputFilter($output);
	}
}

//======================================================================
$content = $output; //$output wurde hier verwendet, um ggf. einfacher copy/paste aus /index.php zu machen.

if ($tnl_showmode == 'archive') {
	tnl_make_archive($tnl_id, $tablename, $content);
	die(); //hier Ende
}


//======================================================================
//Die einzigen Platzhalter, der jetzt noch vorhanden sein sollten sind [SALUTATION] und [REMOVE_LINK]
//Diese bleiben auch in der Text-Version
//======================================================================
//Text version:
if ($tnl_body_text == '') {
	//Notloesung
	//$content_text = tnl_textversion($content);
	// Vielleicht besser?
	require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/Html2Text.php');
	$h2t = new \Html2Text\Html2Text($content);
	$content_text = $h2t->get_text();
} else {
	$content_text = $tnl_body_text."\n".tnl_GetSettings($settings,'all_mails_appendix_text');;
}

//======================================================================

//vordefiniert fuer Test:
$auth_link =  WB_URL.'/modules/'.$mod_dir.'/action.php?auth=0';
$remove_link =  WB_URL.'/modules/'.$mod_dir.'/action.php?auth=0&do=remove#nlfrmblock';
$auth_code = '0';

if ($tnl_showmode == 'preview' OR $tnl_showmode == 'previewtext') {
	//==================================================================
	//Load any receiver as Demo
	$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs ORDER BY rand() LIMIT 1";
	$res = $database->query($sql);
	$row = $res->fetchRow();
	$addr_name = $row['addr_name'];
	$addr_email = $row['addr_email'];
	$addr_type = (int) $row['addr_type'];
	$tnl_salutation = $genderarray['gender_a-'.$addr_type];
	$tnl_salutation = str_replace('[NAME]', $addr_name, $tnl_salutation);
}



//======================================================================
//Mesh up

$vars = array('[SALUTATION]', '[REMOVE_LINK]', '[TR_ELEMENT]', '[WEB_VERSION]');


//Preview
if ($tnl_showmode == 'preview') {
	$tracking_element = str_replace('[TR_CODE]', '0', $tracking_element_raw);
	$values = array($tnl_salutation, $remove_link,  $tracking_element, $webversion_link_raw);
	$mail_content = str_replace($vars, $values, $content);
	echo $mail_content;
	die();
}

//Preview-text
if ($tnl_showmode == 'previewtext') {
	$tracking_element = str_replace('[TR_CODE]', '', $tracking_element_raw);
	$values = array($tnl_salutation, $remove_link, $tracking_element, $webversion_link_raw);
	$mail_text = strip_tags(str_replace($vars, $values, $content_text));
	echo nl2br($mail_text);
	die();
}

//webversion
if ($tnl_showmode == 'webversion') {
	//die variablen sollten schon vorhanden sein, DB muss nicht mehr abgefragt werden.

	$vars = array('[REMOVE_LINK]', '[TR_ELEMENT]');
	$values = array('', '');
	$content = str_replace($vars, $values, $content);

	//[SALUTATION] ist noch drin.
	//Das kann jetzt ins Archiv gespeichert werden:
	//file_put_contents($static_file, $content);

	$tnl_salutation = $genderarray['gender_a-'.$addr_type];
	$tnl_salutation = str_replace('[NAME]', $addr_name, $tnl_salutation);

	$content = str_replace('[SALUTATION]', $tnl_salutation, $content);
	echo $content;
	die();
}


//testmail
if ($tnl_showmode == 'testmail') {
	$testmail_adresses_Arr = explode(',',$settings['testmail_adresses']);
	foreach($testmail_adresses_Arr as $testmail_email ) {
		$testmail_email = trim($testmail_email);
		if (strpos($testmail_email, '@') == false) {continue;}

		$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_email = '$testmail_email'";
		$res = $database->query($sql);
		if ($res->numRows() == 1) {
			$row = $res->fetchRow();
			$addr_name = $row['addr_name'];
			$addr_email = $row['addr_email'];
			$addr_type = (int) $row['addr_type'];
			$tnl_salutation = $genderarray['gender_a-'.$addr_type];
			$tnl_salutation = str_replace('[NAME]', $addr_name, $tnl_salutation);
		} else {
			$tnl_salutation = $genderarray['gender_a-0'];
			$addr_email = $testmail_email;
			$addr_name ='Name';
		}
		$tracking_element = str_replace('[TR_CODE]', '0', $tracking_element_raw);

		$values = array($tnl_salutation, $remove_link, $tracking_element, $webversion_link_raw);
		$tnl_content = str_replace($vars, $values, $content);
		$tnl_content_text = strip_tags(str_replace($vars, $values, $content_text));
		$tnl_content_text = str_replace('&amp;', '&', $content_text); //'&amp;' aus Links entfernen

		$done = tnl_send_mail($from_email, $from_name, $addr_email, $addr_name, $tnl_subject, $tnl_content, $tnl_content_text, $simulation);
		if ($simulation == 0) {
			if ($done) {echo $MOD_TINY_NEWSLETTER['TEST_SENT_TO'].$addr_email;} else {echo $MOD_TINY_NEWSLETTER['TEST_SENT_ERROR'].$addr_email;}
			echo '<br/>';
		}
	}
	die();
}

//send
if ($tnl_showmode == 'send') {
	if ($tnl_first_sent == 0) {
		$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_newsletters SET tnl_first_sent = '$t' WHERE tnl_id = $tnl_id;";
		$database->query($sql);
	}

	//==================================================================
	//statistik Daten:
	//Derzeit vorhandene Newsletter:
	$sql = "SELECT count(*) as total FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters WHERE tnl_first_sent > 0;";
	$res = $database->query($sql);
	$row = $res->fetchRow();
	$total_newsletters = $row['total'];
	//echo '<h2>total_newsletters: '.$total_newsletters.'</h2>';


	//Derzeit vorhandene Empfaenger:
	$sql = "SELECT count(*) as total FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status > 0;";
	$res = $database->query($sql);
	$row = $res->fetchRow();
	$total_addrs = $row['total'];
	//echo '<h2>total_addrs: '.$total_addrs.'</h2>';
	//==================================================================

	$countsent = 0;
	if (count($addr_Arr) < 1) return;

	//$addr_Arr: Array mit bereits heraus gesuchten empfaengern
	if (!isset($min_addr_gotlast)) {$min_addr_gotlast = $t;}
	if (count($addr_Arr) == 1) {
		$addr_Arr_str = $addr_Arr[0];
		$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_id = $addr_Arr_str AND addr_gotlast < $min_addr_gotlast";
	} else {
		$addr_Arr_str = implode(',',$addr_Arr);
		$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_id IN ($addr_Arr_str) AND addr_gotlast < $min_addr_gotlast";
	}
	//echo $sql;

	$intval_block_limit = (int) tnl_GetSettings($settings, 'intval_block'); //linit

	$res = $database->query($sql);
	if ($res->numRows() > 0) {
		while ($row = $res->fetchRow()) {
			$addr_gotlast = (int) $row['addr_gotlast'];
			/*
			if ($addr_gotlast > $min_addr_gotlast) {
				if ($simulation == 1) {echo 'addr_gotlast timeout<br/>';}
				continue;
			}
			*/

			$addr_id = $row['addr_id'];
			$addr_name = $row['addr_name'];
			$addr_email = $row['addr_email'];
			$addr_type = (int) $row['addr_type'];
			if ($addr_name == '') {$addr_type = 0;}
			$tnl_salutation = $genderarray['gender_a-'.$addr_type];
			$tnl_salutation = str_replace('[NAME]', $addr_name, $tnl_salutation);

			$addr_idstr1 = $row['addr_idstr1'];
			$remove_link =  WB_URL.'/modules/'.$mod_dir.'/action.php?auth='.$addr_idstr1.'&do=remove#nlfrmblock';

			$addr_idstr2 = $row['addr_idstr2'];
			$tracking_element = str_replace('[TR_CODE]', $addr_idstr2, $tracking_element_raw);

			$webversion_link = $webversion_link_raw.$addr_idstr2;

			$values = array($tnl_salutation, $remove_link, $tracking_element, $webversion_link);
			$tnl_content = str_replace($vars, $values, $content);
			$values = array($tnl_salutation, $remove_link, ''); //kein tracking in der Text-Version
			$tnl_content_text = strip_tags(str_replace($vars, $values, $content_text));

			$is_testadresse = 0;
			if (strpos($addr_email,'.test') > 0) {$is_testadresse = 1;}


			$done = tnl_send_mail($from_email, $from_name, $addr_email, $addr_name, $tnl_subject, $tnl_content, $tnl_content_text, $simulation);




			//eintragen:
			if ($simulation == 0 OR $is_testadresse == 1) { //KEINE SImulation oder es ist eine Testadresse
				$addr_hasgot = str_replace(',,',',',$row['addr_hasgot']);
				if ($addr_hasgot == '') {
					$addr_hasgot  = ','.$tnl_id.',';
				} else {
					$addr_hasgot  = ','.$tnl_id.$addr_hasgot;
					$addr_hasgot = str_replace(',,',',',$addr_hasgot);
					//Hinweis: wird nach ca 100 Nummern einfach hinten abgeschnitten, wenn es nicht mehr in das DB-Feld passt.
					//Das macht aber nichts, weil dann auch der Beistrich fehlt.
				}
				//echo '<h3>addr_hasgot: '.$addr_hasgot.'</h3>';

				//addr_statsdata: hasgotGesamt,hasgotProzent,hasseenGesamt,hasseenProzent,freie..
				$addr_statsdata = $row['addr_statsdata'];
				if ($addr_statsdata == '') {$addr_statsdata = '0,0,0,0,0,0,0,0,0';}
				$addr_statsdataArr = explode(',',$addr_statsdata);
				$hasgot_sum = (int) $addr_statsdataArr[0] + 1;
				$einproz = 100 / $total_newsletters;
				$hasgot_proz = ceil($hasgot_sum * $einproz);
				$addr_statsdataArr[0] = $hasgot_sum;
				$addr_statsdataArr[1] = $hasgot_proz;
				$addr_statsdata = implode(',',$addr_statsdataArr);


				$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_statsdata = '$addr_statsdata', addr_hasgot = '$addr_hasgot', addr_gotlast = '$t' WHERE addr_id = $addr_id;";
				//echo $sql;
				$database->query($sql);


				$countsent++;
				if ($countsent >= $intval_block_limit) {break;}

			} ///end if ($simulation == 0 OR $is_testadresse == 1)

		} //End while

	//Statistik Newsletter:
	if ($tnl_statsdata == '') {$tnl_statsdata = '0,0,0,0,0,0,0,0,0';}
	$tnl_statsdataArr = explode(',',$tnl_statsdata);

	$sql = "SELECT count(*) as total FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status > 0 AND addr_idstr2 <> '' AND addr_hasgot LIKE '%,$tnl_id,%'";
	$res = $database->query($sql);
	$row = $res->fetchRow();
	$hasgot_sum = $row['total'];

	$einproz = 100 / $total_addrs;
	$hasgot_proz = ceil($hasgot_sum * $einproz);
	$tnl_statsdataArr[0] = $hasgot_sum;
	$tnl_statsdataArr[1] = $hasgot_proz;
	$tnl_statsdata = implode(',',$tnl_statsdataArr);
	if (count($tnl_statsdataArr) < 8) {$tnl_statsdata .= ',0,0,0';} //gleich noch ein paar dazu

	$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_newsletters SET tnl_statsdata = '$tnl_statsdata', tnl_last_sent = '$t' WHERE tnl_id = $tnl_id;";
	$database->query($sql);


	} //end if ($res->numRows() > 0) {

}

?>
