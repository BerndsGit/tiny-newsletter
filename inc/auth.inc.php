<?php

//Egal, was und wie sonst uebermittelt wurd: Wenn auth(code) gueltig, dann wird nur mehr in die Datenbank eingetragen bzw geloescht
$auth  = ''; $act = '';

if ( isset($_GET['auth'])) { $auth = $_GET['auth']; $act='get';} //direkter Aufruf (meist aus Mail)

if ( isset($_POST['authcode'])) { $auth = $_POST['authcode']; $act='post';} //AuthCode direkt eingegeben und per post uebertragen. Wird nicht mehr verwendet, aber bleibt vorerst.

//die('<h1>hierStart</h1>');

if ($auth === '0') { //Testmail
	if ($act=='get') {$finished = $MOD_TINY_NEWSLETTER['UNKNOWN_AUTH0']; $finishedclass='err'; return 0;}
	$output = json_encode(array('type'=>'finished', 'text' => $MOD_TINY_NEWSLETTER['UNKNOWN_AUTH0']));
	return($output);
}

$auth = strtolower($auth);
$auth = preg_replace("/[^a-z0-9,.]+/", "", $auth);

$finished = 'ERR'; $finishedclass='err';

if(strlen($auth) == 12){  //formal gueltig
	//vorhanden?
	$q = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_idstr1 = '$auth' ORDER BY addr_started DESC LIMIT 1";
	//echo $q;
	$res = $database->query($q);
	if( $res->numRows() >  0) {
		$row = $res->fetchRow();
		$addr_idstr2 = $row['addr_idstr2'];
		$addr_id = $row['addr_id'];

		//Der Link im Mail zum entfernen wurde angeklickt:
		if ( isset($_REQUEST['do']) AND $_REQUEST['do'] == 'remove') {
			$q = "DELETE FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_id = ".$addr_id.';';
			$database->query($q);

			if ($act=='get') { //Aufruf aus Mail
				// build at least some basic html-frame
				?>
				<!doctype html>
				<head>
				<title>Newsletter</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<style type="text/css">
					body {background-color:#eee;}
					.main {background-color:#fff; color:#909090;
							padding:1em; max-width:50em; margin:3em auto;
							font-family:"Open Sans",Arial,sans-serif;
							border-radius:5px; text-align:center;}
				</style>
				</head>
				<body>
					<div class="main">
						<?php
							$finished = tnl_GetSettings($settings, 'confirmation_removed');
							$finishedclass='remove';
							return 0; //direkt ausgabe
						?>
					</div>
				</body>
				</html>
				<?php

			} else {
				$output = json_encode(array('type'=>'finished', 'text' => tnl_GetSettings($settings, 'confirmation_removed')));
				return($output);
			}
		}

		if ($addr_idstr2 == '') {
			//OK, ist noch nicht registriert, wird eingetragen:

			$auth2 = tnl_GenerateRandomString();
			$sql = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_addrs SET addr_idstr2 = '$auth2', addr_status = '2' WHERE addr_id = ".$addr_id.';';
			$database->query($sql);

			//Beanchrichtigung an Admin:
			require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/mailfunctions.inc.php');
			$subject = $MOD_TINY_NEWSLETTER['AUTH_CONFIRMED_SUBJECT'];
			$content = $MOD_TINY_NEWSLETTER['AUTH_CONFIRMED_CONTENT'];
			$content = str_replace('[EMAIL]', $row['addr_email'], $content);
			$content = str_replace('[NAME]', $row['addr_name'], $content);
			$content = str_replace('[WB_URL]', WB_URL, $content);

			$to_email = tnl_GetSettings($settings, 'confirmation_mail_email');
			$to_name = tnl_GetSettings($settings, 'confirmation_mail_name');
			tnl_send_mail($to_email, $to_name, $to_email, $to_name, $subject, $content, $content_text='', $simulation=0);



			//$output = json_encode(array('type'=>'finished', 'text' => "ist hier2".$auth));
			//die($output);

			if ($act=='get') {
				$finished = tnl_GetSettings($settings, 'confirmation_finished1'); $finishedclass='done'; return 0;
			} else {
				$output = json_encode(array('type'=>'finished', 'text' =>  tnl_GetSettings($settings, 'confirmation_finished1')));
				return($output);
			}

		} else {
			//$output = json_encode(array('type'=>'finished', 'text' => "ist hier23".$auth));
			//die($output);

			if ($act=='get') {
				$finished = $MOD_TINY_NEWSLETTER['FINISHED2']; $finishedclass='done'; return 0;
			} else {
				$output = json_encode(array('type'=>'finished', 'text' => tnl_GetSettings($settings, 'confirmation_finished1')));
				return($output);
			}
		}
	} else {
	 	if ($act=='get') {$finished = $MOD_TINY_NEWSLETTER['UNKNOWN_AUTH']; $finishedclass='err'; return 0;}
		$output = json_encode(array('type'=>'finished', 'text' => $MOD_TINY_NEWSLETTER['UNKNOWN_AUTH']));
		return($output);

	}
} else {
	 if ($act=='get') {$finished = $MOD_TINY_NEWSLETTER['UNKNOWN_AUTH']; $finishedclass='err'; return 0;}

	//bei post: Weiter als ob kein Auth uebertragen
	$auth = '';
}

?>
