<?php
function tnl_LoadSettings ($tablename) {
	global $database;
	
	$settings = array();
	$query = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_settings ";
	$res = $database->query($query);
	while ($row = $res->fetchRow()) {	
		$property = $row['property'];
		$value = $row['value'];	
		$settings[$property] = $value;
	}
	return $settings;
}



function tnl_GetSettings($settings, $what) {
	if (isset($settings[$what])) {return $settings[$what];} else {return false;}
}

function tnl_GetSpecialSettings($settings, $what) {
	if (isset($settings[$what])) {return htmlspecialchars($settings[$what]);} else {return false;}
}


function tnl_SetSettings($settings, $tablename, $what, $value) {
	global $database;
	if (isset($settings[$what])) {
		$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$value' WHERE property = '$what'";
	} else {
		$query = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_settings SET value = '$value', property = '$what'";		
	}
	$res = $database->query($query);
}

function tnl_GenerateRandomString($length = 12) {
	//test:
	 //return '5z21wla2cm9w';

    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function tnl_get_archived_time ($tablename, $tnl_id) {
	global $database;
	$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_archive WHERE tnl_id = $tnl_id";
	$res = $database->query($sql);
	if ( $res->numRows() ==  1) {
		//eine archivierte Kopie existiert:
		$row = $res->fetchRow();
		$tnl_archived = $row['tnl_archived'];
		return $tnl_archived;
	} else {
		return 0;
	}

}


?>