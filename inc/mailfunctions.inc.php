<?php
function tnl_send_mail($from_email, $from_name, $to_email, $to_name, $subject, $content, $content_text='', $simulation=1) {
	//Dummy, simulation=1
	if ($simulation == 1) {
		echo '<p>'.$to_email.'</p>';
		echo $content;
		
		return false;
	}
	
	
	/*
	framework/class.wb.php line 406: 
	public function mail($fromaddress, $toaddress, $subject, $message, $fromname = '')
	
	modifiziert:	
	*/
	
	
	$from_email = preg_replace('/[\r\n]/', '', $from_email);
	$to_email = preg_replace('/[\r\n]/', '', $to_email);
	$from_name = preg_replace('/[\r\n]/', '', $from_name);
	$to_name = preg_replace('/[\r\n]/', '', $to_name);
		
	$subject = preg_replace('/[\r\n]/', '', $subject);
	
	// $message_alt = $message;
	// $message = preg_replace('/[\r\n]/', '<br \>', $message);

	// create PHPMailer object and define default settings
	$myMail = new wbmailer();
	
	
	$myMail->setFrom($from_email, $from_name);
	$myMail->addAddress($to_email, $to_name);
	
	$myMail->Subject = $subject;             // SUBJECT
	$myMail->Body = $content;         // CONTENT (HTML)
	$myMail->AltBody = $content_text; // CONTENT (TEXT)
											 // check if there are any send mail errors, otherwise say successful
	if (!$myMail->Send()) {
		return false;
	} else {
		return true;
	}	
}

function tnl_make_archive($tnl_id, $tablename, $content) {
	global $database;
	$t = time();
	$tnl_html = addslashes($content);
	
	$query = "SELECT tnl_archived FROM ".TABLE_PREFIX."mod_".$tablename."_archive WHERE tnl_id = $tnl_id";	
	$res = $database->query($query);
	if ($res->numRows() > 0) {
		$query = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_archive SET tnl_archived = '$t', tnl_html = '$tnl_html' WHERE tnl_id = '$tnl_id'";
		echo 'UPDATE';
	} else {
		$query = "INSERT INTO ".TABLE_PREFIX."mod_".$tablename."_archive SET tnl_archived = '$t', tnl_html = '$tnl_html', tnl_id = '$tnl_id'";
			echo 'INSERT';		
	}
	$res = $database->query($query);
}


function tnl_textversion($html) {
	//$html = br2nl($html);
	//$html = preg_replace('/<a href="[^"]*">[^<]*<img src="[^"]*"[^>]*>[^<]*<\/a>/sim', '', $html); //remove all a img
	/*$html = preg_replace('/<a.*?(<img.*?>)<\/a>/', '', $html);*/
	$text = strip_tags($html, '<a><br>');
	$text = html_entity_decode($text);
	$vars = array('<a','</a>', 'target="_blank"', ' href="', '">');
	$values = array(' ',' ', '','',' ');
	$text = str_replace($vars,$values, $text);
	
	$tarr = explode("\n", $text);
	$newtext = '';
	foreach ($tarr as $line) {
		$line = trim($line);
		if (strlen($line) > 10) { $newtext .= $line."\n";}
	}
	return $newtext;
	

}

function tnl_extractlinks($html) {

//wird nicht benutzt, nur als Muster
	$dom = new DOMDocument();
    $dom->loadHTML($html);

    $anchors = $dom->getElementsByTagName('a');
    $len = $anchors->length;

    if ( $len > 0 ) {
        $i = $len-1;
        while ( $i > -1 ) {
        $anchor = $anchors->item( $i );

        if ( $anchor->hasAttribute('href') ) {
            $href = $anchor->getAttribute('href');
            /*
			$regex = '/^http/';

            if ( !preg_match ( $regex, $href ) ) { 
				$i--;
				continue;
            }
			*/

            $text = $anchor->nodeValue;
            $textNode = $dom->createTextNode( $text );

            $strong = $dom->createElement('strong');
            $strong->appendChild( $textNode );

            $anchor->parentNode->replaceChild( $strong, $anchor );
        }
        $i--;
        }
    }

    return $dom->saveHTML();
}

?>