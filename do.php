<?php
//Sending Newsletters

require('../../config.php');
if(!defined('WB_PATH')) { exit("Cannot access this file directly"); }

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;

require_once(WB_PATH.'/framework/class.wb.php');
$wb = new wb;
$t = time();

$simulation = isset($_REQUEST['simulation']) ? intval($_REQUEST['simulation']) : 0;


//Haeufigster und einfachster Fall: Es ist gar kein Newsletter zu versenden:
//==========================================================================
//Find ALL newsletter to send:
$sql = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_newsletters WHERE tnl_status = 2 AND (tnl_published_when < $t OR tnl_published_when = 0) AND (tnl_published_until > $t OR tnl_published_until = 0) AND tnl_body <> '' ORDER BY tnl_published_when";
//echo $sql;
$res = $database->query($sql);
if ($res->numRows() < 1) {
	$msg = '';
	if ($simulation == 1) {$msg = 'No Newsletter to send';}
	die($msg);
} //No Newsletter found, quit

//======================================================================

require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/functions.php');


//Load Settings:
$settings = tnl_LoadSettings ($tablename);
if (is_array($settings)) {
	$genderarray = json_decode($settings['genderarray'], true);
	//$intval_array = json_decode($settings['intval_array'], true);
	//var_dump($intval_array);
} else {
	die('no Settings found');
}

//check delay times
$lastdone = (int) tnl_GetSettings($settings, 'lastdone');
$intval_block_delay = (int) tnl_GetSettings($settings, 'intval_block_delay');

if ($simulation == 0) {
	if ( $lastdone + $intval_block_delay > $t) {
		$d = $t - ($lastdone + $intval_block_delay);
		$msg = '';
		if ($simulation == 1) {$msg = 'intval_block_delay: '.$d;}
		die($msg);
	}

	$intval_receiver_delay = 3600 * tnl_GetSettings($settings, 'intval_receiver_delay');
	$min_addr_gotlast = $t - $intval_receiver_delay;
} else {
	$min_addr_gotlast = $t;
}

//aktuellen aufruf gleich speichern:
tnl_SetSettings($settings, $tablename, 'lastdone', $t);


//echo "hier";


//======================================================================
$intval_block_limit = (int) tnl_GetSettings($settings, 'intval_block');
$addr_limit = 2 * $intval_block_limit;
if ($addr_limit < 10) {$addr_limit = 10;} //Zunaechst mehr abfragen, damit es nicht zu Blockaden kommt
$limit = ' LIMIT '.$addr_limit;

$filtertest = " AND addr_email NOT LIKE '%.test' ";
if ($simulation == 1) {$filtertest = " AND addr_email LIKE '%.test' ";}
$allcountsent = 0;

while ($row = $res->fetchRow()) {
	$tnl_id = (int) $row['tnl_id'];
	$tnl_min_active =  (int) $row['tnl_min_active'];

	//==================================================================================================

	//find receivers:

	$sql2 = "SELECT * FROM ".TABLE_PREFIX."mod_".$tablename."_addrs WHERE addr_status >= $tnl_min_active AND addr_idstr2 <> '' AND addr_hasgot NOT LIKE '%,$tnl_id,%' ".$filtertest." ORDER BY addr_gotlast ".$limit;
	//echo $sql2;
	$res2 = $database->query($sql2);
	//echo $res2->numRows();
	if ($res2->numRows() < 1 AND $simulation == 0) {
		//echo 'keiner gefunden';
		//wenn kein Empfaenger mehr gefunden wurde, ist der Newsletter finished, allerdings erhalten dann auch Empf�nger, die sich zwar w�hrend der Laufzeit des Newsletters noch abmelden, aber nach Ende des Versands, den Newsletter nicht mehr. Ist dieses Verhalten nicht gew�nscht, die beiden nachfolgenden Zeilen auskommentieren.
		
		$sql3 = "UPDATE ".TABLE_PREFIX."mod_".$tablename."_newsletters SET tnl_status = 3 WHERE tnl_id = $tnl_id";
		$database->query($sql3);

		continue; //No Receivers found, continue
	}

	$addr_Arr = array();
	while ($row2 = $res2->fetchRow()) {
		$addr_id = $row2['addr_id'];
		//echo $row2['addr_email'] ;
		$addr_Arr[] = $addr_id;
	}
	//var_dump($addr_Arr);
	if (count($addr_Arr) < 1) {continue;}

	$tnl_showmode = 'send';
	//echo 'build_mail start';
	require_once(WB_PATH . '/modules/'.$mod_dir.'/inc/build_mail.inc.php');
	//echo 'build_mail done';
	$allcountsent += $countsent;
	if ($allcountsent >= $intval_block_limit) {
		die();
	}
	 //done
}

?>
