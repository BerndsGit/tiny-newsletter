<?php
if(!defined('WB_URL')) { die(); }

//view.php zeigt das registrierformular und nimmt die Bestaetigung (Authentifizierung aus eMail) entgehen:

$mod_dir = basename(dirname(__FILE__));
$tablename = $mod_dir;

require_once(WB_PATH.'/modules/'.$mod_dir.'/inc/functions.php');
require_once(WB_PATH.'/include/captcha/captcha.php');

// include the default language
require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/EN-frontend.php');
// check if module language file exists for the language set by the addr (e.g. DE, EN)
if (file_exists(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '-frontend.php')) {
    require_once(WB_PATH . '/modules/'.$mod_dir.'/languages/' . LANGUAGE . '-frontend.php');
}

$settings = tnl_LoadSettings ($tablename);
$genderarray = json_decode($settings['genderarray'], true);

$sstring = '';
for ($i=1; $i < 6; $i++) {
	$s = $genderarray['gender_t-'.$i];
	if ($s == '' OR $s == '-') { continue; }
	$sstring .='<option value="'.$i.'">'.$s.'</option>';
}

echo '<div id="nlfrmblock" class="nlfrmblock_wrapper">'; //das ist immer um den Newsletter herum

$t = time();

//Authentifizierung aus eMail:
if ( isset($_GET['auth'])) {

	require WB_PATH.'/modules/'.$mod_dir.'/inc/auth.inc.php';
	if ($finished != '') {
		echo '<div class="finished '.$finishedclass.'">'.$finished.'</div>
		</div>';
		return; //Ende des Scripts
	}
}

?>

<div id="tnl_register">
    <?php
	ob_start();
	?>

    <div id="contact_body">
		<input type="hidden" name="tnl_page_id" value="<?php echo $page_id;?>" />

		<div class="tnl_inputblock"><label for="tnl_name"><span><?php echo $MOD_TINY_NEWSLETTER['NAME'];?><span class="required">*</span></span></label><br /><input type="text" name="tnl_name" id="tnl_name" required="true" class="input-field" value="" /></div>

		<div class="tnl_inputblock"><label for="tnl_email"><span><?php echo $MOD_TINY_NEWSLETTER['EMAIL'];?><span class="required">*</span></span></label><br /><input type="email" name="tnl_email" id="tnl_email" required="true" class="input-field" value=""/></div>

		<div class="tnl_inputblock">
			<label for="tnl_gender"><span><?php echo $MOD_TINY_NEWSLETTER['GENDER'];?></span> </label><br />
			<select name="tnl_gender" class="select-field">
			<?php echo $sstring; ?>
			</select>
		</div>

		<?php 
		if (!isset($settings['use_captcha'])) {
				$settings['use_captcha']='false';
			}
		if ($settings['use_captcha'] == 'true') { ?>
			<div class="tnl_inputblock">
				<label for="captcha"><b><?php echo $TEXT['CAPTCHA_VERIFICATION'];?></b> </label><br />
				<?php
					//call_captcha();
					call_captcha('image', 'class="tnl-captcha-image"');
					call_captcha('input', 'class="tnl-captcha-input"');
					call_captcha('text', 'class="tnl-captcha-text"');
				?>
			</div>
		<?php } ?>

		<div class="tnl_inputblock" id="tnl_message_box"><label for="tnl_message"><span>Message</span><br/>
			<textarea name="tnl_message" id="tnl_message" class="textarea-field"></textarea>
		</label></div>

		<div id="contact_results"></div>

		<div class="tnl_inputblock" id="authcode_block" style="display:none;" >
			<label for="tnl_authcode"><span><?php echo $MOD_TINY_NEWSLETTER['AUTHCODE_FIELD'];?></span></label><br /><input type="text" name="tnl_authcode" class="input-field" value=""/>
		</div>
	 </div>

	<div class="tnl_inputblock">
	<input style=" margin-top:20px;" type="submit" id="tnl_submit_btn" value="<?php echo $MOD_TINY_NEWSLETTER['SUBMIT']; ?>" />
	</div>

	<?php
	$formbody = ob_get_clean();
	$muster = $settings['newsletter_form'];
	$formbody = str_replace('[FORMBODY]',$formbody,$muster);
	echo $formbody;
	?>


</div>
<iframe style="border:none; height:30px;" scrolling="no" allowtransparency="true" src="<?php echo WB_URL; ?>/modules/<?php echo $mod_dir; ?>/do.php"></iframe>

<script type="text/javascript">
var WB_URL = "<?php echo WB_URL; ?>";
var tnl_mod_dir = "<?php echo $mod_dir; ?>";
$(document).ready(function() {
	$('.captcha_table input').addClass('input-field').css('width', '60px');
});
</script>
</div>
